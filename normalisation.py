import numpy as np
import cv2
import scipy.misc as scm
import matplotlib.pyplot as plt
import os
plt.rcParams['image.cmap']='gray'
import tensorflow as tf
#from tensorflow.python.ops import linalg_ops
#from tensorflow.python.framework import ops
#from tensorflow.python.ops import math_ops
#from tensorflow.python.framework import tensor_shape
#from tensorflow.contrib.data import Dataset, Iterator
from utils import update_dir




curveLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
imLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysLabelled/'
subLocs  = ['xRaysLabelled/','xRaysBatch1/','xRaysBatch2/']
bone     = 'RH2MC'


def histogram_norm_no_justu(imLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/xRaysLabelled/',plot=False):
 imList = sorted(os.listdir(imLoc))
 if imLoc[-1]=='/':
     end_name = os.path.split(imLoc[:-1])[1]
 else:
     end_name = os.path.split(imLoc)[1]
 #output_directory = update_dir('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/histNorm/')
 output_directory  = os.path.join('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/histNorm/',end_name)
 if os.path.isdir(output_directory)==False: os.makedirs(output_directory)
 k = 0
 for imName in imList:
   img = cv2.imread(imLoc+imName,0)
# create a CLAHE object (Arguments are optional).
   clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
   cl1 = clahe.apply(img)
   cv2.imwrite(os.path.join(output_directory,imList[k]),cl1)
   k= k+1
   print(k)
#cv2.imwrite('clahe_2.jpg',cl1)
   if plot:
       fig, axes = plt.subplots(nrows = 1, ncols=2, figsize=(10,12))

       axes[0].imshow(img)
       axes[0].set_title('raw image')
       axes[1].imshow(cl1)
       axes[1].set_title('equalised image')
       plt.show()
       getOut = input("press enter to continue, 0 to exit: ")
       if getOut=='':
          getOut=bool(1)
       else:
          if getOut=='0':
             getOut = bool(0)
          else:
             getOut=bool(1)
       #input('press enter')
       plt.close()
       if getOut==False: break
       #





"""
def batch_normalise_no_jutsu(loc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysLabelled',nIm =2,mu = 0, var = 1,off = 10 ,                             gamma= 1,eps = 0.1 ,fullSet = False, Dtype = tf.float64):
 
 imFiles    = sorted(os.listdir(loc),reverse = False)
 #train_imgs = sorted([os.path.join(loc, file) for file in os.listdir(loc)], key=os.path.getctime ,reverse = False)[0:nIm]
 if fullSet:
    train_imgs = sorted([os.path.join(loc, file) for file in os.listdir(loc)], key=os.path.getctime ,reverse = False)
 else:
    train_imgs = sorted([os.path.join(loc, file) for file in os.listdir(loc)], key=os.path.getctime ,reverse = False)[0:nIm]
 #t_kernel = make_gabor_kernel()
 #t_kernel = make_gabor_kernel(sigmas=sigmas,thetas=thetas,wavelength=wavelength,sigmay=sigmay)
 
 def input_parser(img_path):
    # convert the label to one-hot encoding
    #one_hot = tf.one_hot(label, NUM_CLASSES)

    # read the img from file
    img_file    = tf.read_file(img_path)
    img_decoded = tf.cast(tf.image.decode_image(img_file, channels=1),tf.float64)
    img_decoded = tf.expand_dims(img_decoded,0)
    t_norm      = tf.nn.batch_normalization(img_decoded,
                                            mean    =tf.constant(mu, dtype = Dtype),
                                            variance=tf.constant(var, dtype = Dtype),
                                            offset  =tf.constant(off, dtype = Dtype),
                                            scale   =tf.constant(gamma, dtype = Dtype),
                                            variance_epsilon=tf.constant(eps, dtype = Dtype)
                                            )

    return t_norm

# create TensorFlow Dataset objects

 tr_data = Dataset.from_tensor_slices((train_imgs))
 tr_data  = tr_data.map(input_parser)

# create TensorFlow Iterator object
 iterator = Iterator.from_structure(tr_data.output_types,
                                   tr_data.output_shapes)
 next_element = iterator.get_next()

# create two initialization ops to switch between the datasets
 training_init_op = iterator.make_initializer(tr_data)
 output_directory = update_dir('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/batchNorm/')
 with tf.Session() as sess:

    # initialize the iterator on the training data
    sess.run(training_init_op)

    # get each element of the training dataset until the end is reached
    k = 0
    while True:
        try:
            elem = sess.run(next_element)
            elem = np.squeeze(elem, axis = 0)
            #elem = np.squeeze(elem, axis = 2)
            cv2.imwrite(output_directory+'/'+imFiles[k],elem)
            #plt.imshow(elem)
            k = k+1
            print(k)
            #print(elem)
        except tf.errors.OutOfRangeError:
            print("End of training dataset.")
            break
 """


if __name__ == '__main__':
    for imLoc in ['/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysBatch1Scaled/',
                  '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysBatch2Scaled/',
                  '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysLabelledScaled/']:
        histogram_norm_no_justu(imLoc = imLoc)
   #batch_normalise_no_jutsu(loc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/histNorm/1/',fullSet=True)
