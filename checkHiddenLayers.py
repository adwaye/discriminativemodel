import tensorflow as tf
import numpy as np
from checkTfrecords import check_size,count_data
from utils import update_dir
import shutil
import os
import cv2
import matplotlib.pyplot as plt

modelLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/trainedModel/9/savedModel'
im = cv2.imread('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/validationData/testIm.png',0)
im = cv2.resize(im,dsize=(500,500),fx=0.25,fy=0.25)
im = im.reshape((1,im.shape[0],im.shape[1],1))
im = im/np.max(im)
#sess = tf.InteractiveSession()
    # initialize the iterator on the training data
with tf.Session() as sess:
    print('starting prediction')
    #saver = tf.train.import_meta_graph(modelName + '.meta')
    ckpt = tf.train.get_checkpoint_state(modelLoc)
    #saver = tf.train.Saver()
    saver = tf.train.import_meta_graph(ckpt.model_checkpoint_path + '.meta')
    saver.restore(sess,ckpt.model_checkpoint_path)

    graph = tf.get_default_graph() #MARCUS I GET THE GRAPH
    ops = graph.get_operations() #MARCUS THESE ARE ALL THE OPS
    #Now, access the op that you want to run.
    x = graph.get_tensor_by_name('input/Patches:0')
    y_ = graph.get_tensor_by_name('input/y-input:0')
    #conv = graph.get_tensor_by_name('Conv1/conv/conv2d/convolution:0')
    #relu = graph.get_tensor_by_name('Conv1/conv/conv2d/Relu:0')
    #maxpool = graph.get_tensor_by_name('Conv1/Pool/max_pooling2d/MaxPool:0')
    bias1 = graph.get_tensor_by_name('conv2d/bias:0')
    bias1 = tf.expand_dims(tf.expand_dims(tf.expand_dims(bias1,0),0),0)
    kernel1 = graph.get_tensor_by_name('conv2d/kernel:0')

    kernel2 = graph.get_tensor_by_name('conv2d_1/kernel:0')
    bias2 = graph.get_tensor_by_name('conv2d_1/bias:0')
    bias2 = tf.expand_dims(tf.expand_dims(tf.expand_dims(bias2,0),0),0)

    kernel3 = graph.get_tensor_by_name('conv2d_2/kernel:0')
    bias3 = graph.get_tensor_by_name('conv2d_2/bias:0')
    bias3 = tf.expand_dims(tf.expand_dims(tf.expand_dims(bias3,0),0),0)

    strides = [1,1,1,1]
    padding = 'SAME'
    conv1 = tf.nn.relu(tf.add( bias1,tf.nn.conv2d(im.astype(np.float32),kernel1  ,strides=strides,
                                                       padding=padding ) )  )
    conv1 =  tf.layers.max_pooling2d(inputs=conv1
                            ,pool_size=[2,2]
                            ,strides=2
                            ,padding='valid')
    conv2 = tf.nn.relu(tf.add(bias2, tf.nn.conv2d(conv1,kernel2, strides=strides,padding=padding    ))  )
    conv2 =  tf.layers.max_pooling2d(inputs=conv2
                            ,pool_size=[2,2]
                            ,strides=2
                            ,padding='valid')
    conv3 = tf.nn.relu( tf.add(bias3,tf.nn.conv2d(conv2,kernel3, strides=strides,padding=padding   )))
    conv3 =     tf.layers.max_pooling2d(inputs=conv3
                            ,pool_size=[2,2]
                            ,strides=2
                            ,padding='valid')


    layer1 = sess.run(conv1)
    layer2 = sess.run(conv2)
    layer3 = sess.run(conv3)



fig, ax = plt.subplots(ncols=3,nrows=1)
k = 0
while True:
    ax[0].imshow(layer1[0,:,:,k%48])
    ax[0].set_title('conv1 iteration '+str(k%48))
    ax[1].imshow(layer2[0,:,:,k%64])
    ax[1].set_title('conv2 iteration '+str(k%64))
    ax[2].imshow(layer3[0,:,:,k%128])
    ax[2].set_title('conv3 iteration '+str(k%128))
    plt.pause(0.01)
    k+=1




