from tf_unet import unet, image_util, adnet4
import numpy as np
import matplotlib.pyplot as plt
import cv2
import scipy.io as sio


l_rate = 0.001
batch_size = 30
epochs     = 50
test_generator = image_util.AugmentedImageDataProvider(
    search_path='/home/amr62/Documents/tf_unet/trainingData/croppedFingers/Val/*',
    data_suffix=".png",
                                         mask_suffix='_mask.png', shuffle_data=True)
train_generator = image_util.AugmentedImageDataProvider(
    search_path='/home/amr62/Documents/tf_unet/trainingData/croppedFingers/Train/*', data_suffix=".png",
                                         mask_suffix='_mask.png', shuffle_data=True)
#x_test, y_test = generator(1)

net = unet.Unet(channels=train_generator.channels, n_class=train_generator.n_class, layers=3, features_root=16)
#trainer = unet.Trainer(net, optimizer="momentum", opt_kwargs=dict(momentum=0.2,learning_rate=l_rate,
# batch_size=batch_size))
#path = trainer.train_and_test(train_data_provider=train_generator,test_data_provider=test_generator,
#                              output_path="./unet_trained",
#                      training_iters=batch_size, epochs=50,
#                     display_step=20)


x_test = np.expand_dims(cv2.imread('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/histNorm/xRaysLabelled'
                                   '/CPSA0006h2011.png'),0)[:,400:800,400:800:,0:1]
prediction = net.predict("./unet_trained/model.ckpt", x_test)
plt.figure();plt.imshow(prediction[0,:,:,0])