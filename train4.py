import tensorflow as tf
from checkTfrecords import check_size,count_data
from utils import update_dir
import shutil
import os

#MARCUS FOR WEIGHTS AND BIASES, YOU BUILD A DISTRIBUTION USING MEAN, VARIANCE AND MAX/MIN I HAVE FUNCTION THAT DOES THAT
#MARCUS FOR ACUURACY AND SCALAR VALUED DIAGNOSTICS YOU USE A TF.SUMMARY.SCALAR
#MARCUS FOR A LAYER(TYPICALLY ) THE OUTPUT OF A WEIGHT+BIAS+ACTIVATION, YOU WANT A HISTOGRAM

# Number of classes is 2 (squares and triangles)
#nClass = 2



#hyperparamerteers

tf.reset_default_graph()
BATCH_SIZE = 200
N_EPOCHS = 50
CAPACITY = 400
DEQUEUE = 100
DROP_RATE1 = 0.4
text_file = open("./data/label.txt", "r")
lines  = text_file.readlines()
nClass = len(lines)



#cnn architecture
nFeatures2 = 48
shp2       = (17,17)
nFeatures3 = 64
shp3       = (8,8)
nFeatures4 = 128
shp4       = (4,4)
nNeuronsfc1 = 128
nNeuronsfc2 = 101


def getImage(filename):
    shape = check_size()
    DIMX_IM = shape[0]
    DIMY_IM = shape[1]
    # convert filenames to a queue for an input pipeline.
    filenameQ = tf.train.string_input_producer([filename],num_epochs=None)

    # object to read records
    recordReader = tf.TFRecordReader()

    # read the full set of features for a single example
    key,fullExample = recordReader.read(filenameQ)

    # parse the full example into its' component features.
    features = tf.parse_single_example(
        fullExample,
        features={
            'image/height':tf.FixedLenFeature([],tf.int64),
            'image/width':tf.FixedLenFeature([],tf.int64),
            'image/colorspace':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/channels':tf.FixedLenFeature([],tf.int64),
            'image/class/label':tf.FixedLenFeature([],tf.int64),
            'image/class/text':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/format':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/filename':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/encoded':tf.FixedLenFeature([],dtype=tf.string,default_value='')
            #'image/encoded': tf.VarLenFeature([], dtype=tf.string)
        })

    # now we are going to manipulate the label and image features

    label = features['image/class/label']
    image_buffer = features['image/encoded']

    # Decode the jpeg
    with tf.name_scope('decode_jpeg',[image_buffer],None):
        # decode
        image = tf.image.decode_jpeg(image_buffer,channels=1)

        # and convert to single precision data type
        image = tf.image.convert_image_dtype(image,dtype=tf.float32)#.set_shape([33,33,1])
        image.set_shape([DIMX_IM,DIMY_IM,1])

    # cast image into a single array, where each element corresponds to the greyscale
    # value of a single pixel.
    # the "1-.." part inverts the image, so that the background is black.
    #image = tf.reshape(1 - tf.image.rgb_to_grayscale(image),[DIMX_IM * DIMY_IM])

    image = tf.reshape(1 -image,[DIMX_IM * DIMY_IM])
    # re-define label as a "one-hot" vector
    # it will be [0,1] or [1,0] here.
    # This approach can easily be extended to more classes.
    label = tf.stack(tf.one_hot(label - 1,nClass))
    return label,image



def variable_summaries(var):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean',mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev',stddev)
        tf.summary.scalar('max',tf.reduce_max(var))
        tf.summary.scalar('min',tf.reduce_min(var))
        tf.summary.histogram('histogram',var)

def trainModel(trainFileName = "trainingDataCNN/train-00000-of-00001",testFileName="data/validation-00000-of-00001",
               path_to_save='./trainedModel/',N_EPOCHS=100,LEARNING_RATE=0.0005):
    shape = check_size(trainFileName)
    DIMX_IM = shape[0]
    DIMY_IM = shape[1]
    N_DATA = count_data(trainFileName)
    nSteps = int((N_DATA / BATCH_SIZE) * N_EPOCHS)
    #nSteps = 1000
    #nClass = 2
    #train data
    label,image = getImage(trainFileName)
    # and similarly for the validation data
    vlabel,vimage = getImage(testFileName)

    imageBatch,labelBatch = tf.train.shuffle_batch(
        [image,label],batch_size=BATCH_SIZE,
        capacity=CAPACITY,
        min_after_dequeue=DEQUEUE,
        num_threads=2,
        allow_smaller_final_batch=True)

    # and similarly for the validation data
    vimageBatch,vlabelBatch = tf.train.shuffle_batch(
        [vimage,vlabel],batch_size=BATCH_SIZE,
        capacity=CAPACITY,
        min_after_dequeue=DEQUEUE,
        num_threads=2,
        allow_smaller_final_batch=True)


    print("Running Convolutional Neural Network Model")

    with tf.name_scope('input'):
        x = tf.placeholder(tf.float32,[None,DIMY_IM* DIMX_IM],name='Patches')
        x_image = tf.reshape(x,[-1,DIMX_IM,DIMY_IM,1],name='Reshape_op')
        tf.summary.image('image',x_image,max_outputs=3)
        y_ = tf.placeholder(tf.float32,[None,nClass],name='y-input')



    #CONV LAYER 1
    with tf.name_scope('Conv2'):
        with tf.name_scope('conv'):
            h_conv2 = tf.layers.conv2d(
                    inputs=x_image
                    ,filters=nFeatures2
                    ,kernel_size=[5,5]
                    ,padding='same'
                    ,activation=tf.nn.relu
                )
            tf.summary.histogram('convolves',h_conv2)
        with tf.name_scope('Pool'):
            h_pool2 = tf.layers.max_pooling2d(inputs=h_conv2
                                              ,pool_size=[2,2]
                                              ,strides=2
                                              ,padding='valid')
            tf.summary.histogram('pools',h_pool2)

    #CONV LAYER 2
    with tf.name_scope('Conv3'):
        with tf.name_scope('conv'):
            h_conv3 = tf.layers.conv2d(
                inputs=h_pool2
                ,filters=nFeatures3
                ,kernel_size=[5,5]
                ,padding='same'
                ,activation=tf.nn.relu
            )
            tf.summary.histogram('convolves',h_conv3)
        with tf.name_scope('Pool'):
            h_pool3 = tf.layers.max_pooling2d(inputs=h_conv3
                                              ,pool_size=[2,2]
                                              ,strides=2
                                              ,padding='valid')
            tf.summary.histogram('pools',h_pool3)


    #CONV LAYER 3
    with tf.name_scope('Conv4'):
        with tf.name_scope('conv'):
            h_conv4 = tf.layers.conv2d(
                inputs=h_pool3
                ,filters=nFeatures4
                ,kernel_size=[5,5]
                ,activation=tf.nn.relu
                ,padding='same'
            )
            tf.summary.histogram('convolves',h_conv4)
        with tf.name_scope('Pool'):
            h_pool4 = tf.layers.max_pooling2d(inputs=h_conv4
                                              ,pool_size=[4,4]
                                              ,strides=1
                                              ,padding='valid')
            tf.summary.histogram('pools',h_pool4)

    with tf.name_scope('FC1'):
        h_pool4_flat = tf.reshape(h_pool4,[-1,nFeatures4])
        h_fc1 = tf.layers.dense(inputs=h_pool4_flat,units=nNeuronsfc1,activation=tf.nn.relu)
        tf.summary.histogram('neurons',h_fc1)

    # reduce overfitting by applying dropout
    # each neuron is kept with probability keep_prob

    with tf.name_scope('keep_probs'):
        keep_prob1 = tf.placeholder(tf.float32,name='FC1')

    # create readout layer which outputs to nClass categories
    with tf.name_scope('CL'):
        with tf.name_scope('pre-activations-dropout'):
            h_fc1_drop = tf.nn.dropout(h_fc1,keep_prob1)#,training=mode == tf.estimator.ModeKeys.TRAIN)
            tf.summary.histogram('dropout',h_fc1_drop)
        with tf.name_scope('final_pred'):
            y = tf.layers.dense(h_fc1_drop,units=nClass,activation=tf.nn.softmax,name='Softmax')

            # measure of error of our model
    # this needs to be minimised by adjusting W and b
    with tf.name_scope('cross_entropy'):
        cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y),reduction_indices=[1]))
        tf.summary.scalar('cross_entropy',cross_entropy)

    # define training step which minimises cross entropy
    with tf.name_scope('train'):
        train_step = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE).minimize(cross_entropy,
                                                                                  global_step=tf.train.get_global_step())

    # argmax gives index of highest entry in vector (1st axis of 1D tensor)
    with tf.name_scope('accuracy'):
        with tf.name_scope('correct_prediction'):
            correct_prediction = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
        with tf.name_scope('accuracy'):
            # get mean of all entries in correct prediction, the higher the better
            accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))
    tf.summary.scalar('accuracy',accuracy)

    # Merge all the summaries and write them out to
    # /tmp/tensorflow/mnist/logs/mnist_with_summaries (by default)
    #SAVE SHIT!!!!!!
    log_dir = update_dir(path_to_save)
    log_dir_board = os.path.join(log_dir , 'tensorboard')
    log_dir_save  = os.path.join(log_dir , 'savedModel/model')
    merged = tf.summary.merge_all()

    # run the session
    # start training and record shit
    # initialize the variables
    saver = tf.train.Saver(max_to_keep=4)
    config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
    with tf.Session(config=config) as sess:
        train_writer = tf.summary.FileWriter(log_dir_board + '/train',sess.graph)
        test_writer = tf.summary.FileWriter(log_dir_board + '/test')
        sess.run(tf.local_variables_initializer())
        sess.run(tf.global_variables_initializer())

        #MARCUS READ THAT SHIT YOU WILL GET IT, YOU'RE SMART
        # start the threads used for reading files
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess=sess,coord=coord)

        for i in range(nSteps):
            batch_xs,batch_ys = sess.run([imageBatch,labelBatch])
            if i % 100 == 0:  # Record summaries and test-set accuracy
                run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                run_metadata = tf.RunMetadata()
                vbatch_xs,vbatch_ys = sess.run([vimageBatch,vlabelBatch])
                summary,acc = sess.run([merged,accuracy],feed_dict={x:vbatch_xs,y_:vbatch_ys,keep_prob1:1.0 - DROP_RATE1})
                test_writer.add_summary(summary,i)
                print('Accuracy at step %s: %s' % (i,acc))
            else:  # Record train set summaries, and train
                if i % 100 == 99:  # Record execution stats
                    run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                    run_metadata = tf.RunMetadata()
                    summary,_ = sess.run([merged,train_step],
                                         feed_dict={x:batch_xs,y_:batch_ys,keep_prob1:1.0 - DROP_RATE1},
                                         options=run_options,
                                         run_metadata=run_metadata)
                    train_writer.add_run_metadata(run_metadata,'step%03d' % i)
                    train_writer.add_summary(summary,i)
                    print('Adding run metadata for',i)
                    if not os.path.isdir(log_dir + '/savedModel'): os.mkdir(log_dir + '/savedModel')

                else:  # Record a summary
                    summary,_ = sess.run([merged,train_step],feed_dict={x:batch_xs,y_:batch_ys,keep_prob1:1.0 - DROP_RATE1})
                    train_writer.add_summary(summary,i)

            if i % 1000 == 0:
                save_path = saver.save(sess,log_dir_save,global_step=i + 1)

        save_path = saver.save(sess,log_dir_save,global_step=i + 1)
        train_writer.close()
        test_writer.close()
        # finalise
        coord.request_stop()
        coord.join(threads)

    with open(log_dir_board + '/params.txt','a') as fp:
        fp.write('XDIM = ' + str(DIMX_IM) + '\n')
        fp.write('YDIM = ' + str(DIMY_IM) + '\n')
        fp.write('BATCH_SIZE = ' + str(BATCH_SIZE) + '\n')
        fp.write('N_EPOCHS = ' + str(N_EPOCHS) + '\n')
        fp.write('CAPACITY = ' + str(CAPACITY) + '\n')
        fp.write('DEQUEUE = ' + str(DEQUEUE) + '\n')
        fp.write('DROP_RATE = ' + str(DROP_RATE1) + '\n')
        fp.write('N_DATA = ' + str(N_DATA) + '\n')
        fp.write('nSteps = ' + str(nSteps)+ '\n')
        fp.write('nClass = ' + str(nClass))
        fp.close()

    shutil.copy('./data/label.txt',log_dir)

    return log_dir

if __name__=='__main__':
    log_dir = trainModel(LEARNING_RATE=0.0001,N_EPOCHS=30)
