import numpy as np
import cv2
import os
import matplotlib.pyplot as plt
plt.ion()
plt.rcParams['image.cmap']='gray'
from sklearn.cluster import KMeans
from sklearn.mixture import BayesianGaussianMixture
from copy import deepcopy


#using kmeans to find centre of bone from gorund truth bone masks

def my_kmeans(X,nClusters=4):
    N      = X.shape[0]

    init_coords = np.random.randint(low=0,high=X.shape[0],size=4)
    centers_new = X[init_coords,:]
    distances   = np.zeros((N,nClusters))
    for nrep in range(100):

        for k in range(nClusters):
            distances[:,k] = np.linalg.norm(X-centers_new[k,:],axis=1)

        clusters = np.argmin(distances,axis=1)
        centers_old = deepcopy(centers_new)

        for k in range(nClusters):
            centers_new[k,:] = np.mean(X[clusters == k],axis=0)

        error = np.linalg.norm(centers_new-centers_old)
    return centers_new



# and then you have it! 1-1 correspondence based on dim 0 indexing
#todo: asignment without replacement, so assign one centroid to one cluster, if that cluster is taken, then assign to
#  a different one
#k_means with regularisation
def my_kmeans_prior_vanilla(X,prior_means,alpha =0.5,nClusters=4):
    N = X.shape[0]
    centers_new = KMeans(n_clusters=4, random_state=0).fit(X).cluster_centers_
    centers_new_dist = np.linalg.norm(centers_new,axis=1)
    perm = np.argsort(centers_new_dist)
    centers_new = deepcopy(centers_new[perm])
    distances = np.zeros((N,nClusters))
    for nrep in range(500):
        centers_new_dist = np.linalg.norm(centers_new,axis=1)
        perm = np.argsort(centers_new_dist)
        centers_new = deepcopy(centers_new[perm])
        for k in range(nClusters):
            #distances[:,k] = np.linalg.norm(X - centers_new[k,:],axis=1)
            #prior_means_assigment = prior_means[np.argmin(np.linalg.norm(prior_means - centers_new,axis=1)) ]
            distances[:,k] = np.sqrt((1-alpha)*np.square(np.linalg.norm(X - centers_new[k,:],
                                                                 axis=1))+alpha*np.square(np.linalg.norm(
                X-prior_means[k,:],axis=1)))

        clusters = np.argmin(distances,axis=1)
        centers_old = deepcopy(centers_new)

        for k in range(nClusters):
            data_mean = np.mean(X[clusters == k],axis=0)
            centers_new[k,:] =(1-alpha)*data_mean+alpha*prior_means[np.argmin(np.linalg.norm(prior_means-data_mean,axis=1 ) )]
            #centers_new[k,:] = np.mean(X[clusters == k],axis=0)

        error = np.linalg.norm(centers_new - centers_old)
    return centers_new

prior_means = np.array([[ 351.772, 1362.22 ],
                        [ 533.946, 1416.87 ],
                        [ 830.63 , 1526.18 ],
                        [1296.47 , 1736.98 ]])


#def allocate_mean_to_prior(prior_means,centroids):







imName  = 'CPSA0004h2012.png'
maskLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/boneFill/all'
mask    = cv2.imread(os.path.join(maskLoc,imName),0)

points  = np.where(mask>0)
randomPoints = np.random.randint(low = 0,high = points[0].shape[0],size = (2000))
X =  np.concatenate( (np.expand_dims(points[0][randomPoints],1), np.expand_dims(points[1][randomPoints],1)) ,
                     axis = 1  )



priors = deepcopy(prior_means)








centers_new_prior = my_kmeans_prior_vanilla(X,prior_means,alpha = 0.5,nClusters=4)
plt.figure()
plt.imshow(mask)
#plt.plot(points[1][randomPoints],points[0][randomPoints],'.')

plt.scatter(X[:,1],X[:,0],marker='.',s=3)
plt.scatter(centers_new_prior[:, 1], centers_new_prior[:, 0],
            marker='x', s=50, linewidths=3,
            color='k', zorder=10)








#making the covariance matrices:
imName  = 'CPSA0004h2012.png'
maskLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/boneFill/'
n = 500
for bone in ['RH2MC','RH2PP','RH2MP','RH2DP']:
    loc  = os.path.join(maskLoc,bone)
    mask = cv2.imread(os.path.join(loc,imName),0)
    points = np.where(mask > 0)
    randomPoints = np.random.randint(low=0,high=points[0].shape[0],size=(n))
    X = np.concatenate((np.expand_dims(points[0][randomPoints],1),np.expand_dims(points[1][randomPoints],1)),
                       axis=1)
    X_mean = np.mean(X,axis = 0)
    cov_mat = (1/(n-1))*np.matmul(np.transpose(X-X_mean),X-X_mean)


maskLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/boneFill/all'
imName  = 'CPSA0004h2012.png'
mask    = cv2.imread(os.path.join(maskLoc,imName),0)
points  = np.where(mask>0)
randomPoints = np.random.randint(low = 0,high = points[0].shape[0],size = (2000))
X =  np.concatenate( (np.expand_dims(points[0][randomPoints],1), np.expand_dims(points[1][randomPoints],1)) ,
                     axis = 1  )

kmeans = KMeans(n_clusters=4, random_state=0).fit(X)

#kmeans.predict([[0, 0], [4, 4]])

centroids = kmeans.cluster_centers_

plt.figure()
plt.imshow(mask)
plt.plot(points[1][randomPoints],points[0][randomPoints],'.')
plt.scatter(centroids[:, 1], centroids[:, 0],
            marker='x', s=50, linewidths=3,
            color='b', zorder=10)








centers_new_vanilla = my_kmeans(X,mask=mask,nClusters=4)
plt.figure()
plt.imshow(mask)
#plt.plot(points[1][randomPoints],points[0][randomPoints],'.')
plt.scatter(centers_new[:,1],centers_new[:,0],
            marker='x',s=50,linewidths=3,
            color='b',zorder=10)
plt.scatter(centers_new_vanilla[:,1],centers_new_vanilla[:,0],
            marker='x',s=50,linewidths=3,
            color='r',zorder=10)

