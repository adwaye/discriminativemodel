import numpy as np
import tensorflow as tf
from utils import _run
height  = 32
width   = 32
DIMX_IM = 15
DIMY_IM = 15
BATCH_SIZE = 10
shapeMatx= (1,1,width,DIMX_IM)
shapeMaty= (1,1,height,DIMY_IM)
shapeVec= (BATCH_SIZE,DIMX_IM,DIMY_IM,1)
dType = tf.float32


def train_reshape(b,Wx,Wy):
     Wx = tf.tile(Wx,[BATCH_SIZE,DIMY_IM,1,1]) # need to have first 2 dims equal in matmul
     c  = tf.matmul(Wx,b)                   # dim [1,DIMY_IM,WIDTH,1]
     d  = tf.transpose(c,[0,2,1,3])          # dim [1,WIDTH,DIMY_IM,1]
     Wy = tf.tile(Wy,[BATCH_SIZE,height,1,1])
     return tf.nn.sigmoid(tf.matmul(Wy,d))

#Wxx  = np.random.random(shapeMat).astype(np.float32)#np.repeat(np.random.random(shapeMat),repeats=DIMY_IM,axis=1)

Wxx  = np.repeat(np.random.random(shapeMatx),repeats=DIMY_IM,axis=1).astype(np.float32)
Wyy  = np.repeat(np.random.random(shapeMaty),repeats=DIMY_IM,axis=1).astype(np.float32)
bArr = np.random.random(shapeVec).astype(np.float32)
Wx   = tf.constant(np.random.random(shapeMatx),dtype=dType)
Wy   = tf.constant(np.random.random(shapeMaty),dtype=dType)
b    = tf.constant(bArr,dtype=dType)




#i=1;l=3;np.sum(Wxx[i,:]*bArr[:,0])==cArr[i,0]
#i=1;l=3;np.sum(Wxx[0,l,i,:]*bArr[0,l,:,0])==cArr[0,l,i,0]

res = train_reshape(b,Wx,Wy)
