#from __future__ import absolute_import
#from __future__ import division
#from __future__ import print_function
import numpy as np
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.INFO) # set verbosity (to see certain outputs later).

## Load data
mnist = tf.contrib.learn.datasets.load_dataset("mnist")
train_data = mnist.train.images # Returns np.array of type np.float32
train_labels = np.asarray(mnist.train.labels, dtype=np.int32)
eval_data = mnist.test.images
eval_labels = np.asarray(mnist.test.labels, dtype=np.int32)


def add_conv2d(net,filters):
    with tf.name_scope('conv_plus_pool'):
        net = tf.layers.conv2d(inputs=net,filters=filters,kernel_size=[5,5],
                               padding='same',activation=tf.nn.relu)
        net = tf.layers.max_pooling2d(inputs=net,pool_size=[2,2],strides=2)
    return net


def add_dense(net,units):
    with tf.name_scope('fc_layer'):
        net = tf.layers.dense(inputs=net,units=units,activation=tf.nn.relu)
    return net


def cnn_model_fn(features,labels,mode,params):
    net = tf.reshape(features['x'],[-1,28,28,1]) # input layer

    for filters in params['conv_layers']:
        net = add_conv2d(net,filters)

    net = tf.reshape(net,[-1,7 * 7 * params['conv_layers'][-1]]) # flatten layer

    for units in params['dense_layers']:
        net = add_dense(net,units)

    with tf.name_scope('dropout_plus_logits'):
        net = tf.layers.dropout(inputs=net,rate=params["dropout"][0],
                                training=(mode == tf.estimator.ModeKeys.TRAIN))
        logits = tf.layers.dense(inputs=net,units=10) # final output layer of shape [-1,10]

    with tf.name_scope('LOSS'):
        loss = tf.losses.sparse_softmax_cross_entropy(labels=labels,logits=logits)
        tf.summary.scalar('loss',loss)
    with tf.name_scope('ACCURACY'):
        acc = tf.metrics.accuracy(labels=labels,predictions=tf.argmax(input=logits,axis=1))
        tf.summary.scalar('accuracy',acc[1])

    pred = {'classes':tf.argmax(input=logits,axis=1,name='max_likelihood'),
            'probabilities':tf.nn.softmax(logits,name='softmax_tensor')}

    #    merged = tf.summary.merge_all()
    #    writer = tf.summary.FileWriter('/tmp/mnist_convnet_model/train', graph=tf.get_default_graph())

    # Define the three modes: PREDICT, EVAL and TRAIN
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode,predictions=pred)

    if mode == tf.estimator.ModeKeys.EVAL:
        return tf.estimator.EstimatorSpec(mode=mode,loss=loss,eval_metric_ops={"accuracy":acc})

    assert mode == tf.estimator.ModeKeys.TRAIN
    with tf.name_scope('train'):
        op = tf.train.AdamOptimizer(learning_rate=params['learning_rate'][0])
        train_op = op.minimize(loss=loss,global_step=tf.train.get_global_step())

    return tf.estimator.EstimatorSpec(mode=mode,loss=loss,train_op=train_op)


## Create classifier
params = {'conv_layers': [32,64], 'dense_layers': [256], 'dropout': [0.4], 'learning_rate': [1e-3]}
run_config = tf.estimator.RunConfig(save_summary_steps=30,
                                    )

clf = tf.estimator.Estimator(model_fn=cnn_model_fn, model_dir='/tmp/mnist_convnet_model', params=params,config=run_config)

## Train classifier
train_input_fn = tf.estimator.inputs.numpy_input_fn(x={"x": train_data}, y=train_labels,
                                                    batch_size=128, num_epochs=None, shuffle=True)
clf.train(input_fn=train_input_fn, steps=500)



# Evaluate the model and print results
eval_input_fn = tf.estimator.inputs.numpy_input_fn(x={'x': eval_data}, y=eval_labels,
                                                   num_epochs=1, shuffle=False)
eval_results = clf.evaluate(input_fn=eval_input_fn)
print(eval_results)