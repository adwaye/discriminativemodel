from _data import *
import os
import cv2
from checkTfrecords import *
from scipy.interpolate import griddata
from utils import _run


im = cv2.imread('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/validationData'
                           '/testIm'
                       '.png',0)


im = np.reshape(im,(1,im.shape[0],im.shape[1],1))

patchSize = 100
ksizes    = [1,patchSize,patchSize,1]
strides   = [1,100,100,1]
rates     = [1,1,1,1]


tf_patches = tf.extract_image_patches(im,ksizes=ksizes,strides=strides,rates=rates,padding='SAME')
xdim = tf_patches.shape[1].value
ydim = tf_patches.shape[2].value
tf_patches_resh = tf.reshape(tf_patches,[xdim*ydim,patchSize**2])

with tf.Session() as sess:
    patchCol = sess.run( tf.reshape(tf_patches_resh,[-1,patchSize,patchSize,1],name='Reshape_op'))

