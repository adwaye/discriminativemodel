import sys
sys.path.append('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/')
from _data import *
import os
import cv2
from checkTfrecords import *
from scipy.interpolate import griddata
from utils import _run


height = 32
width = 32
nClass = 2
shape = check_size()
DIMX_IM = shape[0]
DIMY_IM = shape[1]
PRE_RESIZE = False
BATCH_SIZE = 100
N_EPOCHS = 1
CAPACITY = 250
DEQUEUE = 10
DROP_RATE = 0.4
N_DATA = count_data()
nSteps = BATCH_SIZE * N_EPOCHS



def parser(fullExample):
    # parse the full example into its' component features.
    features = tf.parse_single_example(
        fullExample,
        features={
            'image/height':tf.FixedLenFeature([],tf.int64),
            'image/width':tf.FixedLenFeature([],tf.int64),
            'image/colorspace':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/channels':tf.FixedLenFeature([],tf.int64),
            'image/class/label':tf.FixedLenFeature([],tf.int64),
            'image/class/text':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/format':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/filename':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/encoded':tf.FixedLenFeature([],dtype=tf.string,default_value='')
            #'image/encoded': tf.VarLenFeature([], dtype=tf.string)
        })

    # now we are going to manipulate the label and image features

    label = features['image/class/label']
    image_buffer = features['image/encoded']

    # Decode the jpeg
    # decode
    image = tf.image.decode_jpeg(image_buffer,channels=1)

    # and convert to single precision data type
    image = tf.image.convert_image_dtype(image,dtype=tf.float32)#.set_shape([33,33,1])
    image.set_shape([DIMX_IM,DIMY_IM,1])

    # cast image into a single array, where each element corresponds to the greyscale
    # value of a single pixel.
    # the "1-.." part inverts the image, so that the background is black.


    image = tf.reshape(1 - tf.image.rgb_to_grayscale(image),[DIMX_IM * DIMY_IM])
    # re-define label as a "one-hot" vector
    # it will be [0,1] or [1,0] here.
    # This approach can easily be extended to more classes.
    label = tf.stack(tf.one_hot(label - 1,nClass))
    #image.set_shape([height*width,1])
    #label = tf.cast(features['image/class/label'], tf.int32)
    return label,image




def input_parser(img_path):
        # convert the label to one-hot encoding
        label = 1
        one_hot = tf.one_hot(label, 2)

        # read the img from file
        img_file = tf.read_file(img_path)
        img_decoded = tf.image.decode_jpeg(img_file,channels=1)
        #img_decoded = tf.cast(tf.image.decode_image(img_file,channels=1),tf.float32)
        image = tf.image.convert_image_dtype(img_decoded,dtype=tf.float32)#.set_shape([33,33,1])
        image.set_shape([DIMX_IM,DIMY_IM,1])
        image = tf.reshape(1 - tf.image.rgb_to_grayscale(image),[DIMX_IM*DIMY_IM])

        return one_hot, image


def data_iterator(train_imgs):
    #dataset = tf.data.TFRecordDataset(fileName)
    dataset = tf.data.Dataset.from_tensor_slices((train_imgs))
    dataset = dataset.map(input_parser)#,num_threads=2,output_buffer_size=batch_size)
    dataset = dataset.batch(batch_size=BATCH_SIZE)
    dataset = dataset.repeat()
    iterator= dataset.make_one_shot_iterator()
    #labels, features = iterator.get_next()
    return iterator


def patch_one_im(imLoc,patchSize=17,height=32,width=32,plot=False):
    im = cv2.cvtColor(cv2.imread(imLoc),cv2.COLOR_BGR2GRAY)
    xDim = im.shape[0]
    yDim = im.shape[1]
    start = patchSize - 1
    midPointsY = np.arange(start=start,stop=yDim,step=patchSize)
    midPointsX = np.arange(start=start,stop=xDim,step=patchSize)
    k = 0

    if plot:
        sess = tf.InteractiveSession()
        fig,axes = plt.subplots(nrows=1,ncols=2)
        axes[0].set_title('np reshape')
        axes[1].set_title('tf reshape')
    for x in midPointsX:
        for y in midPointsY:
            patch = patchAtMidCoords(1-im[:,:],x,y,patchSize)
            if plot: patchTf = sess.run(
                tf.reshape(tf.image.resize_images(np.expand_dims(patch,2),[width,height]),[width * height]))
            if patch.shape[0] * patch.shape[1] == patchSize * patchSize:
                if PRE_RESIZE:
                    patch = cv2.resize(patch,(width,height))
                    patch = patch.reshape(width * height)
                else:
                    patch = patch.reshape(patchSize**2)
                if k == 0:
                    patchCol = np.expand_dims(patch,0)
                    coords = np.array([[x,y]])
                else:
                    patchCol = np.concatenate((patchCol,np.expand_dims(patch,0)),axis=0)
                    coords = np.concatenate((coords,np.array([[x,y]])),axis=0)
                if plot:
                    print('plotting')
                    if PRE_RESIZE:
                        axes[0].imshow(patch.reshape((width,height)))
                        axes[1].imshow(patchTf.reshape((width,height)))
                    else:
                        axes[0].imshow(patch.reshape((patchSize,patchSize)))
                        axes[1].imshow(patchTf.reshape((patchSize,patchSize)))
                    #plt.title('lab= '+str(lab)+' example '+str(k) )
                    plt.show()
                    plt.pause(0.01)
            k += 1
    if plot: sess.close()
    return im,coords,patchCol


#samping function to test turn_into_predictions

def sample_one_im(imLoc,patchSize=17):
    im = cv2.cvtColor(cv2.imread(imLoc),cv2.COLOR_BGR2GRAY)
    xDim = im.shape[0]
    yDim = im.shape[1]
    start = patchSize - 1
    midPointsY = np.arange(start=start,stop=yDim,step=patchSize)
    midPointsX = np.arange(start=start,stop=xDim,step=patchSize)
    k = 0
    for x in midPointsX:
        for y in midPointsY:
            if k == 0:
                vals = im[x:x + 1,y]
                coords = np.array([[x,y]])
            else:
                vals = np.concatenate((vals,im[x:x + 1,y]),axis=0)
                coords = np.concatenate((coords,np.array([[x,y]])),axis=0)

            k += 1
    return im,coords,vals


#turns the predictions into a potential

def tf_sample_dense_batches(imLoc,patchSize=17,plot=False):
    strides   = 1
    im = np.expand_dims(np.expand_dims(1- cv2.cvtColor(cv2.imread(imLoc), cv2.COLOR_BGR2GRAY),0),3)
    """
    xDim = im.shape[1]
    yDim = im.shape[2]
    fullX = np.arange(start=0,stop=xDim,step=1)
    fullY = np.arange(start=0,stop=yDim,step=1)
    grid_y,grid_x = np.meshgrid(fullY,fullX)
    coords = np.expand_dims( np.concatenate(
                            (np.expand_dims( grid_x,2  ),
                             np.expand_dims( grid_y,2 ) )
                ,axis=2),axis=0)

    tf_coords  = tf.extract_image_patches(   coords , ksizes=[1,1,1,1],
                                           strides=[1,strides,strides,1],rates=[1,1,1,1],padding='VALID' )
    """
    tf_patches = tf.extract_image_patches(   im , ksizes=[1,patchSize,patchSize,1],
                                           strides=[1,strides,strides,1],rates=[1,1,1,1],padding='VALID' )
    tf_patches_resh = tf.reshape(tf_patches,[tf_patches.shape[1]*tf_patches.shape[2],tf_patches.shape[3]])

    #patches = patches.reshape((1,patches.shape[1]*patches.shape[2],patchSize*patchSize))
    if plot:
        patches = _run(tf_patches_resh)
        fig,axes = plt.subplots(nrows=1,ncols=1)
        for k in range(patches.shape[1]):
                print('plotting')
                axes.imshow( patches[0,k,:].reshape(  (patchSize,patchSize) ) )
                #axes[1].imshow( patchTf.reshape(  (width,height) ) )
                #plt.title('lab= '+str(lab)+' example '+str(k) )
                plt.show()
                plt.pause(0.01)
        #k+=1
    dummyY = np.random.random((tf_patches_resh.shape[0].value,2))
    patchBatch,labelBatch = tf.train.batch(
        [tf_patches_resh,dummyY],batch_size=BATCH_SIZE,
        capacity=CAPACITY,
        #min_after_dequeue=DEQUEUE,
        num_threads=2,
        enqueue_many=True,
        allow_smaller_final_batch=True)

    if tf_patches_resh.shape[0].value%BATCH_SIZE==0:
        nBatch = int(tf_patches_resh.shape[0].value/BATCH_SIZE)
    else:
        nBatch = int(tf_patches_resh.shape[0].value / BATCH_SIZE) + 1
    return patchBatch, labelBatch, nBatch


def tf_sample_dense(imLoc,patchSize=17,plot=False):
    strides   = 1
    im = np.expand_dims(np.expand_dims(1- cv2.cvtColor(cv2.imread(imLoc), cv2.COLOR_BGR2GRAY),0),3)
    """
    xDim = im.shape[1]
    yDim = im.shape[2]
    fullX = np.arange(start=0,stop=xDim,step=1)
    fullY = np.arange(start=0,stop=yDim,step=1)
    grid_y,grid_x = np.meshgrid(fullY,fullX)
    coords = np.expand_dims( np.concatenate(
                            (np.expand_dims( grid_x,2  ),
                             np.expand_dims( grid_y,2 ) )
                ,axis=2),axis=0)

    tf_coords  = tf.extract_image_patches(   coords , ksizes=[1,1,1,1],
                                           strides=[1,strides,strides,1],rates=[1,1,1,1],padding='VALID' )
    """
    tf_patches = tf.extract_image_patches(   im , ksizes=[1,patchSize,patchSize,1],
                                           strides=[1,strides,strides,1],rates=[1,1,1,1],padding='VALID' )
    tf_patches_resh = tf.reshape(tf_patches,[tf_patches.shape[1]*tf_patches.shape[2],tf_patches.shape[3]])

    #patches = patches.reshape((1,patches.shape[1]*patches.shape[2],patchSize*patchSize))
    #patches = _run(tf_patches_resh)
    if plot:

        fig,axes = plt.subplots(nrows=1,ncols=1)
        for k in range(patches.shape[1]):
                print('plotting')
                axes.imshow( patches[0,k,:].reshape(  (patchSize,patchSize) ) )
                #axes[1].imshow( patchTf.reshape(  (width,height) ) )
                #plt.title('lab= '+str(lab)+' example '+str(k) )
                plt.show()
                plt.pause(0.01)
        #k+=1
    dummyY = np.random.random((tf_patches_resh.shape[0].value,2))
    patchBatch,labelBatch = tf.train.batch(
        [tf_patches_resh,dummyY],batch_size=BATCH_SIZE,
        capacity=CAPACITY,
        #min_after_dequeue=DEQUEUE,
        num_threads=2,
        allow_smaller_final_batch=True)

    if tf_patches_resh.shape[0].value%BATCH_SIZE==0:
        nBatch = int(tf_patches_resh.shape[0].value/BATCH_SIZE)
    else:
        nBatch = int(tf_patches_resh.shape[0].value / BATCH_SIZE) + 1
    return tf_patches_resh




def turn_into_potential(preds,coords,im,method):
    xDim = im.shape[0]
    yDim = im.shape[1]
    fullX = np.arange(start=0,stop=xDim,step=1)
    fullY = np.arange(start=0,stop=yDim,step=1)
    grid_x,grid_y = np.meshgrid(fullX,fullY)
    grid_z0 = griddata(coords,preds,(grid_x,grid_y),method=method)
    return grid_z0


def predict_from_patchCol(patchCol,batch_size = 200):
    N = patchCol.shape[0]
    yTest = np.random.random((N,2))
    preds = np.zeros((N,2))
    with tf.Session() as sess:
        # initialize the iterator on the training data
        print('starting prediction')
        saver = tf.train.import_meta_graph(modelName + '.meta') #MARCUS I LOAD MY SAVED MODEL
        saver.restore(sess,tf.train.latest_checkpoint(modelLoc)) #MARCUS I RESTORE THE GMODEL
        graph = tf.get_default_graph() #MARCUS I GET THE GRAPH
        ops = graph.get_operations() #MARCUS THESE ARE ALL THE OPS
        #Now, access the op that you want to run.
        #I NEED PLACEHOLDERS AS ALL THE OPS DEPEND ON THEM, THEY ARE IN A WAY THE INPUT TO MY GRAPH
        x = graph.get_tensor_by_name('input/Patches:0') #MARCUS THIS IS MY INPUT PLACEHOLDER
        y_ = graph.get_tensor_by_name('input/y-input:0') #MARCUS SAME SHIT HERE PLACEHOLDE
        keep_prob2 = graph.get_tensor_by_name('keep_probs/FC2:0')
        try:
            pred = graph.get_tensor_by_name('CL/final_pred/dense/Softmax:0') #MARCUS THIS IS THE PREDICTED CLASS
        except KeyError:
            pred = graph.get_tensor_by_name('CL/final_pred/Softmax:0') #MARCUS THIS IS THE PREDICTED CLASS
        keep_prob1 = graph.get_tensor_by_name('keep_probs/FC1:0')
        #we dont need true predictions for actual predictions
        # and similarly for the validation data
        nBatch = int(N/batch_size)
        for i in range(nBatch+1):
            print(i)
            if i<nBatch:
                preds[i*batch_size:(i+1)*batch_size,:] = sess.run(pred,feed_dict={x:patchCol[i*batch_size:(i+1)*batch_size,:]
                                                    ,y_:yTest[i*batch_size:(i+1)*batch_size,:]
                                                    ,keep_prob1:1.0 - 0.4
                                                    ,keep_prob2:1.0 - 0.4}) #RUN THE LAST LAYER TO GET PREDS
            else:
                try:
                    preds[nBatch*i:,:] = sess.run(pred,feed_dict={x:patchCol[i*nBatch:,:]
                                                    ,y_:yTest[i*batch_size:(i+1)*batch_size,:]
                                                    ,keep_prob1:1.0 - 0.4
                                                    ,keep_prob2:1.0 - 0.4}) #RUN THE LAST LAYER TO GET PREDS
                except IndexError:
                    print('nor more')
    return preds



def predict_from_imloc(loc):
    train_imgs = sorted([os.path.join(loc,file) for file in os.listdir(loc)],key=os.path.getctime,reverse=False)
    N = len(train_imgs)
    nBatch = int(N / BATCH_SIZE) + 1 * (N % BATCH_SIZE)
    preds = np.zeros([])
    #config = tf.ConfigProto(gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction =0.75))
    config = tf.ConfigProto()
    config.gpu_options.allocator_type = 'BFC'
    eval_iterator = data_iterator(train_imgs)
    with tf.Session(config=config) as sess:
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess=sess,coord=coord)

        # initialize the iterator on the training data
        print('starting prediction')
        saver = tf.train.import_meta_graph(modelName + '.meta')
        saver.restore(sess,tf.train.latest_checkpoint(modelLoc))
        graph = tf.get_default_graph()
        ops = graph.get_operations()
        #Now, access the op that you want to run.

        handle = graph.get_tensor_by_name('jamrocks:0')
        try:
            prediction = graph.get_tensor_by_name('CL/final_pred/dense/Softmax:0')
        except KeyError:
            prediction = graph.get_tensor_by_name('CL/final_pred/Softmax/Softmax_2:0')

        keep_prob1 = graph.get_tensor_by_name('keep_probs/FC1:0')
        keep_prob2 = graph.get_tensor_by_name('keep_probs/FC2:0')
        #y          = graph.get_tensor_by_name('final_pred/Softmax:0')
        sess.run(tf.local_variables_initializer())
        sess.run(tf.global_variables_initializer())
        #we dont need true predictions for actual predictions
        # and similarly for the validation data

        for i in range(nBatch):
            #xTest,yTest = sess.run([patchBatch,labelBatch])
            print(i)
            #"""
            eval_iterator_handle = sess.run(eval_iterator.string_handle())
            pred = sess.run(prediction,feed_dict={handle:eval_iterator_handle,
                                                  keep_prob1:1.0 - 0.4,
                                                  keep_prob2:1.0 - 0.4}) #RUN THE LAST LAYER TO GET PREDS
            if i ==0:
                preds = pred
            else:
                preds = np.append(preds,pred,axis=0)
            #"""
        coord.request_stop()
        coord.join(threads)

    return preds

if __name__ == '__main__':
    imLoc = "./validationData/testIm.png"
    modelLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/trainedModel/21/savedModel'
    Niter = 5000
    modelName = modelLoc + '/model-10001' #+ str(Niter - (Niter % 1000) + 1)

    print('predicting shit')
    evalFileName = "./data/eval-00000-of-00001"

    nPatch = count_data(evalFileName)
    nReps = int(nPatch / BATCH_SIZE) + 1 * (nPatch % BATCH_SIZE)
    preds = predict_from_loc(loc='./validationData/1/True/')
    cropIm = cv2.imread("./validationData/testIm.png")
    coords = np.loadtxt('./validationData/1/coords.txt')
    grid_z0 = turn_into_potential(preds[:,0],coords,cropIm,method='nearest')
    plt.figure()
    plt.imshow(grid_z0)


