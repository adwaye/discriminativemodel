import sys
sys.path.append('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/')
from _data import *
import os
import cv2
from checkTfrecords import *
from scipy.interpolate import griddata
from utils import _run


height=32
width=32
nClass =2
shape = check_size()
DIMX_IM = shape[0]
DIMY_IM = shape[1]
PRE_RESIZE = True
BATCH_SIZE = 100
N_EPOCHS  = 100
CAPACITY  = 300
DEQUEUE   = 50
DROP_RATE = 0.4
N_DATA    = count_data() 
nSteps    = BATCH_SIZE*N_EPOCHS

imLoc = "/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/histNorm/1/NPSA0017h2012.png"
modelLoc  = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/trainedModel/3/tensorboardsavedModel'
Niter     = 9000
modelName = modelLoc + '/model-'+str(Niter-(Niter%1000)+1)

def patch_one_im(imLoc,patchSize=17,height=32,width=32,plot=False):
    im = cv2.cvtColor(cv2.imread(imLoc), cv2.COLOR_BGR2GRAY)
    xDim = im.shape[0]
    yDim = im.shape[1]
    start = patchSize-1
    midPointsY = np.arange(start=start,stop =yDim,step=patchSize)
    midPointsX = np.arange(start=start,stop =xDim,step=patchSize)
    k = 0
    
    if plot: 
        sess = tf.InteractiveSession()
        fig,axes = plt.subplots(nrows=1,ncols=2)
        axes[0].set_title('np reshape')
        axes[1].set_title('tf reshape')
    for x in midPointsX:
      for y in midPointsY:
        patch   = patchAtMidCoords(im[:,:],x,y,patchSize)
        if plot: patchTf = sess.run(tf.reshape(tf.image.resize_images(np.expand_dims(patch,2),[width,height] ),[width*height]) ) 
        if patch.shape[0]*patch.shape[1]==patchSize*patchSize:
          patch = cv2.resize(patch,(width,height))
          patch = patch.reshape(width*height)
          if k==0:
             patchCol = np.expand_dims(patch,0)
             coords = np.array([[x,y]])
          else:
             patchCol = np.concatenate((patchCol,np.expand_dims(patch,0)),axis =0)
             coords   = np.concatenate((coords,np.array([[x,y]])),axis=0)
          if plot:
                print('plotting')
                axes[0].imshow( patch.reshape(  (width,height) ) )
                axes[1].imshow( patchTf.reshape(  (width,height) ) )
                #plt.title('lab= '+str(lab)+' example '+str(k) )
                plt.show()
                plt.pause(0.01)
        k+=1
    if plot: sess.close()
    return im,coords, 1-patchCol


#samping function to test turn_into_predictions

def sample_one_im(imLoc,patchSize=17):
    im = cv2.cvtColor(cv2.imread(imLoc),cv2.COLOR_BGR2GRAY)
    xDim = im.shape[0]
    yDim = im.shape[1]
    start = patchSize - 1
    midPointsY = np.arange(start=start,stop=yDim,step=patchSize)
    midPointsX = np.arange(start=start,stop=xDim,step=patchSize)
    k = 0
    for x in midPointsX:
      for y in midPointsY:
         if k == 0:
           vals = im[x:x+1,y]
           coords = np.array([[x,y]])
         else:
           vals = np.concatenate((vals,im[x:x+1,y]),axis=0)
           coords   = np.concatenate((coords,np.array([[x,y]])),axis=0)

         k += 1
    return im,coords,vals

#turns the predictions into a potential
def tf_sample_one_im(imLoc,patchSize=17,strides=1,plot=False):
    plot=True
    patchSize = 500
    strides   = 500
    im = np.expand_dims(np.expand_dims(1- cv2.cvtColor(cv2.imread(imLoc), cv2.COLOR_BGR2GRAY),0),3)

    tf_patches = tf.extract_image_patches(   im , ksizes=[1,patchSize,patchSize,1],
                                           strides=[1,strides,strides,1],rates=[1,1,1,1],padding='VALID' )
    tf_patches_resh = tf.reshape(tf_patches,[tf_patches.shape[1]*tf_patches.shape[2],tf_patches.shape[3]])
    patches = _run(tf_patches_resh)

    #patches = patches.reshape((1,patches.shape[1]*patches.shape[2],patchSize*patchSize))
    if plot:
        fig,axes = plt.subplots(nrows=1,ncols=1)
        for k in range(patches.shape[1]):
                print('plotting')
                axes.imshow( patches[0,k,:].reshape(  (patchSize,patchSize) ) )
                #axes[1].imshow( patchTf.reshape(  (width,height) ) )
                #plt.title('lab= '+str(lab)+' example '+str(k) )
                plt.show()
                plt.pause(0.01)
        #k+=1
    if plot: sess.close()
    return tf.patches


def turn_into_potential(preds,coords,im,method):
    xDim = im.shape[0]
    yDim = im.shape[1]
    fullX = np.arange(start=0,stop=xDim,step=1)
    fullY = np.arange(start=0,stop=yDim,step=1)
    grid_x,grid_y = np.meshgrid(fullX,fullY)
    grid_z0 = griddata(coords,preds,(grid_x,grid_y),method=method)
    return grid_z0




def predict_from_patchCol(patchCol):
    N = patchCol.shape[0]
    yTest = np.random.random((1,2))
    preds = np.zeros((N,2))
    with tf.Session() as sess:
        # initialize the iterator on the training data
        print('starting prediction')
        saver = tf.train.import_meta_graph(modelName + '.meta') #MARCUS I LOAD MY SAVED MODEL
        saver.restore(sess,tf.train.latest_checkpoint(modelLoc)) #MARCUS I RESTORE THE GMODEL
        graph = tf.get_default_graph() #MARCUS I GET THE GRAPH
        ops = graph.get_operations() #MARCUS THESE ARE ALL THE OPS
        #Now, access the op that you want to run.
        #I NEED PLACEHOLDERS AS ALL THE OPS DEPEND ON THEM, THEY ARE IN A WAY THE INPUT TO MY GRAPH
        x = graph.get_tensor_by_name('input/Patches:0') #MARCUS THIS IS MY INPUT PLACEHOLDER
        y_ = graph.get_tensor_by_name('input/y-input:0') #MARCUS SAME SHIT HERE PLACEHOLDE
        pred = graph.get_tensor_by_name('CL/final_pred/Softmax:0') #MARCUS THIS IS THE PREDICTED CLASS
        keep_prob = graph.get_tensor_by_name('Placeholder:0')
         #we dont need true predictions for actual predictions
        # and similarly for the validation data
        for i in range(N):
            print(i)
            preds[i,:] = sess.run(pred,feed_dict={x:patchCol[i:i+1,:],y_:yTest,keep_prob:1.0-0.4}) #RUN THE LAST LAYER TO GET PREDS
    return preds


def predict_from_imLoc(loc):
    train_imgs = sorted([os.path.join(loc,file) for file in os.listdir(loc)],key=os.path.getctime,reverse=False)
    N = len(train_imgs)
    nBatch = int(N / BATCH_SIZE) + 1 * (N % BATCH_SIZE)
    def input_parser(img_path):
        # convert the label to one-hot encoding
        #one_hot = tf.one_hot(label, NUM_CLASSES)

        # read the img from file
        img_file = tf.read_file(img_path)
        img_decoded = tf.image.decode_jpeg(img_file,channels=1)
        #img_decoded = tf.cast(tf.image.decode_image(img_file,channels=1),tf.float32)
        image = tf.image.convert_image_dtype(img_decoded,dtype=tf.float32)#.set_shape([33,33,1])
        image.set_shape([DIMX_IM,DIMY_IM,1])
        image = tf.image.resize_images(image,[height,width],method=tf.image.ResizeMethod.BICUBIC)
        image = tf.reshape(1 - tf.image.rgb_to_grayscale(image),[height * width])

        return image

    tr_data = tf.data.Dataset.from_tensor_slices((train_imgs))
    tr_data = tr_data.map(input_parser).batch(batch_size=BATCH_SIZE).repeat()
    iterator= tr_data.make_one_shot_iterator()
    next_element = iterator.get_next()

    with tf.Session() as sess:
        # initialize the iterator on the training data
        print('starting prediction')
        saver = tf.train.import_meta_graph(modelName + '.meta') #MARCUS I LOAD MY SAVED MODEL
        saver.restore(sess,tf.train.latest_checkpoint(modelLoc)) #MARCUS I RESTORE THE GMODEL
        graph = tf.get_default_graph() #MARCUS I GET THE GRAPH
        ops = graph.get_operations() #MARCUS THESE ARE ALL THE OPS
        #Now, access the op that you want to run.
        #I NEED PLACEHOLDERS AS ALL THE OPS DEPEND ON THEM, THEY ARE IN A WAY THE INPUT TO MY GRAPH
        x = graph.get_tensor_by_name('input/Patches:0') #MARCUS THIS IS MY INPUT PLACEHOLDER
        y_ = graph.get_tensor_by_name('input/y-input:0') #MARCUS SAME SHIT HERE PLACEHOLDE
        pred = graph.get_tensor_by_name('CL/final_pred/Softmax:0') #MARCUS THIS IS THE PREDICTED CLASS
        keep_prob = graph.get_tensor_by_name('Placeholder:0')
         #we dont need true predictions for actual predictions
        # and similarly for the validation data
        for i in range(nBatch):
            try:
                print(i)
                xTest = sess.run(next_element)
                if i == 0:
                    preds = sess.run(pred,feed_dict={x:xTest,y_:np.random.random((xTest.shape[0],2)),
                                                      keep_prob:1.0-0.4})
                else:
                    preds = np.concatenate( (preds,sess.run(pred,feed_dict={x:xTest,y_:np.random.random((xTest.shape[
                                                                                                             0],2)),
                                                      keep_prob:1.0-0.4}) ) ,axis=0)
            except tf.errors.OutOfRangeError:
                print("End of training dataset.")
                break
    return preds

        
if __name__=='__main__':
    print('making patches')
    #im,coords,patchCol = patch_one_im(imLoc,patchSize=17,height=32,width=32,plot=False)
    print('predicting shit')
    #preds   = predict_from_patchCol(patchCol)

    preds = predict_from_imLoc('./validationData/1/True/')

    coords = np.loadtxt('./validationData/1/coords.txt')
    im = cv2.imread('./validationData/testIm.png')
    plt.figure()
    grid_z0 = turn_into_potential(preds[:,0],coords,im,method='cubic')
    plt.imshow(np.transpose(grid_z0))
    #plt.imshow(np.transpose(cv2.GaussianBlur(grid_z0,(5,5),sigmaX=4,sigmaY=4)))