import math
from hyperopt import fmin, tpe, hp, Trials, STATUS_OK
from hyperopt.mongoexp import MongoTrials



#import math
a = 2
b = 100



#trials = MongoTrials('mongo://localhost:1234/foo_db/jobs', exp_key='exp1')
trials = Trials()
def f(space):
    x = space['x']
    y = space['y']
    loss = (a-x)**2+b*(y-x**2)**2
    result = {
        "loss": loss,
        "status": STATUS_OK,
        "space": space
    }
    return result

space = {
    'x': hp.uniform('x', -5, 5),
    'y': hp.uniform('y', -5, 5)
}

best = fmin(
    fn=f,
    space=space,
    trials=trials,
    algo=tpe.suggest,
    max_evals=1000
)

print("Found minimum after 1000 trials:")
print(best)
