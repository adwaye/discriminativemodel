import tensorflow as tf
from utils import _run

shap1 = [1,17,17,1]
shap2 = [1,10,10,1]

ksize1 = int(shap1[1]/shap2[1])
ksize2 = int(shap1[2]/shap2[2])
x = tf.truncated_normal(shap1, stddev=0.1, seed = 1)
xMaxPool = tf.nn.max_pool(x,ksize=[1,2,2,1],strides=[1,2,2,1],padding='VALID')