import sys
sys.path.append('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/')
import tensorflow as tf
from checkTfrecords import check_size,count_data,getImage
from utils import update_dir
import os


#MARCUS FOR WEIGHTS AND BIASES, YOU BUILD A DISTRIBUTION USING MEAN, VARIANCE AND MAX/MIN I HAVE FUNCTION THAT DOES THAT
#MARCUS FOR ACUURACY AND SCALAR VALUED DIAGNOSTICS YOU USE A TF.SUMMARY.SCALAR
#MARCUS FOR A LAYER(TYPICALLY ) THE OUTPUT OF A WEIGHT+BIAS+ACTIVATION, YOU WANT A HISTOGRAM

# Number of classes is 2 (squares and triangles)
nClass = 2

# Simple model (set to True) or convolutional neural network (set to False)
simpleModel = False

# Dimensions of image (pixels)
height = 32
width = 32

#hyperparamerteers
shape = check_size()
DIMX_IM = shape[0]
DIMY_IM = shape[1]
PRE_RESIZE = False #if false, input to the nn is the size of the image itself
BATCH_SIZE = 250
N_EPOCHS = 100
CAPACITY = 2000
DEQUEUE = 300
DROP_RATE1 = 0.4
DROP_RATE2 = 0.4
N_DATA = count_data()
LEARNING_RATE = 0.0001
nSteps = int((N_DATA / BATCH_SIZE) * N_EPOCHS)



#cnn architecture
nFeatures1 = 32
shp1       = (22,22)
nFeatures2 = 48
shp2       = (10,10)
nFeatures3 = 64
shp3       = (10,10)
nFeatures4 = 128
shp4       = (4,4)
nNeuronsfc1 = 128
nNeuronsfc2 = 101




#train data
#label, image = getImage("data/train-00000-of-00001",PRE_RESIZE=PRE_RESIZE ,height=height, width=height,nClass=nClass)
trainFileName = "data/train-00000-of-00001"
# and similarly for the validation data
#vlabel, vimage = getImage("data/validation-00000-of-00001",PRE_RESIZE=PRE_RESIZE ,height=height, width=height,
# nClass=nClass)
testFileName = "data/validation-00000-of-00001"

def parser(fullExample):
    # parse the full example into its' component features.
    features = tf.parse_single_example(
        fullExample,
        features={
            'image/height':tf.FixedLenFeature([],tf.int64),
            'image/width':tf.FixedLenFeature([],tf.int64),
            'image/colorspace':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/channels':tf.FixedLenFeature([],tf.int64),
            'image/class/label':tf.FixedLenFeature([],tf.int64),
            'image/class/text':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/format':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/filename':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/encoded':tf.FixedLenFeature([],dtype=tf.string,default_value='')
            #'image/encoded': tf.VarLenFeature([], dtype=tf.string)
        })

    # now we are going to manipulate the label and image features

    label = features['image/class/label']
    image_buffer = features['image/encoded']

    # Decode the jpeg
    with tf.name_scope('decode_jpeg',[image_buffer],None):
        # decode
        image = tf.image.decode_jpeg(image_buffer,channels=1)

        # and convert to single precision data type
        image = tf.image.convert_image_dtype(image,dtype=tf.float32)#.set_shape([33,33,1])
        image.set_shape([DIMX_IM,DIMY_IM,1])

    # cast image into a single array, where each element corresponds to the greyscale
    # value of a single pixel.
    # the "1-.." part inverts the image, so that the background is black.


    image = tf.reshape(1 - tf.image.rgb_to_grayscale(image),[DIMX_IM * DIMY_IM])
    # re-define label as a "one-hot" vector
    # it will be [0,1] or [1,0] here.
    # This approach can easily be extended to more classes.
    label = tf.stack(tf.one_hot(label - 1,nClass))
    #image.set_shape([height*width,1])
    #label = tf.cast(features['image/class/label'], tf.int32)
    return label,image



def data_iterator(fileName):
    dataset = tf.data.TFRecordDataset(fileName)
    dataset = dataset.map(parser)#,num_threads=2,output_buffer_size=batch_size)
    dataset = dataset.batch(batch_size=BATCH_SIZE)
    dataset = dataset.repeat()
    iterator= dataset.make_one_shot_iterator()
    #labels, features = iterator.get_next()
    return iterator


train_iterator = data_iterator(trainFileName)
#train_iterator = tf.data.Dataset(...).make_one_shot_iterator()
#train_iterator_handle = sess.run(train_iterator.string_handle())

test_iterator = data_iterator(testFileName)
#test_iterator_handle = sess.run(test_iterator.string_handle())




# interactive session allows inteleaving of building and running steps
#sess = tf.InteractiveSession()
def weight_variable(shape):
    initial = tf.truncated_normal(shape,stddev=0.1,seed=1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1,shape=shape)
    return tf.Variable(initial)

    # set up "vanilla" versions of convolution and pooling

def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1], padding='VALID')


def conv2d(x,W):
    return tf.nn.conv2d(x,W,strides=[1,1,1,1],padding='SAME')


def pool(x):
    return tf.nn.max_pool(x,ksize=[1,2,2,1],
                          strides=[1,2,2,1],padding='SAME')
    #b has dim [1,DIMY_IM,DIMX_IM,1]
    #Wx has dim [1,1,width,DIMX_IM]
    #Wy has dim [1,1,height,DIMY_IM]
    # TEST THIS!!!!!




def variable_summaries(var):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean',mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev',stddev)
        tf.summary.scalar('max',tf.reduce_max(var))
        tf.summary.scalar('min',tf.reduce_min(var))
        tf.summary.histogram('histogram',var)


# run convolutional neural network model given in "Expert MNIST" TensorFlow tutorial
# functions to init small positive weights and biases
print("Running Convolutional Neural Network Model")

handle = tf.placeholder(tf.string, shape=[],name='jamrocks')
iterator = tf.data.Iterator.from_string_handle(
    handle, train_iterator.output_types)

labs,x  = iterator.get_next()

with tf.name_scope('input'):
    if PRE_RESIZE:
        #x = tf.placeholder(tf.float32,[None,height * width],name='Patches')
        x_image = tf.reshape(x,[-1,width,height,1],name='Reshape_op')
    else:
        #x = tf.placeholder(tf.float32,[None,DIMY_IM* DIMX_IM],name='Patches')
        x_image = tf.reshape(x,[-1,DIMX_IM,DIMY_IM,1],name='Reshape_op')
    #x_image = tf.nn.batch_normalization(x_image,mean=0, variance =1, variance_epsilon=0.01,offset=None,scale=1,
    # name='Batch_norm_op')
    #y_ = tf.placeholder(tf.float32,[None,nClass],name='y-input'
    y_ = labs


# hidden layer 1
# pool(convolution(Wx)+b)
if PRE_RESIZE:
    with tf.name_scope('Conv1'):
        with tf.name_scope('conv'):
            h_conv1 = tf.layers.conv2d(
                inputs=x_image
                ,filters=nFeatures1
                ,kernel_size=[5,5]
                ,activation=tf.nn.relu
            )
            tf.summary.histogram('convolves',h_conv1)
        with tf.name_scope('Pool'):
            h_pool1 = tf.layers.max_pooling2d(inputs=h_conv1
                                              ,pool_size=[2,2]
                                              ,strides=2
                                              ,padding='valid')

            tf.summary.histogram('pools',h_pool1)

# similarly for second layer, with nFeatures2 features per 5x5 patch
# input is nFeatures1 (number of features output from previous layer)
with tf.name_scope('Conv2'):
    with tf.name_scope('conv'):
        if PRE_RESIZE:
            h_conv2 = tf.layers.conv2d(
                inputs=h_pool1
                ,filters=nFeatures2
                ,kernel_size=[5,5]
                ,activation=tf.nn.relu
            )
        else:
            h_conv2 = tf.layers.conv2d(
                inputs=x_image
                ,filters=nFeatures2
                ,kernel_size=[5,5]
                ,padding='same'
                ,activation=tf.nn.relu
            )
        tf.summary.histogram('convolves',h_conv2)
    with tf.name_scope('Pool'):
        h_pool2 = tf.layers.max_pooling2d(inputs=h_conv2
                                          ,pool_size=[2,2]
                                          ,strides=2
                                          ,padding='valid')
        tf.summary.histogram('pools',h_pool2)

with tf.name_scope('Conv3'):
    with tf.name_scope('conv'):
        h_conv3 = tf.layers.conv2d(
            inputs=h_pool2
            ,filters=nFeatures3
            ,kernel_size=[5,5]
            ,padding='same'
            ,activation=tf.nn.relu
        )
        tf.summary.histogram('convolves',h_conv3)
    with tf.name_scope('Pool'):
        h_pool3 = tf.layers.max_pooling2d(inputs=h_conv3
                                          ,pool_size=[2,2]
                                          ,strides=2
                                          ,padding='valid')
        tf.summary.histogram('pools',h_pool3)

with tf.name_scope('Conv4'):
    with tf.name_scope('conv'):
        h_conv4 = tf.layers.conv2d(
            inputs=h_pool3
            ,filters=nFeatures4
            ,kernel_size=[5,5]
            ,activation=tf.nn.relu
            ,padding='same'
        )
        tf.summary.histogram('convolves',h_conv4)
    with tf.name_scope('Pool'):
        h_pool4 = tf.layers.max_pooling2d(inputs=h_conv4
                                          ,pool_size=[4,4]
                                          ,strides=1
                                          ,padding='valid')
        tf.summary.histogram('pools',h_pool4)

with tf.name_scope('FC1'):
    h_pool4_flat = tf.reshape(h_pool4,[-1,nFeatures4])
    h_fc1 = tf.layers.dense(inputs=h_pool4_flat,units=nNeuronsfc1,activation=tf.nn.relu)
    tf.summary.histogram('neurons',h_fc1)

# reduce overfitting by applying dropout
# each neuron is kept with probability keep_prob
#"""
with tf.name_scope('keep_probs'):
    keep_prob1 = tf.placeholder(tf.float32,name='FC1')
    keep_prob2 = tf.placeholder(tf.float32,name='FC2')
#"""
with tf.name_scope('FC2'):
    with tf.name_scope('drop_outs'):
        h_fc1_drop = tf.nn.dropout(h_fc1,keep_prob1)
        tf.summary.histogram('drops',h_fc1_drop)
    with tf.name_scope('activations'):
        h_fc2 = tf.layers.dense(h_fc1_drop,units=nNeuronsfc2,activation=tf.nn.relu)
        tf.summary.histogram('activations',h_fc2)

# create readout layer which outputs to nClass categories
with tf.name_scope('CL'):
    with tf.name_scope('pre-activations-dropout'):
        h_fc2_drop = tf.nn.dropout(h_fc2,keep_prob2)#,training=mode == tf.estimator.ModeKeys.TRAIN)
        tf.summary.histogram('dropout',h_fc2_drop)
    with tf.name_scope('final_pred'):
        y = tf.layers.dense(h_fc2_drop,units=nClass,activation=tf.nn.softmax,name='Softmax')

        # measure of error of our model
# this needs to be minimised by adjusting W and b
with tf.name_scope('cross_entropy'):
    cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y),reduction_indices=[1]))
tf.summary.scalar('cross_entropy',cross_entropy)

# define training step which minimises cross entropy
with tf.name_scope('train'):
    train_step = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE).minimize(cross_entropy)

# argmax gives index of highest entry in vector (1st axis of 1D tensor)
with tf.name_scope('accuracy'):
    with tf.name_scope('correct_prediction'):
        correct_prediction = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
    with tf.name_scope('accuracy'):
        # get mean of all entries in correct prediction, the higher the better
        accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))
tf.summary.scalar('accuracy',accuracy)

# Merge all the summaries and write them out to
# /tmp/tensorflow/mnist/logs/mnist_with_summaries (by default)
#SAVE SHIT!!!!!!
log_dir = update_dir('./trainedModel/')
log_dir_board = log_dir + '/tensorboard'
merged = tf.summary.merge_all()

# run the session
# start training and record shit
# initialize the variables
saver = tf.train.Saver(max_to_keep=4)
config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
with tf.Session(config=config) as sess:
    train_writer = tf.summary.FileWriter(log_dir_board + '/train',sess.graph)
    test_writer = tf.summary.FileWriter(log_dir_board + '/test')
    sess.run(tf.local_variables_initializer())
    sess.run(tf.global_variables_initializer())

    #MARCUS READ THAT SHIT YOU WILL GET IT, YOU'RE SMART
    # start the threads used for reading files
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess,coord=coord)

    for i in range(nSteps):
        """
        test_iterator_handle = sess.run(test_iterator.string_handle())
        im = sess.run(x,feed_dict={handle: test_iterator_handle,
                                                                           keep_prob1:1.0 -DROP_RATE1,keep_prob2:1.0 - DROP_RATE2})
        print(im.shape)
        print(test_iterator)
        """
        if i % 100 == 0:  # Record summaries and test-set accuracy
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            test_iterator_handle = sess.run(test_iterator.string_handle())
            summary,acc = sess.run([merged,accuracy],feed_dict={handle: test_iterator_handle,
                                                                           keep_prob1:1.0 -DROP_RATE1,keep_prob2:1.0 - DROP_RATE2} )
            test_writer.add_summary(summary,i)
            print('Accuracy at step %s: %s' % (i,acc))
        else:  # Record train set summaries, and train
            if i % 100 == 99:  # Record execution stats
                run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                run_metadata = tf.RunMetadata()
                train_iterator_handle = sess.run( train_iterator.string_handle() )
                summary,_ = sess.run([merged,train_step],
                                     feed_dict={handle: train_iterator_handle,keep_prob1:1.0 - DROP_RATE1,keep_prob2:1.0 -
                 DROP_RATE2},
                                     options=run_options,
                                     run_metadata=run_metadata)
                train_writer.add_run_metadata(run_metadata,'step%03d' % i)
                train_writer.add_summary(summary,i)
                print('Adding run metadata for',i)
                if not os.path.isdir(log_dir + '/savedModel'): os.mkdir(log_dir + '/savedModel')

            else:  # Record a summary
                train_iterator_handle = sess.run(train_iterator.string_handle())
                summary,_ = sess.run([merged,train_step],feed_dict={handle: train_iterator_handle,keep_prob1:1.0 - DROP_RATE1,keep_prob2:1.0 - DROP_RATE2})
                train_writer.add_summary(summary,i)
        #"""
        if i % 1000 == 0:
            save_path = saver.save(sess,log_dir + '/savedModel/model',global_step=i + 1)

    save_path = saver.save(sess,log_dir + '/savedModel/model',global_step=i + 1)
    train_writer.close()
    test_writer.close()
    # finalise
    coord.request_stop()
    coord.join(threads)

with open(log_dir_board + '/params.txt','a') as fp:
    fp.write('XDIM = ' + str(DIMX_IM) + '\n')
    fp.write('YDIM = ' + str(DIMY_IM) + '\n')
    fp.write('PRE_RESIZE = ' + str(PRE_RESIZE) + '\n')
    fp.write('BATCH_SIZE = ' + str(BATCH_SIZE) + '\n')
    fp.write('N_EPOCHS = ' + str(N_EPOCHS) + '\n')
    fp.write('CAPACITY = ' + str(CAPACITY) + '\n')
    fp.write('DEQUEUE = ' + str(DEQUEUE) + '\n')
    fp.write('DROP_RATE = ' + str(DROP_RATE1) + '\n')
    fp.write('DROP_RATE = ' + str(DROP_RATE2) + '\n')
    fp.write('N_DATA = ' + str(N_DATA) + '\n')
    fp.write('nSteps = ' + str(nSteps))
    fp.close()



