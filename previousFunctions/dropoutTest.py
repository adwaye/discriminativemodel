import numpy as np
import tensorflow as tf
from utils import _run

mat = np.ones((10,10))
b   = np.ones((10,1))

tf_mat = tf.constant(mat)
tf_b   = tf.constant(b)

#res   = _run(tf.matmul(tf.layers.dropout(tf_mat,0.5,training=False),tf_b))
trainFlag = tf.constant(False,dtype=tf.bool)
prob = tf.placeholder(tf.float64)#,shape=tf_mat.get_shape())
tf_cond = tf.cond(trainFlag,lambda:tf.nn.dropout(tf_mat,0.5), lambda:tf.multiply(prob,tf_mat)   )
#tf_cond = tf.multiply(prob,tf_mat)
#grad    = _run( tf.gradients( tf_cond ,tf_mat) ) 
#res = _run(tf.layers.dropout(tf_mat,0.3,training=True))
#print(grad)
with tf.Session() as sess:
    #sess.run(tf.)
    #prob = 
    #print(sess.run(tf_mat))
    res = sess.run(tf_cond,feed_dict={prob: 0.2})
