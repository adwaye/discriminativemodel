import tensorflow as tf
from checkTfrecords import check_size,count_data,getImage
from utils import update_dir
import os

#MARCUS FOR WEIGHTS AND BIASES, YOU BUILD A DISTRIBUTION USING MEAN, VARIANCE AND MAX/MIN I HAVE FUNCTION THAT DOES THAT
#MARCUS FOR ACUURACY AND SCALAR VALUED DIAGNOSTICS YOU USE A TF.SUMMARY.SCALAR
#MARCUS FOR A LAYER(TYPICALLY ) THE OUTPUT OF A WEIGHT+BIAS+ACTIVATION, YOU WANT A HISTOGRAM

# Number of classes is 2 (squares and triangles)

nClass = 2

# Simple model (set to True) or convolutional neural network (set to False)
simpleModel = False

# Dimensions of image (pixels)
height = 32
width = 32

#hyperparamerteers
shape = check_size()
DIMX_IM = shape[0]
DIMY_IM = shape[1]
PRE_RESIZE = False #if false, input to the nn is the size of the image itself
BATCH_SIZE = 250
N_EPOCHS = 50
CAPACITY = 2000
DEQUEUE = 300
DROP_RATE1 = 0.4
DROP_RATE2 = 0.4
N_DATA = count_data()
LEARNING_RATE = 0.0001
nSteps = int((N_DATA / BATCH_SIZE) * N_EPOCHS)



#cnn architecture
nFeatures1 = 32
shp1       = (22,22)
nFeatures2 = 48
shp2       = (10,10)
nFeatures3 = 64
shp3       = (10,10)
nFeatures4 = 128
shp4       = (4,4)
nNeuronsfc1 = 128
nNeuronsfc2 = 101



#train data
label,image = getImage("data/train-00000-of-00001",PRE_RESIZE=PRE_RESIZE,height=height,width=height,nClass=nClass)

# and similarly for the validation data
vlabel,vimage = getImage("data/validation-00000-of-00001",PRE_RESIZE=PRE_RESIZE,height=height,width=height,
                         nClass=nClass)

# associate the "label_batch" and "image_batch" objects with a randomly selected batch---
# of labels and images respectively
imageBatch,labelBatch = tf.train.shuffle_batch(
    [image,label],batch_size=BATCH_SIZE,
    capacity=CAPACITY,
    min_after_dequeue=DEQUEUE,
    num_threads=2,
    allow_smaller_final_batch=True)

# and similarly for the validation data
vimageBatch,vlabelBatch = tf.train.shuffle_batch(
    [vimage,vlabel],batch_size=BATCH_SIZE,
    capacity=CAPACITY,
    min_after_dequeue=DEQUEUE,
    num_threads=2,
    allow_smaller_final_batch=True)


# interactive session allows inteleaving of building and running steps
#sess = tf.InteractiveSession()
def weight_variable(shape):
    initial = tf.truncated_normal(shape,stddev=0.1,seed=1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1,shape=shape)
    return tf.Variable(initial)

    # set up "vanilla" versions of convolution and pooling


def max_pool_2x2(x):
    return tf.nn.max_pool(x,ksize=[1,2,2,1],
                          strides=[1,2,2,1],padding='VALID')


def conv2d(x,W):
    return tf.nn.conv2d(x,W,strides=[1,1,1,1],padding='SAME')


def pool(x):
    return tf.nn.max_pool(x,ksize=[1,2,2,1],
                          strides=[1,2,2,1],padding='SAME')
    #b has dim [1,DIMY_IM,DIMX_IM,1]
    #Wx has dim [1,1,width,DIMX_IM]
    #Wy has dim [1,1,height,DIMY_IM]
    # TEST THIS!!!!!


def variable_summaries(var):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean',mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev',stddev)
        tf.summary.scalar('max',tf.reduce_max(var))
        tf.summary.scalar('min',tf.reduce_min(var))
        tf.summary.histogram('histogram',var)





"""
params dict with following keys
'PRE_RESIZE'
'nFeatures1'
'nFeatures2'
'nNeuronsfc1'
'nNeuronsfc2'
'LEARNING_RATE'
'DROP_RATE1'
'DROP_RATE2'
'DIMX_IM'
'DIMy_IM'
'N_DATA'
'BATCH_SIZE'
'N_EPOCHS'
'height'
'width'
"""
#make features a dict, with valid set and train set as arguments
def cnnFunction(features,labels,mode,params):

    # Dimensions of image (pixels)
    height = params['height']
    width  = params['width']
    DIMX_IM = params['DIMX_IM']
    DIMY_IM = params['DIMY_IM']
    PRE_RESIZE = params['PRE_RESIZE'] #if false, input to the nn is the size of the image itself
    BATCH_SIZE = params['BATCH_SIZE']
    N_EPOCHS = params['N_EPOCHS']
    CAPACITY = params['CAPACITY']
    #DEQUEUE = params['']
    DROP_RATE1 = params['DROP_RATE1']
    DROP_RATE2 = params['DROP_RATE2']
    N_DATA = params['N_DATA']
    LEARNING_RATE = params['LEARNING_RATE']
    nSteps = int((N_DATA / BATCH_SIZE) * N_EPOCHS)

    # Dimensions of image (pixels)
    height = params['height']
    width  = params['width']
    DIMX_IM = params['DIMX_IM']
    DIMY_IM = params['DIMY_IM']
    PRE_RESIZE = params['PRE_RESIZE'] #if false, input to the nn is the size of the image itself
    BATCH_SIZE = params['BATCH_SIZE']
    N_EPOCHS = params['N_EPOCHS']
    CAPACITY = params['CAPACITY']
    #DEQUEUE = params['']
    DROP_RATE1 = params['DROP_RATE1']
    DROP_RATE2 = params['DROP_RATE2']
    N_DATA = params['N_DATA']
    LEARNING_RATE = params['LEARNING_RATE']
    nSteps = int((N_DATA / BATCH_SIZE) * N_EPOCHS)
    with tf.name_scope('input'):
        if PRE_RESIZE:
            x = features
            x_image = tf.reshape(x,[-1,width,height,1],name='Reshape_op')
        else:
            x = features
            x_image = tf.reshape(x,[-1,DIMX_IM,DIMY_IM,1],name='Reshape_op_BIGDICK')
        #x_image = tf.nn.batch_normalization(x_image,mean=0, variance =1, variance_epsilon=0.01,offset=None,scale=1,
        # name='Batch_norm_op')
        y_ = labels

    # hidden layer 1
    # pool(convolution(Wx)+b)
    if PRE_RESIZE:
        with tf.name_scope('Conv1'):
            with tf.name_scope('conv'):
                h_conv1 = tf.layers.conv2d(
                            inputs = x_image
                            ,filters = nFeatures1
                            ,kernel_size=[5,5]
                            ,activation=tf.nn.relu
                            )
                tf.summary.histogram('convolves',h_conv1)
            with tf.name_scope('Pool'):
                h_pool1 = tf.layers.max_pooling2d(inputs=h_conv1
                                                 ,pool_size=[2,2]
                                                 ,strides=2
                                                 ,padding='valid')

                tf.summary.histogram('pools',h_pool1)

    # similarly for second layer, with nFeatures2 features per 5x5 patch
    # input is nFeatures1 (number of features output from previous layer)
    with tf.name_scope('Conv2'):
        with tf.name_scope('conv'):
            if PRE_RESIZE:
                h_conv2 = tf.layers.conv2d(
                            inputs = h_pool1
                            ,filters = nFeatures2
                            ,kernel_size=[5,5]
                            ,activation=tf.nn.relu
                            )
            else:
                h_conv2 = tf.layers.conv2d(
                            inputs = x_image
                            ,filters = nFeatures2
                            ,kernel_size=[5,5]
                            ,padding='same'
                            ,activation=tf.nn.relu
                            )
            tf.summary.histogram('convolves',h_conv2)
        with tf.name_scope('Pool'):
            h_pool2 = tf.layers.max_pooling2d(inputs=h_conv2
                                              ,pool_size=[2,2]
                                              ,strides=2
                                              ,padding='valid')
            tf.summary.histogram('pools',h_pool2)

    with tf.name_scope('Conv3'):
        with tf.name_scope('conv'):
            h_conv3 = tf.layers.conv2d(
                            inputs = h_pool2
                            ,filters = nFeatures3
                            ,kernel_size=[5,5]
                            ,padding='same'
                            ,activation=tf.nn.relu
                            )
            tf.summary.histogram('convolves',h_conv3)
        with tf.name_scope('Pool'):
            h_pool3 = tf.layers.max_pooling2d(inputs=h_conv3
                                              ,pool_size=[2,2]
                                              ,strides=2
                                              ,padding='valid')
            tf.summary.histogram('pools',h_pool3)

    with tf.name_scope('Conv4'):
        with tf.name_scope('conv'):
            h_conv4 = tf.layers.conv2d(
                            inputs = h_pool3
                            ,filters = nFeatures4
                            ,kernel_size=[5,5]
                            ,activation=tf.nn.relu
                            ,padding='same'
                            )
            tf.summary.histogram('convolves',h_conv4)
        with tf.name_scope('Pool'):
            h_pool4 = tf.layers.max_pooling2d(inputs=h_conv4
                                              ,pool_size=[4,4]
                                              ,strides=1
                                              ,padding='valid')
            tf.summary.histogram('pools',h_pool4)


    with tf.name_scope('FC1'):
        h_pool4_flat = tf.reshape(h_pool4,[-1, nFeatures4])
        h_fc1 = tf.layers.dense(inputs=h_pool4_flat,units=nNeuronsfc1,activation=tf.nn.relu)
        tf.summary.histogram('neurons',h_fc1)

    # reduce overfitting by applying dropout
    # each neuron is kept with probability keep_prob
    """
    with tf.name_scope('keep_probs'):
        keep_prob1 = tf.placeholder(tf.float32,name='FC1')
        keep_prob2 = tf.placeholder(tf.float32,name='FC2')
    """
    with tf.name_scope('FC2'):
        with tf.name_scope('drop_outs'):
            h_fc1_drop = tf.layers.dropout(h_fc1,rate = DROP_RATE1,training=mode==tf.estimator.ModeKeys.TRAIN)
            tf.summary.histogram('drops',h_fc1_drop)
        with tf.name_scope('activations'):
            h_fc2 = tf.layers.dense(h_fc1_drop,units=nNeuronsfc2,activation=tf.nn.relu)
            tf.summary.histogram('activations',h_fc2)


    # create readout layer which outputs to nClass categories
    with tf.name_scope('CL'):
        with tf.name_scope('pre-activations-dropout'):
            h_fc2_drop = tf.layers.dropout(h_fc2,rate=DROP_RATE2,training=mode==tf.estimator.ModeKeys.TRAIN)
            tf.summary.histogram('dropout',h_fc2_drop)
        with tf.name_scope('final_pred'):
            y = tf.layers.dense(h_fc2_drop,units=nClass,activation=tf.nn.softmax)

            # measure of error of our model
    # this needs to be minimised by adjusting W and b
    with tf.name_scope('cross_entropy'):
        cross_entropy = tf.reduce_mean(-tf.reduce_sum( y_* tf.log(y),reduction_indices=[1]),name='loss')
    tf.summary.scalar('cross_entropy',cross_entropy)

    # define training step which minimises cross entropy
    with tf.name_scope('train'):
        train_step = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE).minimize(cross_entropy,
                                                                                  global_step=tf.train.get_global_step())

    # argmax gives index of highest entry in vector (1st axis of 1D tensor)
    with tf.name_scope('accuracy'):
        #with tf.name_scope('correct_prediction'):
         correct_prediction = tf.equal(tf.argmax(y,1),tf.argmax(y_,1),name='correct_prediction')
        #with tf.name_scope('accuracy'):
            # get mean of all entries in correct prediction, the higher the better
         accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32),name='metric')
    tf.summary.scalar('accuracy',accuracy)
    #merged = tf.summary.merge_all()
    #summary_hook = tf.train.SummarySaverHook(save_steps=100,output_dir=params['OUTPUT_DIR'],summary_op=merged)
    predictions = {
        "classes":tf.argmax(y,1),
        "probabilities":y}


    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode,predictions=predictions)

    if mode == tf.estimator.ModeKeys.TRAIN:
        train_op = train_step
        return tf.estimator.EstimatorSpec(mode=mode,loss=cross_entropy,train_op=train_op)

    eval_metric_ops = {
        "accuracy"  :tf.metrics.accuracy(tf.argmax(labels,1),tf.argmax(y,1)),
        "RMSE"      :tf.metrics.root_mean_squared_error(tf.argmax(labels,1),tf.argmax(y,1))
    }
    return tf.estimator.EstimatorSpec(
        mode=mode,loss=cross_entropy,eval_metric_ops=eval_metric_ops)




