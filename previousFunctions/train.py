import sys
sys.path.append('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/')
import tensorflow as tf
import sys
import numpy
from checkTfrecords import check_size, count_data, getImage
from utils import update_dir, _run
import os

#MARCUS FOR WEIGHTS AND BIASES, YOU BUILD A DISTRIBUTION USING MEAN, VARIANCE AND MAX/MIN I HAVE FUNCTION THAT DOES THAT
#MARCUS FOR ACUURACY AND SCALAR VALUED DIAGNOSTICS YOU USE A TF.SUMMARY.SCALAR
#MARCUS FOR A LAYER(TYPICALLY ) THE OUTPUT OF A WEIGHT+BIAS+ACTIVATION, YOU WANT A HISTOGRAM

# Number of classes is 2 (squares and triangles)
nClass=2

# Simple model (set to True) or convolutional neural network (set to False)
simpleModel=False

# Dimensions of image (pixels)
height=32
width=32



shape = check_size()
DIMX_IM = shape[0]
DIMY_IM = shape[1]
PRE_RESIZE = True #if false, does a trainable resize op, not very good after tests
BATCH_SIZE = 100
N_EPOCHS  = 100
CAPACITY  = 200
DEQUEUE   = 100
DROP_RATE = 0.4
N_DATA    = count_data() 
nSteps    = int((N_DATA/BATCH_SIZE)*N_EPOCHS)
LEARNING_RATE = 0.0001

#train data
label, image = getImage("data/train-00000-of-00001",PRE_RESIZE=PRE_RESIZE ,height=height, width=height,nClass=nClass)

# and similarly for the validation data
vlabel, vimage = getImage("data/validation-00000-of-00001",PRE_RESIZE=PRE_RESIZE ,height=height, width=height,nClass=nClass)

# associate the "label_batch" and "image_batch" objects with a randomly selected batch---
# of labels and images respectively
imageBatch, labelBatch = tf.train.shuffle_batch(
    [image, label], batch_size=BATCH_SIZE,
    capacity=CAPACITY,
    min_after_dequeue=DEQUEUE,
    num_threads=2,
    allow_smaller_final_batch=True)

# and similarly for the validation data 
vimageBatch, vlabelBatch = tf.train.shuffle_batch(
    [vimage, vlabel], batch_size=BATCH_SIZE,
    capacity=CAPACITY,
    min_after_dequeue=DEQUEUE,
    num_threads=2,
    allow_smaller_final_batch=True)

# interactive session allows inteleaving of building and running steps
#sess = tf.InteractiveSession()
def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1, seed = 1)
    return tf.Variable(initial)
    

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

  # set up "vanilla" versions of convolution and pooling
def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1], padding='SAME')
  #b has dim [1,DIMY_IM,DIMX_IM,1]
  #Wx has dim [1,1,width,DIMX_IM]
  #Wy has dim [1,1,height,DIMY_IM]
  # TEST THIS!!!!!
def train_reshape(b,Wx,Wy):
     Wx = tf.tile(Wx,[BATCH_SIZE,DIMY_IM,1,1]) # need to have first 2 dims equal in matmul
     c  = tf.matmul(Wx,b)                   # dim [1,DIMY_IM,WIDTH,1]
     d  = tf.transpose(c,[0,2,1,3])          # dim [1,WIDTH,DIMY_IM,1]
     Wy = tf.tile(Wy,[BATCH_SIZE,height,1,1])
     return tf.nn.sigmoid(tf.matmul(Wy,d)) # dim [1,DIMY_IM,WIDTH,1]

def variable_summaries(var):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope('summaries'):
      mean = tf.reduce_mean(var)
      tf.summary.scalar('mean', mean)
      with tf.name_scope('stddev'):
        stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
      tf.summary.scalar('stddev', stddev)
      tf.summary.scalar('max', tf.reduce_max(var))
      tf.summary.scalar('min', tf.reduce_min(var))
      tf.summary.histogram('histogram', var)

# run convolutional neural network model given in "Expert MNIST" TensorFlow tutorial
# functions to init small positive weights and biases
print("Running Convolutional Neural Network Model")
nFeatures1=32
nFeatures2=64
nNeuronsfc=1024

"""
with tf.name_scope('input'):
 if PRE_RESIZE:
  x = tf.placeholder(tf.float32, [None, height*width],name='Patches')
  x_image = tf.reshape(x, [-1,width,height,1],name='Reshape_op')
 else:
  x = tf.placeholder(tf.float32, [None, DIMX_IM*DIMY_IM],name='Patches')
  with tf.name_scope('trainable_resize'):
   with tf.name_scope('resize_weights_x'):
    Wx      = weight_variable([1,1,width,DIMX_IM])
    variable_summaries(Wx)
   with tf.name_scope('resize_weights_y'):
    Wy      = weight_variable([1,1,height,DIMY_IM])
    variable_summaries(Wy)
   x_image = train_reshape(tf.reshape(x, [-1,DIMX_IM,DIMY_IM,1]),Wx,Wy)
 y_ = tf.placeholder(tf.float32, [None, nClass],name='y-input')
"""
with tf.name_scope('input'):
 x = tf.placeholder(tf.float32, [None, height*width],name='Patches')
 x_image = tf.reshape(x, [-1,width,height,1],name='Reshape_op')
 #x_image = tf.nn.batch_normalization(x_image,mean=0, variance =1, variance_epsilon=0.01,offset=None,scale=1,name='Batch_norm_op')
 y_ = tf.placeholder(tf.float32, [None, nClass],name='y-input')



# hidden layer 1 
# pool(convolution(Wx)+b)
# pool reduces each dim by factor of 2.
with tf.name_scope('Conv1'):
 with tf.name_scope('weights'):
  W_conv1 = weight_variable([5, 5, 1, nFeatures1]) #5 by 5 filter kernel turning each patch of 5by 5 into nfeatures: mapping it to higher dimension
  variable_summaries(W_conv1)
 with tf.name_scope('biases'):
  b_conv1 = bias_variable([nFeatures1])
  variable_summaries(b_conv1)
 with tf.name_scope('filter'):
  h_conv1 = conv2d(x_image, W_conv1) + b_conv1
  tf.summary.histogram('pre_activations',h_conv1)
 with tf.name_scope('Activations'):
  h_conv1_act = tf.nn.relu(h_conv1)
  tf.summary.histogram('activations',h_conv1_act)
 with tf.name_scope('Pool'):
  h_pool1 = max_pool_2x2(h_conv1_act)
  tf.summary.histogram('pools',h_pool1)
  
# similarly for second layer, with nFeatures2 features per 5x5 patch
# input is nFeatures1 (number of features output from previous layer)
with tf.name_scope('Conv2'):
 with tf.name_scope('weights'):
  W_conv2 = weight_variable([5, 5, nFeatures1, nFeatures2])
  variable_summaries(W_conv2)
 with tf.name_scope('biases'):
  b_conv2 = bias_variable([nFeatures2])
  variable_summaries(b_conv2)
 with tf.name_scope('filter'):
  h_conv2 = conv2d(h_pool1, W_conv2) + b_conv2
  tf.summary.histogram('pre_activations',h_conv2)
 with tf.name_scope('activations'):
  h_conv2_act = tf.nn.relu(h_conv2)
  tf.summary.histogram('activations',h_conv2_act)
 with tf.name_scope('Pool'):
  h_pool2 = max_pool_2x2(h_conv2_act)
  tf.summary.histogram('pools',h_pool2)

# denseley connected layer. Similar to above, but operating
# on entire image (rather than patch) which has been reduced by a factor of 4 
# in each dimension
# so use large number of neurons 

# check our dimensions are a multiple of 4
if (width%4 or height%4):
    print("Error: width and height must be a multiple of 4")
    sys.exit(1)

with tf.name_scope('FC1'):
 with tf.name_scope('weights'):
  W_fc1 = weight_variable([int((width/4) * (height/4) * nFeatures2), nNeuronsfc])
  variable_summaries(W_fc1)
 with tf.name_scope('biases'):
  b_fc1 = bias_variable([nNeuronsfc])
  variable_summaries(b_fc1)
 with tf.name_scope('pre_activations'):
  h_pool2_flat = tf.reshape(h_pool2, [-1, int((width/4) * (height/4) * nFeatures2)])
  h_fc1 = tf.matmul(h_pool2_flat, W_fc1) + b_fc1
  tf.summary.histogram('pre_activations',h_fc1)
 with tf.name_scope('activations'):
  h_fc1_act = tf.nn.relu(h_fc1)
  tf.summary.histogram('activations',h_fc1_act)
  
# reduce overfitting by applying dropout
# each neuron is kept with probability keep_prob
with tf.name_scope('keep_prob'):
 keep_prob = tf.placeholder(tf.float32)


# create readout layer which outputs to nClass categories
with tf.name_scope('CL'):
 with tf.name_scope('weights'):
  W_fc2 = weight_variable([nNeuronsfc, nClass])
  variable_summaries(W_fc2)
 with tf.name_scope('biases'):
  b_fc2 = bias_variable([nClass])
  variable_summaries(b_fc2)
 with tf.name_scope('pre-activations-dropout'):
  h_fc1_drop = tf.nn.dropout(h_fc1_act, keep_prob)
  tf.summary.histogram('dropout',h_fc1_drop)
 # define output calc (for each class) y = softmax(Wx+b)
 # softmax gives probability distribution across all classes
 # this is not run until later
  h_fcl = tf.matmul(h_fc1_drop, W_fc2) + b_fc2
  tf.summary.histogram('pre_activations',h_fcl)
 with tf.name_scope('final_pred'):
  y=tf.nn.softmax(h_fc1)

# measure of error of our model
# this needs to be minimised by adjusting W and b
with tf.name_scope('cross_entropy'):
 cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
tf.summary.scalar('cross_entropy',cross_entropy) 


# define training step which minimises cross entropy
with tf.name_scope('train'):
 train_step = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE).minimize(cross_entropy)

# argmax gives index of highest entry in vector (1st axis of 1D tensor)
with tf.name_scope('accuracy'):
 with tf.name_scope('correct_prediction'):
  correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
 with tf.name_scope('accuracy'):
# get mean of all entries in correct prediction, the higher the better
  accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
tf.summary.scalar('accuracy',accuracy)

# Merge all the summaries and write them out to
# /tmp/tensorflow/mnist/logs/mnist_with_summaries (by default)
#SAVE SHIT!!!!!!
log_dir = update_dir('./trainedModel/')
log_dir_board = log_dir +'/tensorboard'
merged = tf.summary.merge_all()




# run the session
# start training and record shit
# initialize the variables
saver = tf.train.Saver(max_to_keep=4)
config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
with tf.Session(config=config) as sess:
 train_writer = tf.summary.FileWriter(log_dir_board + '/train', sess.graph)
 test_writer = tf.summary.FileWriter(log_dir_board + '/test')
 sess.run(tf.local_variables_initializer())
 sess.run(tf.global_variables_initializer())
 
#MARCUS READ THAT SHIT YOU WILL GET IT, YOU'RE SMART
# start the threads used for reading files
 coord = tf.train.Coordinator()
 threads = tf.train.start_queue_runners(sess=sess,coord=coord)
 
 
 
 for i in range(nSteps):
    batch_xs, batch_ys = sess.run([imageBatch, labelBatch])
    if i % 100 == 0:  # Record summaries and test-set accuracy
      run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
      run_metadata = tf.RunMetadata()
      vbatch_xs, vbatch_ys = sess.run([vimageBatch, vlabelBatch])
      summary, acc = sess.run([merged, accuracy], feed_dict={x:vbatch_xs, y_: vbatch_ys,keep_prob: 1.0-DROP_RATE})
      test_writer.add_summary(summary, i)
      print('Accuracy at step %s: %s' % (i, acc))
    else:  # Record train set summaries, and train
      if i % 100 == 99:  # Record execution stats
        run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
        run_metadata = tf.RunMetadata()
        summary, _ = sess.run([merged, train_step],
                              feed_dict={x:batch_xs, y_: batch_ys, keep_prob: 1.0-DROP_RATE},
                              options=run_options,
                              run_metadata=run_metadata)
        train_writer.add_run_metadata(run_metadata, 'step%03d' % i)
        train_writer.add_summary(summary, i)
        print('Adding run metadata for', i)
        if not os.path.isdir(log_dir+'/savedModel'): os.mkdir(log_dir+'/savedModel')
        
      else:  # Record a summary
        summary, _ = sess.run([merged, train_step], feed_dict={x:batch_xs, y_: batch_ys, keep_prob: 1.0-DROP_RATE})
        train_writer.add_summary(summary, i)
    
    if i%1000==0:
       save_path = saver.save(sess, log_dir+'/savedModel/model', global_step=i+1)
   
 save_path = saver.save(sess, log_dir+'/savedModel/model', global_step=i+1)
 train_writer.close()
 test_writer.close()
# finalise 
 coord.request_stop()
 coord.join(threads)


with open(log_dir_board+'/params.txt', 'a') as fp:
 fp.write('XDIM '+str(DIMX_IM)+'\n') 
 fp.write('YDIM '+str(DIMY_IM)+'\n')
 fp.write('PRE_RESIZE '+ str(PRE_RESIZE)+'\n')
 fp.write('BATCH_SIZE '+ str(BATCH_SIZE) +'\n')
 fp.write('N_EPOCHS '+ str(N_EPOCHS) +'\n')
 fp.write('CAPACITY '+ str(CAPACITY) +'\n')
 fp.write('DEQUEUE '+ str(DEQUEUE) +'\n')
 fp.write('DROP_RATE '+ str(DROP_RATE) +'\n')
 fp.write('N_DATA '+ str(N_DATA)  +'\n')
 fp.write('nSteps '+ str(nSteps))
 fp.close()



