import tensorflow as tf
import sys
import numpy
from checkTfrecords import check_size, count_data, getImage
from utils import update_dir, _run
import os
from hyperopt import fmin, tpe, hp, Trials, STATUS_OK
from hyperopt.mongoexp import MongoTrials



def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1, seed = 1)
    return tf.Variable(initial)
    

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

  # set up "vanilla" versions of convolution and pooling
def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1], padding='SAME')



# Number of classes is 2 (patch and no patch)
nClass=2

# Dimensions of image (pixels)
height=32
width=32



shape = check_size()
DIMX_IM = shape[0]
DIMY_IM = shape[1]
PRE_RESIZE = True #if false, does a trainable resize op, not very good after tests
#BATCH_SIZE = 200
N_EPOCHS  = 100
CAPACITY  = 2000
#DEQUEUE   = 200
#DROP_RATE = 0.4
N_DATA    = count_data() 
#nSteps    = int((N_DATA/BATCH_SIZE)*N_EPOCHS)
N_DATA_VAL  = count_data(tfrecords_filename = "data/validation-00000-of-00001")

def computeAccuracy(BATCH_SIZE=200,nFeatures1=32,nFeatures2=64,nNeuronsfc=1024,DROP_RATE = 0.4):
    #COMPUTING NITER FOR GRAD DESCENT
    nSteps    = 1#int((N_DATA/BATCH_SIZE)*N_EPOCHS)
    #CAPACITY  = BATCH_SIZE
    BATCH_SIZE=int(BATCH_SIZE)
    nFeatures1=int(nFeatures1)
    nFeatures2=int(nFeatures2)
    nNeuronsfc=int(nNeuronsfc)
    
    
    DEQUEUE   = BATCH_SIZE+100
    
    #train data
    label, image = getImage("data/train-00000-of-00001",PRE_RESIZE=PRE_RESIZE ,height=height, width=height,nClass=nClass)
    
    # and similarly for the validation data
    vlabel, vimage = getImage("data/validation-00000-of-00001",PRE_RESIZE=PRE_RESIZE ,height=height, width=height,nClass=nClass)
    
    # associate the "label_batch" and "image_batch" objects with a randomly selected batch---
    # of labels and images respectively
    imageBatch, labelBatch = tf.train.shuffle_batch(
        [image, label], batch_size=BATCH_SIZE,
        capacity=CAPACITY,
        #enqueue_many=True,
        min_after_dequeue=DEQUEUE,
        allow_smaller_final_batch=True)
    
    # and similarly for the validation data 
    vimageBatch, vlabelBatch = tf.train.batch(
        [vimage, vlabel], batch_size=200,
        capacity=300,
        #   enqueue_many=True,
        #min_after_dequeue=100,
        allow_smaller_final_batch=True)
    
    
    # run convolutional neural network model given in "Expert MNIST" TensorFlow tutorial
    # functions to init small positive weights and biases
    print("Running Convolutional Neural Network Model")
    
    with tf.name_scope('input'):
     x = tf.placeholder(tf.float32, [None, height*width],name='Patches')
     x_image = tf.reshape(x, [-1,width,height,1],name='Reshape_op')
     y_ = tf.placeholder(tf.float32, [None, nClass],name='y-input')
    
    
    
    # hidden layer 1 
    # pool(convolution(Wx)+b)
    # pool reduces each dim by factor of 2.
    with tf.name_scope('Conv1'):
     with tf.name_scope('weights'):
      W_conv1 = weight_variable([5, 5, 1, nFeatures1]) #5 by 5 filter kernel turning each patch of 5by 5 into nfeatures: mapping it to higher dimension
     with tf.name_scope('biases'):
      b_conv1 = bias_variable([nFeatures1])
     with tf.name_scope('filter'):
      h_conv1 = conv2d(x_image, W_conv1) + b_conv1
     with tf.name_scope('Activations'):
      h_conv1_act = tf.nn.relu(h_conv1)
     with tf.name_scope('Pool'):
      h_pool1 = max_pool_2x2(h_conv1_act)
    
      
    # similarly for second layer, with nFeatures2 features per 5x5 patch
    # input is nFeatures1 (number of features output from previous layer)
    with tf.name_scope('Conv2'):
     with tf.name_scope('weights'):
      W_conv2 = weight_variable([5, 5, nFeatures1, nFeatures2])
     with tf.name_scope('biases'):
      b_conv2 = bias_variable([nFeatures2])
     with tf.name_scope('filter'):
      h_conv2 = conv2d(h_pool1, W_conv2) + b_conv2
     with tf.name_scope('activations'):
      h_conv2_act = tf.nn.relu(h_conv2)
     with tf.name_scope('Pool'):
      h_pool2 = max_pool_2x2(h_conv2_act)
    
    # denseley connected layer. Similar to above, but operating
    # on entire image (rather than patch) which has been reduced by a factor of 4 
    # in each dimension
    # so use large number of neurons 
    
    # check our dimensions are a multiple of 4
    if (width%4 or height%4):
        print("Error: width and height must be a multiple of 4")
        sys.exit(1)
    
    with tf.name_scope('FC1'):
     with tf.name_scope('weights'):
      W_fc1 = weight_variable([int((width/4) * (height/4) * nFeatures2), nNeuronsfc])
     with tf.name_scope('biases'):
      b_fc1 = bias_variable([nNeuronsfc])
     with tf.name_scope('pre_activations'):
      h_pool2_flat = tf.reshape(h_pool2, [-1, int((width/4) * (height/4) * nFeatures2)])
      h_fc1 = tf.matmul(h_pool2_flat, W_fc1) + b_fc1
     with tf.name_scope('activations'):
      h_fc1_act = tf.nn.relu(h_fc1)
      
    # reduce overfitting by applying dropout
    # each neuron is kept with probability keep_prob
    with tf.name_scope('keep_prob'):
     keep_prob = tf.placeholder(tf.float32)
    
    
    # create readout layer which outputs to nClass categories
    with tf.name_scope('CL'):
     with tf.name_scope('weights'):
      W_fc2 = weight_variable([nNeuronsfc, nClass])
     with tf.name_scope('biases'):
      b_fc2 = bias_variable([nClass])
     with tf.name_scope('pre-activations-dropout'):
      h_fc1_drop = tf.nn.dropout(h_fc1_act, keep_prob)
     # define output calc (for each class) y = softmax(Wx+b)
     # softmax gives probability distribution across all classes
     # this is not run until later
      h_fcl = tf.matmul(h_fc1_drop, W_fc2) + b_fc2
     with tf.name_scope('activations-dopout'):
      h_fc1_act = tf.nn.relu(h_fcl)
     with tf.name_scope('final_pred'):
      y=tf.nn.softmax(h_fc1_act)  
    
    # measure of error of our model
    # this needs to be minimised by adjusting W and b
    with tf.name_scope('cross_entropy'):
     cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
    
    # define training step which minimises cross entropy
    with tf.name_scope('train'):
     train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
    
    # argmax gives index of highest entry in vector (1st axis of 1D tensor)
    with tf.name_scope('accuracy'):
     with tf.name_scope('correct_prediction'):
      correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
     with tf.name_scope('accuracy'):
    # get mean of all entries in correct prediction, the higher the better
      accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    # run the session
    nBatchVal = int(N_DATA_VAL/BATCH_SIZE)
    funcVal = 0
    with tf.Session() as sess:
     sess.run(tf.local_variables_initializer())
     sess.run(tf.global_variables_initializer())
    
    # start the threads used for reading files
     coord = tf.train.Coordinator()
     threads = tf.train.start_queue_runners(sess=sess,coord=coord)
     
     
     for i in range(nSteps):
        batch_xs, batch_ys = sess.run([imageBatch, labelBatch])
        #training step
        _ = sess.run(train_step, feed_dict={x:batch_xs, y_: batch_ys, keep_prob: 1.0-DROP_RATE})
        if i % 100 == 0:  # Record summaries and test-set accuracy
          run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
          run_metadata = tf.RunMetadata()
          vbatch_xs, vbatch_ys = sess.run([vimageBatch, vlabelBatch])
          acc = sess.run( accuracy, feed_dict={x:vbatch_xs, y_: vbatch_ys,keep_prob: 1.0-DROP_RATE})
          print('Accuracy at step %s: %s' % (i, acc))
     nValBatch = int((N_DATA_VAL/BATCH_SIZE)*N_EPOCHS)
     for k in range(nValBatch):
        vbatch_xs, vbatch_ys = sess.run([vimageBatch, vlabelBatch])
        acc = sess.run(accuracy, feed_dict={x:vbatch_xs, y_: vbatch_ys,keep_prob: 1.0-DROP_RATE})
        funcVal = acc+funcVal
     coord.request_stop()
     coord.join(threads)
    return funcVal/nValBatch


def obj_fn(space):
    BATCH_SIZE = space['BATCH_SIZE']
    nFeatures1 = space['nFeatures1']
    nFeatures2 = space['nFeatures2']
    nNeuronsfc = space['nNeuronsfc']
    dropout    = space['dropout']
    return {
        'BATCH_SIZE': space['BATCH_SIZE'],
        'nFeatures1': space['nFeatures1'],
        'nFeatures2': space['nFeatures2'],
        'nNeuronsfc': space['nNeuronsfc'],
        'dropout'   : space['dropout'],
        'loss': computeAccuracy(BATCH_SIZE,nFeatures1,nFeatures2,nNeuronsfc,dropout),
        'status': STATUS_OK,
        }


searchSpace = {
          'BATCH_SIZE':hp.quniform('BATCH_SIZE', 100, 300, 5),
          'nFeatures1':hp.quniform('nFeatures1', 24, 64, 2),
          'nFeatures2':hp.quniform('nFeatures2', 32, 128, 2),
          'nNeuronsfc':hp.quniform('nNeuronsfc', 512, 128, 2),
          'dropout'   :hp.uniform('dropout', 0.3, 0.8)
            } 

if __name__=='__main__':
    trials = Trials()
    #trials = MongoTrials('mongo://localhost:1234/foo_db/jobs', exp_key='exp1')
    best = fmin(
              fn=obj_fn,
              space=searchSpace,
              trials=trials,
              algo=tpe.suggest,
              max_evals=50
             )
  #accuracy = computeAccuracy()
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
