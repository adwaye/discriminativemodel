import tensorflow as tf
import numpy as np
from utils import _run
from checkTfrecords import *
from cnnModel1 import *
import itertools
from customEvalHook import *
from utils import update_dir
import cv2
from scipy.interpolate import griddata
nClass = 2

# Simple model (set to True) or convolutional neural network (set to False)
simpleModel = False

# Dimensions of image (pixels)
height = 32
width = 32
nClass = 2
shape = check_size()
DIMX_IM = shape[0]
DIMY_IM = shape[1]
PRE_RESIZE = False
BATCH_SIZE = 100
N_EPOCHS = 1
CAPACITY = 250
DEQUEUE = 10
DROP_RATE = 0.4
N_DATA = count_data()
nSteps = BATCH_SIZE * N_EPOCHS

imLoc = "/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/histNorm/1/NPSA0017h2012.png"
modelLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/trainedModel/12/savedModel'
Niter = 5000
modelName = modelLoc + '/model-' + str(Niter - (Niter % 1000) + 1)


trainFileName = "data/train-00000-of-00001"
testFileName  = "data/validation-00000-of-00001"
evalFileName  = "data/eval-00000-of-00001"



def parser(fullExample):
    # parse the full example into its' component features.
    features = tf.parse_single_example(
        fullExample,
        features={
            'image/height':tf.FixedLenFeature([],tf.int64),
            'image/width':tf.FixedLenFeature([],tf.int64),
            'image/colorspace':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/channels':tf.FixedLenFeature([],tf.int64),
            'image/class/label':tf.FixedLenFeature([],tf.int64),
            'image/class/text':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/format':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/filename':tf.FixedLenFeature([],dtype=tf.string,default_value=''),
            'image/encoded':tf.FixedLenFeature([],dtype=tf.string,default_value='')
            #'image/encoded': tf.VarLenFeature([], dtype=tf.string)
        })

    # now we are going to manipulate the label and image features

    label = features['image/class/label']
    image_buffer = features['image/encoded']

    # Decode the jpeg
    with tf.name_scope('decode_jpeg',[image_buffer],None):
        # decode
        image = tf.image.decode_jpeg(image_buffer,channels=1)

        # and convert to single precision data type
        image = tf.image.convert_image_dtype(image,dtype=tf.float32)#.set_shape([33,33,1])
        image.set_shape([DIMX_IM,DIMY_IM,1])

    # cast image into a single array, where each element corresponds to the greyscale
    # value of a single pixel.
    # the "1-.." part inverts the image, so that the background is black.


    image = tf.reshape(1 - tf.image.rgb_to_grayscale(image),[DIMX_IM * DIMY_IM])
    # re-define label as a "one-hot" vector
    # it will be [0,1] or [1,0] here.
    # This approach can easily be extended to more classes.
    label = tf.stack(tf.one_hot(label - 1,nClass))
    #image.set_shape([height*width,1])
    #label = tf.cast(features['image/class/label'], tf.int32)
    return label,image



def train_input_fn(fileName):
    dataset = tf.data.TFRecordDataset(fileName)
    dataset = dataset.map(parser)#,num_threads=2,output_buffer_size=batch_size)
    dataset = dataset.batch(batch_size=BATCH_SIZE)
    dataset = dataset.repeat()
    iterator= dataset.make_one_shot_iterator()
    labels, features = iterator.get_next()
    return features, labels


def predict_input_fn(fileName):
    dataset = tf.data.TFRecordDataset(fileName)
    dataset = dataset.map(parser)#,num_threads=2,output_buffer_size=batch_size)
    dataset = dataset.batch(batch_size=BATCH_SIZE)
    dataset = dataset.repeat()
    iterator= dataset.make_one_shot_iterator()
    labels, features = iterator.get_next()
    return features

def eval_input_fn(fileName):
    dataset = tf.data.TFRecordDataset(fileName)
    dataset = dataset.map(parser)#,num_threads=2,output_buffer_size=batch_size)
    dataset = dataset.batch(batch_size=BATCH_SIZE)
    dataset = dataset.repeat()
    iterator= dataset.make_one_shot_iterator()
    labels, features = iterator.get_next()

    with tf.Session() as sess:
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess=sess,coord=coord)
        sess.run(tf.global_variables_initializer())
        labs, feats = sess.run([labels,features])
    coord.request_stop()
    coord.join(threads)

    return feats, labs

"""
def train_input_fn(image,label,batch_size=250,DEQUEUE=100,capacity=2000):
    imageBatch,labelBatch = tf.train.shuffle_batch(
        [image,label],batch_size=BATCH_SIZE,
        capacity=CAPACITY,
        min_after_dequeue=DEQUEUE,
        num_threads=2,
        allow_smaller_final_batch=True)

    return imageBatch, labelBatch
"""

params = {
        'OUTPUT_DIR'    : "/tmp/mnist_convnet_model",
        'PRE_RESIZE'    : PRE_RESIZE,
        'nFeatures1'    : nFeatures1,
        'nFeatures2'    : nFeatures2,
        'nNeuronsfc1'   : nNeuronsfc1,
        'nNeuronsfc2'   : nNeuronsfc2,
        'LEARNING_RATE' : LEARNING_RATE,
        'DROP_RATE1'    : DROP_RATE1,
        'DROP_RATE2'    : DROP_RATE2,
        'DIMX_IM'       : DIMX_IM,
        'DIMY_IM'       : DIMY_IM,
        'N_DATA'        : N_DATA,
        'BATCH_SIZE'    : BATCH_SIZE,
        'N_EPOCHS'      : N_EPOCHS,
        'height'        : height,
        'width'         : width,
        'CAPACITY'      : CAPACITY
}

run_config = tf.estimator.RunConfig(save_summary_steps=100,
                                    )

tensors_to_log = {'cross_entropy':'cross_entropy/loss','accuracy':'accuracy/metric'}
#eval_hook = EvalSummarySaverHook(output_dir="/tmp/mnist_convnet_model")
#eval_hook = tf.train.LoggingTensorHook(tensors=tensors_to_log,every_n_iter=100)
#clf = tf.estimator.Estimator(model_fn=cnn_model_fn, model_dir='/tmp/mnist_convnet_model', params=params,
# config=run_config)
"""
patch_classifier = tf.estimator.Estimator(
    model_fn=cnnFunction,model_dir='/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/trainedModel/19/savedModel',params = params,config=run_config)


patch_classifier.train(
    input_fn=lambda:train_input_fn(trainFileName),
    steps=2000)
    #hooks=[logging_hook])

# Evaluate the model and print results

eval_results = patch_classifier.evaluate(
    input_fn=lambda:train_input_fn(testFileName),
    steps=nBatchVal
    #hooks = [eval_hook]
    )

#y = list(itertools.islice(eval_results))
#predictions = patch_classifier.predict(
#    input_fn=lambda:predict_input_fn(testFileName)
#    #,yield_single_examples=False
    #steps=nBatchVal
#)
#"""

def tf_sample_dense_batches(imLoc,patchSize=17,plot=False):

    strides   = 1
    im = np.expand_dims(np.expand_dims(1- cv2.cvtColor(cv2.imread(imLoc), cv2.COLOR_BGR2GRAY),0),3).astype(np.float32)

    tf_patches = tf.extract_image_patches(   im , ksizes=[1,patchSize,patchSize,1],
                                           strides=[1,strides,strides,1],rates=[1,1,1,1],padding='VALID' )
    tf_patches_resh = tf.reshape(tf_patches,[tf_patches.shape[1]*tf_patches.shape[2],tf_patches.shape[3]])

    dummyY = np.random.random((tf_patches_resh.shape[0].value,2))


    dataset = tf.data.Dataset.from_tensor_slices((tf_patches_resh))
    dataset = dataset.batch(batch_size=BATCH_SIZE)
    dataset = dataset.repeat()
    iterator= dataset.make_one_shot_iterator()
    features = iterator.get_next()

    if tf_patches_resh.shape[0].value%BATCH_SIZE==0:
        nBatch = int(tf_patches_resh.shape[0].value/BATCH_SIZE)
    else:
        nBatch = int(tf_patches_resh.shape[0].value / BATCH_SIZE) + 1
    return features


def tf_sample_save_patches(imLoc,patchSize=17,destLoc = './validationData/'):
    logdir = update_dir(destLoc)
    strides   = 1
    im = np.expand_dims(np.expand_dims(1- cv2.cvtColor(cv2.imread(imLoc), cv2.COLOR_BGR2GRAY),0),3).astype(np.float32)

    tf_patches = tf.extract_image_patches(   im , ksizes=[1,patchSize,patchSize,1],
                                           strides=[1,strides,strides,1],rates=[1,1,1,1],padding='VALID' )
    tf_patches_resh = tf.reshape(tf_patches,[tf_patches.shape[1]*tf_patches.shape[2],patchSize,patchSize])
    with tf.Session() as sess:
        print('converting patches to numpy array')
        patchCol = sess.run(tf_patches_resh)
        print('done converting to numpy array')
    print('saving into')
    os.mkdir(logdir+'/True')
    os.mkdir(logdir + '/False')
    with open(logdir + '/label.txt','a') as fp:
        fp.write('True\n')
        fp.write('False')
    for i in range(patchCol.shape[0]):
        cv2.imwrite(logdir+'/True'+'/patch' + str(i)+'.jpeg',patchCol[i,:,:])

    return patchCol

def turn_into_potential(preds,coords,im,method):
    xDim = im.shape[0]
    yDim = im.shape[1]
    fullX = np.arange(start=0,stop=xDim,step=1)
    fullY = np.arange(start=0,stop=yDim,step=1)
    grid_x,grid_y = np.meshgrid(fullY,fullX)
    grid_z0 = griddata(coords,preds,(grid_x,grid_y),method=method)
    return grid_z0

# Rebuild the input pipeline
#input_fn = train_input_fn(testFileName)#)
#features, labels = input_fn()
features, labels= train_input_fn(evalFileName)

#labels= tf_sample_dense_batches(imLoc,patchSize=17,plot=False)
# Rebuild the model
predictions = cnnFunction(features, tf.cast(labels,tf.float32), tf.estimator.ModeKeys.PREDICT,
                         params = params).predictions

# Manually load the latest checkpoint
saver  = tf.train.Saver()
config = tf.ConfigProto(gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction =0.75))

nPatch = count_data(evalFileName)
nReps  = int(nPatch/BATCH_SIZE)+1*(nPatch%BATCH_SIZE)
with tf.Session(config=config) as sess:
    #coord = tf.train.Coordinator()
    #threads = tf.train.start_queue_runners(sess=sess,coord=coord)
    ckpt = tf.train.get_checkpoint_state('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel'
                                         '/trainedModel/20/savedModel')
    saver.restore(sess, ckpt.model_checkpoint_path)
    sess.run(tf.local_variables_initializer())
    sess.run(tf.global_variables_initializer())
    # Loop through the batches and store predictions and labels
    prediction_values = {}
    label_values = []
    #i = 0
    #while True:
    #    try:
    #print('entering loop')
    for i in range(nReps):
            print(i)
            #feats = sess.run(labels)
            preds, lbls = sess.run([predictions, labels])
    #        i+=1
            if i ==0:
                probabilities  = preds['probabilities']
                classes        = preds['classes']
            else:
                probabilities = np.concatenate((probabilities,preds['probabilities']),axis=0)
                classes       = np.concatenate((classes,preds['classes']),axis=0)
    #coord.request_stop()
    #coord.join(threads)
cropIm  = cv2.imread('./validationData/testIm.png')
coords  = np.loadtxt('./validationData/1/coords.txt')
grid_z0 = turn_into_potential(probabilities[:,1],coords,cropIm,method='cubic')