from checkTfrecords import *
from build_image_data import _process_dataset
from _data import *
#from predict4 import *
from trainArchitectures import *
from tensorboard import main as tb
import matplotlib.pyplot as plt
import sys
sys.path.append('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/priorSegmentation')
from modifiedSnakes import *
from skimage.feature import peak_local_max

#todo: clean code; a stabilised version of this
class DiscriminativeModel(object):

    def __init__(self,trainFileName='./data/train-00000-of-00001',testFileName='./data/train-00000-of-00001', logDir=None, im=None,method='nearest'):
        """
        Class Discriminative model, uses a CNN to train a texture model that classifies patches as edges or not
        properties:
        trainFileName: TFRecordFile containing labels and patches used for training
        testFileName : TFRecordFile containing labels and patches used for training
        logDir       : directory to save the model. If set and contains savedModel as a directory, assumes model is
                       trained
        saveDir      : location where model checkpoints are saved
        istrained    : set to true is already trained
        im           : image needing an edge transofmr
        evalDir      : location of patches of im
        patchSize    : size of patches usd in training, obatined from checkTfrecords.checkSize(trainFileName)

        methods:
        fit(N_EPOCH,LEARNING_RATE) :
        fits the CNN USING THE trainFileName and testFilename

        predict() :
        constructs the edge transform of self.im using patches in evalDir, evalDir contains the coords of the patches
        and in a txt file, patches need to be in evalDir/Image
        returns preds,coords,grid_z0
        preds (N,2) array where N is number of patches
        coords (N,2) coordinate of patches corresponding to each prediction
        grid_z0 transpose of edge map

        predict_on_patches(self,im,patchLoc,plot=True)
        run predict with im and patchLoc arbitrary image and patchLocation

        predict_on_im(self,im,plot=True)
        turns im into patches and saves in a directory patchLoc
        run predict with im and patchLoc arbitrary image and patchLocation



        """

        patchSize = check_size(trainFileName)[0]
        self._patchSize = patchSize

        if im == None:
            raise UserWarning('need to feed an image to preedict on')

        if logDir == None:
            logDir = './trainedModel'
        self._logDir         = logDir
        self._tensorboardDir = logDir+'/tensorboard'
        self._savedDir       = logDir+'/savedModel'

        self._trainFileName = trainFileName
        self._testFileName  = testFileName

        self._im = im
        self._patchSize = patchSize
        if os.path.exists(os.path.join(logDir,'savedModel')):
            self.trained  = True
            self._savedDir = os.path.join(logDir,'savedModel')
            self._tensorboardDir = os.path.join(logDir,'tensorboard')
        else:
            self.trained= False

        self._labFile = None
        self._method  = method

    @property
    def patchSize(self):
        return self._patchSize

    @patchSize.setter
    def patchSize(self,val):
        self._patchSize = val

    @property
    def logDir(self):
        return self._logDir

    @logDir.setter
    def logDir(self,evalDir):
        if not os.path.isdir(os.path.join(evalDir,'savedModel')):
            self.trained=False
        self._logDir = evalDir

    @property
    def evalDir(self):
        return self._evalDir

    @evalDir.setter
    def evalDir(self,evalDir):
        self._evalDir = evalDir

    @property
    def trainFileName(self):
        return self._trainFileName

    @trainFileName.setter
    def trainFileName(self,file):
        self._trainFileName = file

    @property
    def testFileName(self):
        return self._testFileName


    @testFileName.setter
    def testFileName(self,file):
        self._testFileName = file

    @property
    def im(self):
        return self._im

    @im.setter
    def im(self,im):
        self._im = im



    def update_dirs(self,logDir):
        self.logDir         = logDir
        self._tensorboardDir = os.path.join(logDir,'tensorboard')
        self._savedDir       = os.path.join(logDir,'savedModel')

    def fit(self,N_EPOCHS=50,LEARNING_RATE=0.0001):
        if not self.trained:
            tf.reset_default_graph()
            dir  = trainModel(trainFileName=self.trainFileName,testFileName=self.testFileName,
                              path_to_save=self.logDir,N_EPOCHS=N_EPOCHS,LEARNING_RATE=LEARNING_RATE)
            #train on the tfrecords
            self.update_dirs(dir)
            self.trained=True
        else:
            print('already have a checkpoint')




    def predict_on_im(self,im,fullRes=True,plot=True):
        #will turn into a location of patches and predict on this
        #im can be a string pointing to an imloc or an actual ndaaray
        if type(im)==str:
            im = cv2.imread(im,0)
        im,logDir = patch_one_im(im,patchSize=self.patchSize,strides=6,destLoc = './validationData/')

        tf.reset_default_graph()
        if fullRes:
            skip = 1
        else:
            skip = 6
        preds = predict_from_im(im=im,modelLoc=self._savedDir,skip=skip)
        #preds   = predict_from_imLoc(dataLoc=os.path.join(logDir,'Image'),modelLoc=self._savedDir)
        coords  = np.loadtxt(os.path.join(logDir,'coords.txt'))
        if not fullRes:
            grid_z0 = turn_into_potential(preds[:,0],coords,im,method=self.method)
        else:
            grid_z0 = preds
        if plot:
            fig, axes = plt.subplots(nrows=1,ncols=2)
            fig.suptitle('Results applied on an image')
            axes[0].imshow(im)
            axes[0].set_title('original image')
            axes[1].imshow(np.transpose(grid_z0))
            axes[1].set_title('predictions at coordinates')
        return preds,coords,grid_z0






    def launch_tensorboard(self):
        if self.trained:
            tf.flags.FLAGS.logdir = self._tensorboardDir
            tb.main()
        return self


def useInSnakes():
 preds1,coords1,grid_z1 = my_model.predict_on_patches(im='./validationData/testIm.png',
                                                         patchLoc='./validationData/1/')
 imLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/priorSegmentation/testData/'
 imNames = __getPIX(imLoc)
 nIm = len(imNames)
 bone = 'RH2MP'
 curveLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
 nits = 200
 options = {'disp': False}
 options['maxiter'] = nits  # FIXME: check convergence
 method = 'BFGS'  # 'BFGS', 'CG', or 'Powell'. 'Nelder-Mead' has very slow convergence
 p = np.array([0.0])
 lat_pts = np.mean(X, axis=0)
 ndiscr = int(Y_dat.shape[1] / 2)
 theta_init = 0
 scoreArray = np.zeros((nIm,2))
 trueList  = []
 snakeList = []
 gpList    = []
 i = 0
 for imName in imNames[:,0]:
     im = scm.imread(imLoc+imName)
     Nx = im.shape[0]
     Ny = im.shape[1]
     if os.path.isfile(curveLoc + imName[:-4] + '_' + bone + '.txt'):
      cv = np.loadtxt(curveLoc + imName[:-4] + '_' + bone + '.txt')
      cv[:, 1] = Nx - cv[:, 1]
      true_pts = cv.astype(np.int32)
      true_mask = np.zeros((Nx, Ny))
      cv2.fillConvexPoly(true_mask, true_pts, (255, 255, 255))
      trueList = trueList+[true_mask]

      #true_potential = ndimage.distance_transform_edt(~true_mask.astype(bool))

      mask = np.load('enlarged_mask.npy')
      ridges = enhance_ridges(im)
      thresh = filters.threshold_otsu(ridges)
      prominent_ridges = ridges > 0.8*thresh
      skeleton = morphology.skeletonize(prominent_ridges)
      edge_dist = ndimage.distance_transform_edt(~skeleton)
      edge_dist = filters.gaussian(edge_dist, sigma=2)

      coords = np.loadtxt(imLoc + imName[:-4]+'_'+bone+'.txt') #initialisation based on circle drawn around object
      Na = 20
      coeffs = fourierFit3(coords[:,0],coords[:,1],T=10,Na=Na,nits=100)
      x,y    = fourierCurve(coeffs,10,ndiscr)
      y = im.shape[0]-y
      x = x[::-1]
      y = y[::-1]
      l = len(x)


      k = 0
      fig, axes = plt.subplots(nrows = 1, ncols=2, figsize=(10,12) )
      fig.suptitle(imName)
      for gp_weight in [0,2]:
       var_init = np.concatenate((lat_pts.ravel(),np.array([theta_init]),x,y   ),axis = 0)
       axes[k].imshow(im, cmap='gray')
       #axes[k].plot(x, y)
       bbox = axes[k].axis()
       textyLoc = bbox[2]+200
       textxLoc = bbox[0]
       axes[k].text(textxLoc,textyLoc,'gp weight = '+str(gp_weight)+', '+bone)
       line_obj, = axes[k].plot(x, y,'--',linewidth=1,color='k')

       optimal_latent_pts, c_pts, res = fit_snake_gplvm_1(var_init, edge_dist, Q=2,Ndisrc=110,gp_weight=gp_weight,
                                                          nits=nits, point_plot=line_obj)

       recon_pts = np.concatenate( (np.expand_dims(c_pts[0:l],1 ),np.expand_dims(c_pts[l:],1 )),1 ).astype(np.int32)
       false_mask = np.zeros((Nx, Ny))
       cv2.fillConvexPoly(false_mask, recon_pts, (255, 255, 255))
       #cv2.polylines(false_mask, [recon_pts], False, (255, 255, 255), 2)
       #recon_potential = ndimage.distance_transform_edt(~false_mask.astype(bool))
       #print(np.linalg.norm(recon_potential-true_potential))
       scoreArray[i,k] = adjusted_rand_score(false_mask.ravel(),true_mask.ravel())
       #fig.set_tight_layout(True)
       if k==0: snakeList=snakeList+[false_mask]
       if k==1:  gpList  =gpList+[false_mask]

       k += 1
      fig.tight_layout()
      fig.savefig('./RESULTS/' + imName + '_' + bone + '.png')
      #plt.close()
      i+=1
 return trueList, snakeList, gpList, scoreArray


if __name__=='__main__':
    """
    im = cv2.imread('./validationData/testIm.png',0)
    my_model = DiscriminativeModel(trainFileName='./data/train-00000-of-00001',
                                   testFileName='./data/validation-00000-of-00001',
                                   logDir='./trainedModel/3/',
                                    evalDir='./validationData/2/',im='./validationData/CPSA0054h2012.png')
    print('training and saving in')
    print(my_model.logDir)
    my_model.fit(N_EPOCHS=20)
    print('checkpoints saved in'+my_model._savedDir)
    print('tensorboard saved in' + my_model._tensorboardDir)


    #print('evaluating on '+my_model.evalDir+' and '+my_model.im)
    #preds0,coords0,grid_z0 = my_model.predict(plot=False)
    print('evaluating on ./validationData/testIm.png')
    my_model.method = 'cubic'
    preds1,coords1,grid_z1 =my_model.predict_on_patches(im='./validationData/testIm.png',
                                                        patchLoc='./validationData/1/')
    #preds1,coords1,grid_z1 =my_model.predict_on_im(im='./validationData/CPSA0054h2012.png')
    grid_z1[np.where(np.isnan(grid_z1))] = 0
    grid_skeleton = ((grid_z1>0.3)*255).astype('uint8')

    bone = 'RH2PP'
    coords = np.loadtxt('./validationData/testIm_'+bone+'1.txt') #initialisation based on circle drawn around
    """


    imName = 'CPSA0004h2012.png'
    imLoc  = './validationData/'
    predLoc  = './trainedModel/39/'
    bone     = 'RH2MP'
    label  = 0
    im = cv2.imread(os.path.join(imLoc,imName),0)
    nits = 200
    options = {'disp':False}
    options['maxiter'] = nits  # FIXME: check convergence
    method = 'BFGS'  # 'BFGS', 'CG', or 'Powell'. 'Nelder-Mead' has very slow convergence
    #p = np.array([0.0])
    #lat_pts = np.mean(X,axis=0)
    #ndiscr = int(Y_dat.shape[1] / 2)
    #theta_init = 0
    Nx = im.shape[0]
    Ny = im.shape[1]

    matFile = sio.loadmat(os.path.join(predLoc,imName[:-4]+'.mat'))

    preds   = matFile['preds']
    mask    = np.argmax(preds,2)
    #todo:get edge dist from preds
    edge_dist = ndimage.distance_transform_edt(mask == 2)
    edge_dist = filters.gaussian(edge_dist,sigma=2)


    coords = np.loadtxt(imLoc + imName[:-4] + '_' + bone + '_init.txt') #initialisation based on circle drawn around
    # object
    #Na = 20
    #coeffs = fourierFit3(coords[:,0],coords[:,1],T=10,Na=Na,nits=100)
    #x,y = fourierCurve(coeffs,10,ndiscr)

    x = coords[:,0]
    y = coords[:,1]
    y = im.shape[0] - y
    #l = len(x)
    k = 0

    for gp_weight in [2]:
        var_init = np.concatenate((x,y),axis=0)
        snakes_gp = modifiedSnakes(im=im,potential=edge_dist,init_curve=var_init,bone=bone)

        res = snakes_gp.fit_snake(plot=True,nits=nits)
        Q = snakes_gp._Q
        c_pts = res.x[Q + 1:]
        l = snakes_gp._Ndiscr
        recon_pts = np.concatenate((np.expand_dims(c_pts[0:l],1),np.expand_dims(c_pts[l:],1)),1).astype(
            np.int32)

        k += 1
        plt.savefig(os.path.join(predLoc,imName[:-4]+'snakes_gp'+str(gp_weight) +'.png' ))



    # object
    #Na = 20
    #coeffs = fourierFit3(coords[:,0],coords[:,1],T=10,Na=Na,nits=100)
    #x,y = fourierCurve(coeffs,10,ndiscr)
    """
    x = coords[:,0]
    y = coords[:,1]
    y = im.shape[0] - y
    var_init = np.concatenate((x,y),axis=0)


    #edge_dist_CNN = ndimage.distance_transform_edt(~grid_skeleton)
    edge_dist_CNN = filters.gaussian(1-grid_z1,sigma=10)
    snakes_CNN    = modifiedSnakes(im=im,potential=np.transpose(edge_dist_CNN),init_curve=var_init,bone=bone,)
    snakes_CNN.fit_snake(plot=True,gp_weight=2,curvature_weight=0.5,spacing_weight=0.25)


    ridges = enhance_ridges(im)
    thresh = filters.threshold_otsu(ridges)
    prominent_ridges = ridges > 0.8 * thresh
    skeleton = morphology.skeletonize(prominent_ridges)
    edge_dist = ndimage.distance_transform_edt(~prominent_ridges)
    snakes_dist = modifiedSnakes(im=im,potential=edge_dist,init_curve=var_init,bone=bone)
    snakes_dist.fit_snake(plot=True)
    #edge_dist = filters.gaussian(edge_dist,sigma=2)

    cv2.imwrite('./validationData/edgeDistCnn.png',np.transpose(edge_dist_CNN))
    cv2.imwrite('./validationData/edgesSekeleton.png',np.transpose(grid_skeleton))
    cv2.imwrite('./validationData/edgesPredictions.png',np.transpose(grid_z1))

#    img = cv2.imread('./validationData/testIm.png',0)
#    edges = cv2.Canny(img,100,300)
#    plt.subplot(121),plt.imshow(img,cmap='gray')
#    plt.title('Original Image'),plt.xticks([]),plt.yticks([])
#    plt.subplot(122),plt.imshow(edges,cmap='gray')
#    plt.title('Edge Image'),plt.xticks([]),plt.yticks([])

    lambdastr = str(input("enter value of lambda: "))
    order66  = "cd /home/amr62/fastms; ./main -edges 1 -i /home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/validationData/edgeDistCnn.png -alpha -1 -lambda 2 -show 1 -use_double 0  -save /"
    os.system(order66)
#    plt.show()

    """