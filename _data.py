import os
from skimage import filters
import socket
x = socket.gethostname()
if x != 'mapc-lapsup74':
    import matplotlib as mpl
    mpl.use('agg')
import numpy as np
import locations



import matplotlib.pyplot as plt
plt.rcParams['image.cmap']='gray'
plt.ion()
from mpl_toolkits.mplot3d import Axes3D
import scipy.io as sio
from sklearn.feature_extraction.image import extract_patches_2d
import cv2
import tensorflow as tf
from utils import update_dir, _run
#from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from matplotlib.pyplot import imread
import shutil
from build_image_data import _process_dataset
import errno

def make_labs(patchCol):
    labList = []
    for k in range(patchCol.shape[0]):
        lab = np.sum((patchCol[k,:,:]).astype(np.int)) > 0
        labList = labList + [lab]
    return labList




# Reads an image from a file
def read_and_convert(patchLoc  = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/patches/2',valRatio=20 ):
    filenames = [os.path.join(patchLoc, file) for file in sorted(os.listdir(patchLoc) ,reverse = False)][1:]
    labels    = np.loadtxt(patchLoc+'/labels.txt').astype(np.int16)
    index     = range(len(labels))
    train_index, test_index = train_test_split(index, test_size=0.2)
    
    image_array = np.expand_dims(imread(filenames[1]).flatten(),0).astype(np.float32)
    for files in filenames[2:]:
      print(files)
      image_array = np.concatenate(  (image_array,np.expand_dims(imread(filenames[1]).flatten(),0)) ,axis= 0 )
    xtrain = image_array[train_index,:]
    ytrain = labels[train_index]
    xtest  = image_array[test_index,:]
    ytest  = labels[test_index]
    return xtrain, xtest, ytrain, ytest


# reads in each x-ray, turns them into patches and puts them in True foleder if there is an edge and in the false folder if there is no edge
def patchAtMidCoords(array,x,y,size):
    Nx = array.shape[0]
    Ny = array.shape[1]
    width = int(size/2)+1
    cond = x<width or y<width or Nx-x<width or Ny-y<width

    if cond:
        patch = np.zeros((patchSize,patchSize))
    else:


        template = np.zeros((size,size))
        step = int(size/2)
        startX = max(x-step,0)
        endX   = min(x+step,Nx)
        startY = max(y-step,0)
        endY   = min(y+step,Ny)
        patch = array[startX-1:endX,startY-1:endY]#.copy()

    return patch


#needs to output startInd, endInd: nPatch per im
def savePatch(im,mask,trueLoc,falseLoc,patchSize,startInd,prate=1.5,spacing=1.7):
    #prate  = 1.5
    xrdMax = 10
    yrdMax = 20
    skip   = patchSize+1
    coords = np.where(mask)
    xMid  = coords[0][0]
    yMid  = coords[1][0]
    xArrP   = np.array([xMid])
    yArrP   = np.array([yMid])
    patch = patchAtMidCoords(im,xMid,yMid,patchSize)
    cv2.imwrite(os.path.join(trueLoc,'patch'+str(startInd)+'.jpeg'),patch)
    endInd = startInd
    k = 0
    for xMid, yMid in zip(coords[0][1:],coords[1][1:]):
      if np.sum( np.abs(xArrP-xMid) + np.abs(yArrP-yMid) < spacing*skip)==0:
       xArrP= np.append( xArrP,xMid )
       yArrP= np.append( yArrP,yMid )
       #print(xMid)
       #print(yMid)
       #print('=========')
       #xtemp = coords[0][k]
       #ytemp = coords[1][k]
       #tempIm = mask.astype(np.uint8)
       truePatch = patchAtMidCoords(im,xMid,yMid,patchSize)
       if (np.sum(np.isnan(truePatch)) == 0) and (truePatch.shape == (patchSize,patchSize)):
               cv2.imwrite(os.path.join(trueLoc,'patch'+str(endInd)+'_'+str(xMid)+'_'+str(yMid)+'.jpeg'),truePatch)
       endInd+=1
       #poisson number of false patch rate approx 1 to have equal true and false on avrg
       nfalsePatch = max(1,np.random.poisson(prate))
       ind = 0
       for k in range(nfalsePatch):
           #random search for a false patch around the true patch
         xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
         yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
         while np.sum(patchAtMidCoords(mask,xMid+xrd,yMid+yrd,patchSize))!=0:
            xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
            yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
            ind += 1
            if ind>100:
                break
         falsePatch = patchAtMidCoords(im,xMid+xrd,yMid+yrd,patchSize)
         if (np.sum(np.isnan(falsePatch)) == 0) and (falsePatch.shape == (patchSize,patchSize)):
            cv2.imwrite(os.path.join(falseLoc,'patch'+str(endInd)+'_'+str(xMid+xrd)+'_'+str(yMid+yrd)+'.jpeg'),
                        falsePatch)
         endInd+=1

      k+=1
    return endInd


def drawPatch(im,mask,patchSize,prate=1.5,spacing=1.7,_plot=False):
    #prate  = 1.5
    xrdMax = 10
    yrdMax = 60
    skip   = patchSize+1
    coords = np.where(mask>0)
    xMid  = coords[0][0]
    yMid  = coords[1][0]
    xArrP   = np.array([xMid])
    yArrP   = np.array([yMid])
    cv2.rectangle(img=im,pt1=(xMid - int(patchSize / 2),yMid - int(patchSize / 2)),
                  pt2=(xMid + int(patchSize / 2),yMid + int(patchSize / 2)),thickness=2,lineType=8,shift=0,color=0)
    k = 0
    for xMid, yMid in zip(coords[0],coords[1]):
      if np.sum( np.add(np.abs(xArrP-xMid) , np.abs(yArrP-yMid)) < spacing*skip)==0:
       xArrP= np.append( xArrP,xMid )
       yArrP= np.append( yArrP,yMid )
       cv2.rectangle(img=im,pt1=(yMid-int(patchSize/2),xMid-int(patchSize/2)),pt2=(yMid+int(patchSize/2),xMid+int(patchSize/2)),thickness=3,lineType=8,shift=0,color=0)
       #poisson number of false patch rate approx 1 to have equal true and false on avrg
       nfalsePatch = max(1,np.random.poisson(prate))
       ind = 0
       for k in range(nfalsePatch):
           #random search for a false patch around the true patch
         xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
         yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
         while np.sum(patchAtMidCoords(mask,xMid+xrd,yMid+yrd,patchSize))!=0:
            xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
            yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
            ind += 1
            if ind>100:
                break
         #falsePatch = patchAtMidCoords(im,xMid+xrd,yMid+yrd,patchSize)
         cv2.rectangle(img=im,pt1=(yMid+yrd - int(patchSize / 2),xMid+xrd - int(patchSize / 2)),
                        pt2=(yMid+yrd + int(patchSize / 2),xMid+xrd + int(patchSize / 2)),thickness=-1,lineType=8,
                       shift=0,color=0)
      k+=1
      #print(k)
    if _plot:
        plt.figure()
        plt.imshow(im)
        plt.title('one mask used for all bones')

        plt.show()
    return im


def savePatchRefinedExtended(im,bigMask,maskList,handMask,boneLoc,tissueLoc,falseLoc,patchSize,startInd,
                             drawmode=False,prate1=1.3,prate2=1.4,spacing = 2.5):
    xDim = im.shape[0]
    yDim = im.shape[1]
    paddedIm = np.zeros((xDim + 2 * int(patchSize / 2) + 2,yDim + 2 * int(patchSize / 2) + 2))
    paddedIm[int(patchSize / 2) + 1:xDim + int(patchSize / 2) + 1,
    int(patchSize / 2) + 1:yDim + int(patchSize / 2) + 1] = im[:,:]
    paddedMask = np.zeros((xDim + 2 * int(patchSize / 2) + 2,yDim + 2 * int(patchSize / 2) + 2))
    paddedMask[int(patchSize / 2) + 1:xDim + int(patchSize / 2) + 1,
    int(patchSize / 2) + 1:yDim + int(patchSize / 2) + 1] = bigMask[:,:]
    xrdMax = 10
    yrdMax = 20
    skip   = patchSize+1
    endInd = startInd
    xArrP = np.array([])
    yArrP = np.array([])
    for mask in maskList:
        coords = np.where(mask)
        coordx = coords[0]
        coordy = coords[1]
        np.random.seed(1)
        np.random.shuffle(coordx)
        np.random.seed(1)
        np.random.shuffle(coordy)
        xMid = coordx[0] + int(patchSize / 2) + 1
        yMid = coordy[0] + int(patchSize / 2) + 1
        xArrP = np.array([xMid])
        yArrP = np.array([yMid])
        if drawmode:
            cv2.rectangle(img=paddedIm,pt1=(yMid - int(patchSize / 2),xMid - int(patchSize / 2)),
                          pt2=(yMid + int(patchSize / 2),xMid + int(patchSize / 2)),thickness=-1,
                          lineType=8,
                          shift=0,color=0)
        else:
            truePatch = patchAtMidCoords(paddedIm,xMid,yMid,patchSize)
            if (np.sum(np.isnan(truePatch))==0) and (truePatch.shape == (patchSize,patchSize)):
                cv2.imwrite(boneLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) +'.jpeg',truePatch)
                endInd += 1
        for xMid, yMid in zip(coordx[1:]+ int(patchSize / 2) + 1,coordy[1:]+ int(patchSize / 2) + 1):
            if np.sum( np.add(np.abs(xArrP-xMid) , np.abs(yArrP-yMid)) < 1*skip)==0:
                xArrP= np.append( xArrP,xMid )
                yArrP= np.append( yArrP,yMid )
                if drawmode:
                    cv2.rectangle(img=paddedIm,pt1=(yMid - int(patchSize / 2),xMid - int(patchSize / 2)),
                                  pt2=(yMid +  int(patchSize / 2),xMid  + int(patchSize / 2)),thickness=3,
                                  lineType=8,
                                  shift=0,color=0)
                else:
                    truePatch = patchAtMidCoords(paddedIm,xMid,yMid,patchSize)
                    if (np.sum(np.isnan(truePatch) ) == 0) and (truePatch.shape == (patchSize,patchSize)):
                        cv2.imwrite(boneLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',truePatch)
                        endInd += 1
                #cv2.rectangle(img=im,pt1=(yMid-int(patchSize/2),xMid-int(patchSize/2)),pt2=(yMid+int(patchSize/2),xMid+int(patchSize/2)),thickness=3,lineType=8,shift=0,color=0)
                #poisson number of false patch rate approx 1 to have equal true and false on avrg
                nfalsePatch = max(1,np.random.poisson(prate1))
                for k in range(nfalsePatch):
                       #random search for a false patch around the true patch
                     xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
                     yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
                     ind = 0
                     flag = True
                     while flag:
                        xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
                        yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
                        ind+=1
                        flag = np.sum(patchAtMidCoords(paddedMask,xMid + xrd,yMid + yrd,patchSize)) != 0
                        if ind>100:
                            break
                     if not flag:
                         if drawmode:
                             cv2.rectangle(img=paddedIm,pt1=(yMid+yrd - int(patchSize / 2),xMid+xrd - int(patchSize /
                                                                                                         2)),
                                           pt2=(yMid+yrd + int(patchSize / 2),xMid+xrd + int(patchSize / 2)),
                                           thickness=-1,
                                           lineType=8,
                                           shift=0,color=0)
                         else:
                            falsePatch = patchAtMidCoords(paddedIm,xMid+xrd,yMid+yrd,patchSize)
                            if (np.sum(np.isnan(falsePatch) )== 0) and (falsePatch.shape == (patchSize,patchSize)):
                                cv2.imwrite(falseLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',falsePatch)
                                endInd += 1
             #cv2.rectangle(img=im,pt1=(yMid+yrd - int(patchSize / 2),xMid+xrd - int(patchSize / 2)),pt2=(yMid+yrd + int(patchSize / 2),xMid+xrd + int(patchSize / 2)),thickness=-1,lineType=8,shift=0,color=0)
    if np.sum(handMask)>0:
        print('starting tissue patches now')
        #"""
        coords = np.where(handMask)
        coordx = coords[0]
        coordy = coords[1]
        np.random.seed(1)
        np.random.shuffle(coordx)
        np.random.seed(1)
        np.random.shuffle(coordy)
        xrdMax = 10
        yrdMax = 10
        for xMid,yMid in zip(coordx+ int(patchSize / 2) + 1,coordy+ int(patchSize / 2) + 1):
            if np.sum(np.add(np.abs(xArrP - xMid),np.abs(yArrP - yMid)) < spacing * skip) == 0:
                xArrP = np.append(xArrP,xMid)
                yArrP = np.append(yArrP,yMid)
                if drawmode:
                    cv2.rectangle(img=paddedIm,pt1=(yMid - int(patchSize / 2),xMid - int(patchSize / 2)),
                                  pt2=(yMid +  int(patchSize / 2),xMid  + int(patchSize / 2)),thickness=3,
                                  lineType=8,
                                  shift=0,color=0)
                else:
                    truePatch = patchAtMidCoords(paddedIm,xMid,yMid,patchSize)
                    if (np.sum(np.isnan(truePatch)) == 0) and (truePatch.shape == (patchSize,patchSize)):
                        cv2.imwrite(tissueLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',
                                    truePatch)
                        endInd += 1
                #poisson number of false patch rate approx 1 to have equal true and false on avrg
                nfalsePatch = max(1,np.random.poisson(prate2))
                for k in range(nfalsePatch):
                    #random search for a false patch around the true patch
                    xrd = np.random.choice([-1,1]) * np.random.randint(low=int(patchSize / 2) + 1,
                                                                       high=int(patchSize / 2) + xrdMax)
                    yrd = np.random.choice([-1,1]) * np.random.randint(low=int(patchSize / 2) + 1,
                                                                       high=int(patchSize / 2) + yrdMax)
                    ind = 0
                    flag = True
                    while flag:
                        xrd = np.random.choice([-1,1]) * np.random.randint(low=int(patchSize / 2) + 1,
                                                                           high=int(patchSize / 2) + xrdMax)
                        yrd = np.random.choice([-1,1]) * np.random.randint(low=int(patchSize / 2) + 1,
                                                                           high=int(patchSize / 2) + yrdMax)
                        ind += 1
                        flag = np.sum(patchAtMidCoords(paddedMask,xMid + xrd,yMid + yrd,patchSize)) != 0
                        if ind > 100:
                            break
                        if not flag:
                            if drawmode:
                                cv2.rectangle(img=paddedIm,
                                              pt1=(yMid + yrd - int(patchSize / 2),xMid + xrd - int(patchSize /
                                                                                                    2)),
                                              pt2=(yMid + yrd + int(patchSize / 2),xMid + xrd + int(patchSize / 2)),
                                              thickness=-1,
                                              lineType=8,
                                              shift=0,color=0)
                            else:
                                falsePatch = patchAtMidCoords(paddedIm,xMid + xrd,yMid + yrd,patchSize)
                                if (np.sum(np.isnan(falsePatch) )== 0) and (falsePatch.shape == (patchSize,patchSize)):
                                    cv2.imwrite(
                                    falseLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',
                                    falsePatch)
                                    endInd += 1
    print('done saving bone and tissue edges')
    if drawmode:
        plt.figure()
        plt.imshow(paddedIm)
        plt.title('separate masks used for each outline')
        plt.show()
    return endInd





def savePatchInOut(im,maskIn,maskEdge,handMask,edgeLoc,tissueLoc,inLoc,falseLoc,patchSize,startInd,
                             drawmode=False,prate1=1.3,prate2=1.4,prate3=1.4,spacing1 = 2.5,spacing2=2.5,spacing3=2.5):
    xDim = im.shape[0]
    yDim = im.shape[1]
    paddedIm = np.zeros((xDim + 2 * int(patchSize / 2) + 2,yDim + 2 * int(patchSize / 2) + 2))
    paddedIm[int(patchSize / 2) + 1:xDim + int(patchSize / 2) + 1,
    int(patchSize / 2) + 1:yDim + int(patchSize / 2) + 1] = im[:,:]
    paddedMask = np.zeros((xDim + 2 * int(patchSize / 2) + 2,yDim + 2 * int(patchSize / 2) + 2))
    paddedMask[int(patchSize / 2) + 1:xDim + int(patchSize / 2) + 1,
    int(patchSize / 2) + 1:yDim + int(patchSize / 2) + 1] = (maskIn[:,:]+handMask[:,:])>0
    if drawmode:
        colorPad = np.repeat(np.expand_dims(paddedIm,axis=2),3,axis=2)
    xrdMax = 10
    yrdMax = 20
    skip   = patchSize+1
    endInd = startInd
    xArrP = np.array([])
    yArrP = np.array([])
    if np.sum(maskIn)>0:
        coords = np.where(maskIn)
        coordx = coords[0]
        coordy = coords[1]
        np.random.seed(1)
        np.random.shuffle(coordx)
        np.random.seed(1)
        np.random.shuffle(coordy)
        xMid = coordx[0] + int(patchSize / 2) + 1
        yMid = coordy[0] + int(patchSize / 2) + 1
        xArrP = np.array([xMid])
        yArrP = np.array([yMid])
        if np.sum(patchAtMidCoords(maskEdge,xMid,yMid,patchSize))==0:
            if drawmode:
                cv2.rectangle(img=paddedIm,pt1=(yMid - int(patchSize / 2),xMid - int(patchSize / 2)),
                              pt2=(yMid + int(patchSize / 2),xMid + int(patchSize / 2)),thickness=3,
                              lineType=8,
                              shift=0,color=0)
            else:
                truePatch = patchAtMidCoords(paddedIm,xMid,yMid,patchSize)
                if (np.sum(np.isnan(truePatch))==0) and (truePatch.shape == (patchSize,patchSize)):
                    cv2.imwrite(inLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) +'.jpeg',truePatch)
                    endInd += 1
        for xMid, yMid in zip(coordx[1:]+ int(patchSize / 2) + 1,coordy[1:]+ int(patchSize / 2) + 1):
            if np.sum( np.add(np.abs(xArrP-xMid) , np.abs(yArrP-yMid)) < spacing1*skip)==0 and np.sum(patchAtMidCoords(
                    maskEdge,xMid,yMid,patchSize))==0:
                xArrP= np.append( xArrP,xMid )
                yArrP= np.append( yArrP,yMid )
                if drawmode:
                    cv2.rectangle(img=paddedIm,pt1=(yMid - int(patchSize / 2),xMid - int(patchSize / 2)),
                                  pt2=(yMid +  int(patchSize / 2),xMid  + int(patchSize / 2)),thickness=3,
                                  lineType=8,
                                  shift=0,color=0)
                else:
                    truePatch = patchAtMidCoords(paddedIm,xMid,yMid,patchSize)
                    if (np.sum(np.isnan(truePatch) ) == 0) and (truePatch.shape == (patchSize,patchSize)):
                        cv2.imwrite(inLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',truePatch)
                        endInd += 1
                #cv2.rectangle(img=im,pt1=(yMid-int(patchSize/2),xMid-int(patchSize/2)),pt2=(yMid+int(patchSize/2),xMid+int(patchSize/2)),thickness=3,lineType=8,shift=0,color=0)
                #poisson number of false patch rate approx 1 to have equal true and false on avrg
                nfalsePatch = max(1,np.random.poisson(prate1))
                for k in range(nfalsePatch):
                    #random search for a false patch around the true patch
                    xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
                    yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
                    ind = 0
                    flag = True
                    while flag:
                        xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
                        yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
                        ind+=1
                        flag = np.sum(patchAtMidCoords(paddedMask,xMid + xrd,yMid + yrd,patchSize)) != 0
                        if ind>100:
                            break
                    if not flag:
                        if drawmode:
                            cv2.rectangle(img=paddedIm,pt1=(yMid+yrd - int(patchSize / 2),xMid+xrd - int(patchSize /
                                                                                                         2)),
                                          pt2=(yMid+yrd + int(patchSize / 2),xMid+xrd + int(patchSize / 2)),
                                          thickness=-1,
                                          lineType=8,
                                          shift=0,color=0)
                        else:
                            falsePatch = patchAtMidCoords(paddedIm,xMid+xrd,yMid+yrd,patchSize)
                            if (np.sum(np.isnan(falsePatch) )== 0) and (falsePatch.shape == (patchSize,patchSize)):
                                cv2.imwrite(falseLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',falsePatch)
                                endInd += 1
                                #cv2.rectangle(img=im,pt1=(yMid+yrd - int(patchSize / 2),xMid+xrd - int(patchSize / 2)),pt2=(yMid+yrd + int(patchSize / 2),xMid+xrd + int(patchSize / 2)),thickness=-1,lineType=8,shift=0,color=0)
    if np.sum(maskEdge)>0:
        coords = np.where(maskEdge)
        coordx = coords[0]
        coordy = coords[1]
        np.random.seed(1)
        np.random.shuffle(coordx)
        np.random.seed(1)
        np.random.shuffle(coordy)
        xMid = coordx[0] + int(patchSize / 2) + 1
        yMid = coordy[0] + int(patchSize / 2) + 1
        xArrP = np.array([xMid])
        yArrP = np.array([yMid])

        if drawmode:
            cv2.circle(paddedIm,(yMid,xMid),patchSize,(0,255,0),3)
        else:
            truePatch = patchAtMidCoords(paddedIm,xMid,yMid,patchSize)
            if (np.sum(np.isnan(truePatch))==0) and (truePatch.shape == (patchSize,patchSize)):
                cv2.imwrite(edgeLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) +'.jpeg',truePatch)
                endInd += 1
        for xMid, yMid in zip(coordx[1:]+ int(patchSize / 2) + 1,coordy[1:]+ int(patchSize / 2) + 1):
            if np.sum( np.add(np.abs(xArrP-xMid) , np.abs(yArrP-yMid)) < spacing2*skip)==0:
                xArrP= np.append( xArrP,xMid )
                yArrP= np.append( yArrP,yMid )
                if drawmode:
                    cv2.circle(paddedIm,(yMid,xMid),patchSize, (0,255,0), 3)
                else:
                    truePatch = patchAtMidCoords(paddedIm,xMid,yMid,patchSize)
                    if (np.sum(np.isnan(truePatch) ) == 0) and (truePatch.shape == (patchSize,patchSize)):
                        cv2.imwrite(edgeLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',
                                    truePatch)
                        endInd += 1
                #cv2.rectangle(img=im,pt1=(yMid-int(patchSize/2),xMid-int(patchSize/2)),pt2=(yMid+int(patchSize/2),xMid+int(patchSize/2)),thickness=3,lineType=8,shift=0,color=0)
                #poisson number of false patch rate approx 1 to have equal true and false on avrg
                nfalsePatch = max(1,np.random.poisson(prate2))
                for k in range(nfalsePatch):
                    #random search for a false patch around the true patch
                    xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
                    yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
                    ind = 0
                    flag = True
                    while flag:
                        xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
                        yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
                        ind+=1
                        flag = np.sum(patchAtMidCoords(paddedMask,xMid + xrd,yMid + yrd,patchSize)) != 0
                        if ind>100:
                            break
                    if not flag:
                        if drawmode:
                            cv2.rectangle(img=paddedIm,pt1=(yMid+yrd - int(patchSize / 2),xMid+xrd - int(patchSize /
                                                                                                         2)),
                                          pt2=(yMid+yrd + int(patchSize / 2),xMid+xrd + int(patchSize / 2)),
                                          thickness=-1,
                                          lineType=8,
                                          shift=0,color=0)
                        else:
                            falsePatch = patchAtMidCoords(paddedIm,xMid+xrd,yMid+yrd,patchSize)
                            if (np.sum(np.isnan(falsePatch) )== 0) and (falsePatch.shape == (patchSize,patchSize)):
                                cv2.imwrite(falseLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',falsePatch)
                                endInd += 1

    if np.sum(handMask)>0:
        print('starting tissue patches now')
        #"""
        coords = np.where(handMask)
        coordx = coords[0]
        coordy = coords[1]
        np.random.seed(1)
        np.random.shuffle(coordx)
        np.random.seed(1)
        np.random.shuffle(coordy)
        xrdMax = 10
        yrdMax = 10
        for xMid,yMid in zip(coordx+ int(patchSize / 2) + 1,coordy+ int(patchSize / 2) + 1):
            if np.sum(np.add(np.abs(xArrP - xMid),np.abs(yArrP - yMid)) < spacing3 * skip) == 0:
                xArrP = np.append(xArrP,xMid)
                yArrP = np.append(yArrP,yMid)
                if drawmode:
                    cv2.circle(paddedIm,(yMid,xMid),patchSize,(0,255,0),-1)
                else:
                    truePatch = patchAtMidCoords(paddedIm,xMid,yMid,patchSize)
                    if (np.sum(np.isnan(truePatch)) == 0) and (truePatch.shape == (patchSize,patchSize)):
                        cv2.imwrite(tissueLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',
                                    truePatch)
                        endInd += 1
                #poisson number of false patch rate approx 1 to have equal true and false on avrg
                nfalsePatch = max(1,np.random.poisson(prate3))
                for k in range(nfalsePatch):
                    #random search for a false patch around the true patch
                    xrd = np.random.choice([-1,1]) * np.random.randint(low=int(patchSize / 2) + 1,
                                                                       high=int(patchSize / 2) + xrdMax)
                    yrd = np.random.choice([-1,1]) * np.random.randint(low=int(patchSize / 2) + 1,
                                                                       high=int(patchSize / 2) + yrdMax)
                    ind = 0
                    flag = True
                    while flag:
                        xrd = np.random.choice([-1,1]) * np.random.randint(low=int(patchSize / 2) + 1,
                                                                           high=int(patchSize / 2) + xrdMax)
                        yrd = np.random.choice([-1,1]) * np.random.randint(low=int(patchSize / 2) + 1,
                                                                           high=int(patchSize / 2) + yrdMax)
                        ind += 1
                        flag = np.sum(patchAtMidCoords(paddedMask,xMid + xrd,yMid + yrd,patchSize)) != 0
                        if ind > 100:
                            break
                        if not flag:
                            if drawmode:
                                cv2.rectangle(img=paddedIm,
                                              pt1=(yMid + yrd - int(patchSize / 2),xMid + xrd - int(patchSize /
                                                                                                    2)),
                                              pt2=(yMid + yrd + int(patchSize / 2),xMid + xrd + int(patchSize / 2)),
                                              thickness=-1,
                                              lineType=8,
                                              shift=0,color=0)
                            else:
                                falsePatch = patchAtMidCoords(paddedIm,xMid + xrd,yMid + yrd,patchSize)
                                if (np.sum(np.isnan(falsePatch) )== 0) and (falsePatch.shape == (patchSize,patchSize)):
                                    cv2.imwrite(
                                    falseLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',
                                    falsePatch)
                                    endInd += 1


    if drawmode:
        plt.figure()
        plt.imshow(paddedIm)
        plt.title('separate masks used for each outline')
        plt.show()
    return endInd



def savePatchBoneTissue(im,maskBone,maskHand,maskEdge,boneLoc,tissueLoc,falseLoc,patchSize,startInd,
                             drawmode=False,prate1=1.3,prate2=1.4,prate3=1.4,spacing1 = 2.5,spacing2=2.5,spacing3=2.5):
    xDim = im.shape[0]
    yDim = im.shape[1]
    paddedIm = np.zeros((xDim + 2 * int(patchSize / 2) + 2,yDim + 2 * int(patchSize / 2) + 2))
    paddedIm[int(patchSize / 2) + 1:xDim + int(patchSize / 2) + 1,
    int(patchSize / 2) + 1:yDim + int(patchSize / 2) + 1] = im[:,:]
    paddedMask = np.zeros((xDim + 2 * int(patchSize / 2) + 2,yDim + 2 * int(patchSize / 2) + 2))
    paddedMask[int(patchSize / 2) + 1:xDim + int(patchSize / 2) + 1,
    int(patchSize / 2) + 1:yDim + int(patchSize / 2) + 1] = (maskBone[:,:]+maskHand[:,:])>0

    xrdMax = 30
    yrdMax = 50
    skip   = patchSize+1
    endInd = startInd
    xArrP = np.array([])
    yArrP = np.array([])
    if np.sum(maskBone)>0 and np.sum(maskHand)>0:
        coords = np.where(maskBone)
        coordx = coords[0]
        coordy = coords[1]
        np.random.seed(1)
        np.random.shuffle(coordx)
        np.random.seed(1)
        np.random.shuffle(coordy)
        xMid = coordx[0] + int(patchSize / 2) + 1
        yMid = coordy[0] + int(patchSize / 2) + 1
        xArrP = np.array([xMid])
        yArrP = np.array([yMid])
        if np.sum(patchAtMidCoords(maskHand,xMid,yMid,patchSize))==0:
            if drawmode:
                cv2.rectangle(img=paddedIm,pt1=(yMid - int(patchSize / 2),xMid - int(patchSize / 2)),
                              pt2=(yMid + int(patchSize / 2),xMid + int(patchSize / 2)),thickness=3,
                              lineType=8,
                              shift=0,color=0)
            else:
                truePatch = patchAtMidCoords(paddedIm,xMid,yMid,patchSize)
                if (np.sum(np.isnan(truePatch))==0) and (truePatch.shape == (patchSize,patchSize)):
                    cv2.imwrite(boneLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) +'.jpeg',truePatch)
                    endInd += 1
        for xMid, yMid in zip(coordx[1:]+ int(patchSize / 2) + 1,coordy[1:]+ int(patchSize / 2) + 1):
            if np.sum( np.add(np.abs(xArrP-xMid) , np.abs(yArrP-yMid)) < spacing1*skip)==0 and np.sum(patchAtMidCoords(
                    maskHand-maskBone,xMid- int(patchSize / 2) - 1,yMid- int(patchSize / 2) - 1,patchSize))==0:
                xArrP= np.append( xArrP,xMid )
                yArrP= np.append( yArrP,yMid )
                if drawmode:
                    cv2.rectangle(img=paddedIm,pt1=(yMid - int(patchSize / 2),xMid - int(patchSize / 2)),
                                  pt2=(yMid +  int(patchSize / 2),xMid  + int(patchSize / 2)),thickness=3,
                                  lineType=8,
                                  shift=0,color=0)
                else:
                    truePatch = patchAtMidCoords(paddedIm,xMid,yMid,patchSize)
                    if (np.sum(np.isnan(truePatch) ) == 0) and (truePatch.shape == (patchSize,patchSize)):
                        cv2.imwrite(boneLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',
                                    truePatch)
                        endInd += 1
                #cv2.rectangle(img=im,pt1=(yMid-int(patchSize/2),xMid-int(patchSize/2)),pt2=(yMid+int(patchSize/2),xMid+int(patchSize/2)),thickness=3,lineType=8,shift=0,color=0)
                #poisson number of false patch rate approx 1 to have equal true and false on avrg
                nfalsePatch = max(1,np.random.poisson(prate1))
                #print('drew a true patch')
                for k in range(nfalsePatch):
                    #random search for a false patch around the true patch
                    #xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,
                    # high=int(patchSize/2)+xrdMax )
                    #yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,
                    # high=int(patchSize/2)+yrdMax )
                    ind = 0
                    flag = False
                    while not flag:
                        xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
                        yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
                        ind+=1
                        flag = np.sum(patchAtMidCoords(maskHand,xMid + xrd- int(patchSize / 2) - 1,
                                                       yMid + yrd- int(patchSize / 2) - 1,patchSize))>0 and np.sum(
                            patchAtMidCoords(maskBone,xMid + xrd- int(patchSize / 2) - 1,
                                                       yMid + yrd- int(patchSize / 2) - 1,patchSize))==0
                        if ind>400:
                            break
                            #print('broke away')
                    if flag:
                        if drawmode:
                            cv2.rectangle(img=paddedIm,pt1=(yMid+yrd - int(patchSize / 2),xMid+xrd - int(patchSize /
                                                                                                         2)),
                                          pt2=(yMid+yrd + int(patchSize / 2),xMid+xrd + int(patchSize / 2)),
                                          thickness=-1,
                                          lineType=8,
                                          shift=0,color=0)
                            #print('drew a tissue patch')
                        else:
                            falsePatch = patchAtMidCoords(paddedIm,xMid+xrd,yMid+yrd,patchSize)
                            if (np.sum(np.isnan(falsePatch) )== 0) and (falsePatch.shape == (patchSize,patchSize)):
                                cv2.imwrite(tissueLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) +
                                            '.jpeg',falsePatch)
                                endInd += 1
                                #cv2.rectangle(img=im,pt1=(yMid+yrd - int(patchSize / 2),xMid+xrd - int(patchSize / 2)),pt2=(yMid+yrd + int(patchSize / 2),xMid+xrd + int(patchSize / 2)),thickness=-1,lineType=8,shift=0,color=0)
    xrdMax = 20
    yrdMax = 20
    if np.sum(maskEdge)>0:
        coords = np.where(maskEdge)
        coordx = coords[0]
        coordy = coords[1]
        np.random.seed(1)
        np.random.shuffle(coordx)
        np.random.seed(1)
        np.random.shuffle(coordy)
        xMid = coordx[0] + int(patchSize / 2) + 1
        yMid = coordy[0] + int(patchSize / 2) + 1
        xArrP = np.array([xMid])
        yArrP = np.array([yMid])

        for xMid, yMid in zip(coordx[1:]+ int(patchSize / 2) + 1,coordy[1:]+ int(patchSize / 2) + 1):
            if np.sum( np.add(np.abs(xArrP-xMid) , np.abs(yArrP-yMid)) < spacing2*skip)==0:
                xArrP= np.append( xArrP,xMid )
                yArrP= np.append( yArrP,yMid )
                #cv2.rectangle(img=im,pt1=(yMid-int(patchSize/2),xMid-int(patchSize/2)),pt2=(yMid+int(patchSize/2),xMid+int(patchSize/2)),thickness=3,lineType=8,shift=0,color=0)
                #poisson number of false patch rate approx 1 to have equal true and false on avrg
                nfalsePatch = np.random.poisson(prate2)
                for k in range(nfalsePatch):
                    #random search for a false patch around the true patch
                    xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
                    yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
                    ind = 0
                    flag = True
                    while flag:
                        xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
                        yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
                        ind+=1
                        flag = np.sum(patchAtMidCoords(paddedMask,xMid+xrd ,yMid+yrd ,patchSize)) != 0
                        if ind>100:
                            break
                    if not flag:
                        if drawmode:
                            cv2.circle(paddedIm,(yMid+yrd,xMid+xrd),patchSize,(0,255,0),-1)
                        else:
                            falsePatch = patchAtMidCoords(paddedIm,xMid+xrd,yMid+yrd,patchSize)
                            if (np.sum(np.isnan(falsePatch) )== 0) and (falsePatch.shape == (patchSize,patchSize)):
                                cv2.imwrite(falseLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',falsePatch)
                                endInd += 1



    if drawmode:
        plt.figure()
        plt.imshow(paddedIm)
        plt.title('separate masks used for each outline')
        plt.show()
    return endInd


def savePatchBone(im,maskBone,boneLoc,falseLoc,patchSize,startInd,
                             drawmode=False,prate=1.3,spacing = 2.5):
    xDim = im.shape[0]
    yDim = im.shape[1]
    paddedIm = np.zeros((xDim + 2 * int(patchSize / 2) + 2,yDim + 2 * int(patchSize / 2) + 2))
    paddedIm[int(patchSize / 2) + 1:xDim + int(patchSize / 2) + 1,
    int(patchSize / 2) + 1:yDim + int(patchSize / 2) + 1] = im[:,:]
    paddedMask = np.zeros((xDim + 2 * int(patchSize / 2) + 2,yDim + 2 * int(patchSize / 2) + 2))
    paddedMask[int(patchSize / 2) + 1:xDim + int(patchSize / 2) + 1,
    int(patchSize / 2) + 1:yDim + int(patchSize / 2) + 1] = (maskBone[:,:])>0

    xrdMax = 50
    yrdMax = 50
    skip   = patchSize+1
    endInd = startInd
    xArrP = np.array([])
    yArrP = np.array([])
    if np.sum(maskBone)>0:
        coords = np.where(maskBone)
        coordx = coords[0]
        coordy = coords[1]
        np.random.seed(1)
        np.random.shuffle(coordx)
        np.random.seed(1)
        np.random.shuffle(coordy)
        xMid = coordx[0] + int(patchSize / 2) + 1
        yMid = coordy[0] + int(patchSize / 2) + 1
        xArrP = np.array([xMid])
        yArrP = np.array([yMid])

        if drawmode:
                cv2.rectangle(img=paddedIm,pt1=(yMid - int(patchSize / 2),xMid - int(patchSize / 2)),
                              pt2=(yMid + int(patchSize / 2),xMid + int(patchSize / 2)),thickness=3,
                              lineType=8,
                              shift=0,color=0)
        else:
                truePatch = patchAtMidCoords(paddedIm,xMid,yMid,patchSize)
                if (np.sum(np.isnan(truePatch))==0) and (truePatch.shape == (patchSize,patchSize)):
                    cv2.imwrite(boneLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) +'.jpeg',truePatch)
                    endInd += 1
        for xMid, yMid in zip(coordx[1:]+ int(patchSize / 2) + 1,coordy[1:]+ int(patchSize / 2) + 1):
            if np.sum( np.add(np.abs(xArrP-xMid) , np.abs(yArrP-yMid)) < spacing*skip)==0:
                xArrP= np.append( xArrP,xMid )
                yArrP= np.append( yArrP,yMid )
                if drawmode:
                    cv2.rectangle(img=paddedIm,pt1=(yMid - int(patchSize / 2),xMid - int(patchSize / 2)),
                                  pt2=(yMid +  int(patchSize / 2),xMid  + int(patchSize / 2)),thickness=3,
                                  lineType=8,
                                  shift=0,color=0)
                else:
                    truePatch = patchAtMidCoords(paddedIm,xMid,yMid,patchSize)
                    if (np.sum(np.isnan(truePatch) ) == 0) and (truePatch.shape == (patchSize,patchSize)):
                        cv2.imwrite(boneLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) + '.jpeg',
                                    truePatch)
                        endInd += 1
                #cv2.rectangle(img=im,pt1=(yMid-int(patchSize/2),xMid-int(patchSize/2)),pt2=(yMid+int(patchSize/2),xMid+int(patchSize/2)),thickness=3,lineType=8,shift=0,color=0)
                #poisson number of false patch rate approx 1 to have equal true and false on avrg
                nfalsePatch = max(1,np.random.poisson(prate))
                for k in range(nfalsePatch):
                    #random search for a false patch around the true patch
                    xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
                    yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
                    ind = 0
                    flag = True
                    while flag:
                        xrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+xrdMax )
                        yrd=np.random.choice([-1,1])*np.random.randint(low=int(patchSize/2)+1,high=int(patchSize/2)+yrdMax )
                        ind+=1
                        flag = np.sum(patchAtMidCoords(maskBone,xMid + xrd,yMid + yrd,patchSize)) != 0
                        if ind>500:
                            break
                    if not flag:
                        if drawmode:
                            cv2.rectangle(img=paddedIm,pt1=(yMid+yrd - int(patchSize / 2),xMid+xrd - int(patchSize /
                                                                                                         2)),
                                          pt2=(yMid+yrd + int(patchSize / 2),xMid+xrd + int(patchSize / 2)),
                                          thickness=-1,
                                          lineType=8,
                                          shift=0,color=0)
                        else:
                            falsePatch = patchAtMidCoords(paddedIm,xMid+xrd,yMid+yrd,patchSize)
                            if (np.sum(np.isnan(falsePatch) )== 0) and (falsePatch.shape == (patchSize,patchSize)):
                                cv2.imwrite(falseLoc + '/patch' + str(endInd) + '_' + str(xMid) + '_' + str(yMid) +
                                            '.jpeg',falsePatch)
                                endInd += 1
                                #cv2.rectangle(img=im,pt1=(yMid+yrd - int(patchSize / 2),xMid+xrd - int(patchSize / 2)),pt2=(yMid+yrd + int(patchSize / 2),xMid+xrd + int(patchSize / 2)),thickness=-1,lineType=8,shift=0,color=0)


    if drawmode:
        plt.figure()
        plt.imshow(paddedIm)
        plt.title('separate masks used for each outline')
        plt.show()
    return endInd



def make_label_data_TFInOut(curveLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
                      ,imLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/batchNorm/1/'
                      ,patchSize = 41
                      ):
    skip = patchSize+1
    output_directory = update_dir('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/patchesTFrecord/')
    edgeLoc   = os.path.join(output_directory,'boneEdge')
    inLoc     = os.path.join(output_directory,'boneIn')
    tissueLoc = os.path.join(output_directory,'tissueEdge')
    falseLoc  = os.path.join(output_directory,'false')
    os.makedirs(edgeLoc)
    os.makedirs(inLoc)
    os.makedirs(tissueLoc)
    os.makedirs(falseLoc)
    imList = sorted(os.listdir(imLoc))
    Y      = []
    startInd = 0
    for imName in imList:
        print(imName)
        im = cv2.imread(imLoc+imName,0)
        Nx = im.shape[0]
        Ny = im.shape[1]
        bones = ['RH2MC','RH2DP','RH2PP','RH2MP']
        maskIn = np.zeros((Nx,Ny))
        maskEdge = np.zeros((Nx,Ny))
        for bone in bones:
            if os.path.isfile(curveLoc + imName[:-4] + '_' + bone + '.txt'):
                cv = np.loadtxt(curveLoc + imName[:-4] + '_' + bone + '.txt')
                cv[:,1] = Nx - cv[:,1]
                pts = cv.astype(np.int32)
                #cv2.fillConvexPoly(maskIn,pts,255,1)
                cv2.polylines(maskEdge,[pts],False,(255,255,255),1)
        handMask = np.zeros((Nx,Ny))
        if os.path.isfile('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/userInterface/savedPoints/'+imName[:-4]+'_RH'
                '.txt'):
            handCv = np.loadtxt(
                '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/userInterface/savedPoints/'+imName[:-4]+'_RH'
                '.txt')

            handCv[:,1] = Nx - handCv[:,1]
            pts = handCv.astype(np.int32)
            cv2.polylines(handMask,[pts],False,(255,255,255),1)
        savePatchInOut(im,maskIn,maskEdge,handMask,edgeLoc=edgeLoc,tissueLoc=tissueLoc,inLoc=inLoc,falseLoc=falseLoc,
                       patchSize=patchSize,startInd=0,
                       drawmode=False,prate1=0.2,prate2=0.1,prate3=0.1,spacing1=0.9,spacing2=0.9,spacing3=1.1)

    trainLoc = output_directory+'/Train'
    valLoc   = output_directory+'/Val'
    if not os.path.isdir(trainLoc): os.makedirs(trainLoc)
    if not os.path.isdir(valLoc): os.makedirs(valLoc)
    L = len(output_directory)+1
    for loc in [edgeLoc,inLoc,tissueLoc,falseLoc]:
        fileNames = np.array(sorted(os.listdir(loc),reverse=False))
        index     = range(len(fileNames))
        train,val = train_test_split(index,test_size=0.2)
        destLoc = os.path.join(trainLoc,loc[L:])
        os.makedirs(destLoc)
        for f in fileNames[train]:
            shutil.move(os.path.join(loc,f),os.path.join(destLoc,f))
        #moving patched with edges to validation folder
        destLoc = os.path.join(valLoc,loc[L:])
        os.makedirs(destLoc)
        for f in fileNames[val]:
            shutil.move(os.path.join(loc,f),os.path.join(destLoc,f))
        os.rmdir(loc)


    if not os.path.isfile(os.path.join(output_directory,'label.txt')):
     with open(os.path.join(output_directory,'label.txt'), 'a') as fp:
      fp.write('boneEdge\n')
      fp.write('boneIn\n')
      fp.write('tissueEdge\n')
      fp.write('false')
    return output_directory

def make_label_data_TF(curveLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
                      ,imLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/batchNorm/1/'
                      ,patchSize = 41
                      ):
    skip = patchSize+1
    output_directory = update_dir('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/patchesTFrecord/')
    trueLoc  = output_directory+'/Bone'
    falseLoc = output_directory+'/False'
    os.makedirs(trueLoc)
    os.makedirs(falseLoc)
    imList = sorted(os.listdir(imLoc))
    Y      = []
    startInd = 0
    for imName in imList:
        print(imName)
        im = cv2.imread(imLoc+imName,0)
        Nx = im.shape[0]
        Ny = im.shape[1]
        mask = np.zeros((Nx, Ny))
        for bone in ['RH2MC','RH2PP','RH2MP','RH2DP']:
            if os.path.isfile(curveLoc + imName[:-4] + '_' + bone + '.txt'):
              cv = np.loadtxt(curveLoc + imName[:-4] + '_' + bone + '.txt')
              cv[:,1] = Nx - cv[:,1]
              pts = cv.astype(np.int32)
              #cv2.polylines(mask,[pts],False,(255,255,255),1)
              cv2.fillConvexPoly(mask,pts,255,1)

        if np.sum(mask>0):
            #startInd = savePatch(im=im,mask=mask,trueLoc=trueLoc,falseLoc=falseLoc,
            #                                      patchSize=patchSize,startInd=startInd)
            startInd = savePatchBone(im,mask,trueLoc,falseLoc,patchSize,startInd,
                          drawmode=False,prate=1.3,spacing=2.5)
    if not os.path.isfile(output_directory+'/label.txt'):
     with open(output_directory+'/label.txt', 'a') as fp:
      fp.write('Bone\n')
      fp.write('False')



    trainLoc = output_directory+'/Train'
    valLoc   = output_directory+'/Val'
    if not os.path.isdir(trainLoc): os.makedirs(trainLoc)
    if not os.path.isdir(valLoc): os.makedirs(valLoc)
    L = len(output_directory)+1
    for loc in [trueLoc,falseLoc]:
        fileNames = np.array(sorted(os.listdir(loc),reverse=False))
        index     = range(len(fileNames))
        train,val = train_test_split(index,test_size=0.2)
        destLoc = os.path.join(trainLoc,loc[L:])
        os.makedirs(destLoc)
        for f in fileNames[train]:
            shutil.move(os.path.join(loc,f),os.path.join(destLoc,f))
        #moving patched with edges to validation folder
        destLoc = os.path.join(valLoc,loc[L:])
        os.makedirs(destLoc)
        for f in fileNames[val]:
            shutil.move(os.path.join(loc,f),os.path.join(destLoc,f))
        os.rmdir(loc)


    if not os.path.isfile(os.path.join(output_directory,'label.txt')):
     with open(os.path.join(output_directory,'label.txt'), 'a') as fp:
      fp.write('Bone\n')
      fp.write('False')
    return output_directory



def augment_label_data(dataLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/patchesTFrecord/9/'
                       ):
    if dataLoc[-1]=='/':
        destLoc = dataLoc[:-1]+'augmented'
    else:
        destLoc = dataLoc+'augmented'


    try:
        shutil.copytree(dataLoc, destLoc)
    except OSError as e:
        # If the error was caused because the source wasn't a directory
        if e.errno == errno.ENOTDIR:
            shutil.copy(dataLoc, destLoc)

    locs = [os.path.join(destLoc, f) for f in os.listdir(destLoc)]
    trainLoc = os.path.join(destLoc,'Train')
    sublocs = [os.path.join(trainLoc, f) for f in os.listdir(trainLoc)]
    for subloc in sublocs:
        files = os.listdir(subloc)
        for file in files:
            im = cv2.imread(os.path.join(subloc,file),0)
            cv2.imwrite(os.path.join(subloc,file[:-5]+'aug.jpeg'),255-im)


    # for loc in locs:
    #     if os.path.isdir(loc):
    #         files = os.listdir(loc)
    #         for file in files:
    #             im = cv2.imread(os.path.join(loc,file),0)
    #             cv2.imwrite(os.path.join)







def make_label_data_TF_edge(curveLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
                      ,imLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/batchNorm/1/'
                      ,patchSize = 41
                      ):
    skip = patchSize+1
    output_directory = update_dir('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/patchesTFrecord/')
    trueLoc  = output_directory+'/True'
    falseLoc = output_directory+'/False'
    dummyLoc = '/tmp/dummies'
    os.makedirs(trueLoc)
    if not os.path.isdir(dummyLoc):
        os.makedirs(dummyLoc)
    os.makedirs(falseLoc)
    imList = sorted(os.listdir(imLoc))
    Y      = []
    startInd = 0
    for imName in imList:
        print(imName)
        im = cv2.imread(imLoc+imName,0)
        Nx = im.shape[0]
        Ny = im.shape[1]
        mask = np.zeros((Nx, Ny))
        """
        for bone in ['RH2MC','RH2PP','RH2MP','RH2DP']:
            if os.path.isfile(curveLoc + imName[:-4] + '_' + bone + '.txt'):
              cv = np.loadtxt(curveLoc + imName[:-4] + '_' + bone + '.txt')
              cv[:,1] = Nx - cv[:,1]
              pts = cv.astype(np.int32)
              cv2.polylines(mask,[pts],False,(255,255,255),1)

        if np.sum(mask>0):
            startInd = savePatch(im=im,mask=mask,trueLoc=output_directory+'/True',falseLoc=falseLoc,
                                                  patchSize=patchSize,startInd=startInd)
        """
        mask = np.zeros((Nx,Ny))
        if os.path.isfile(curveLoc + imName[:-4] + '_RH.txt'):
            print('adding edge too')
            cv = np.loadtxt(curveLoc + imName[:-4] + '_RH.txt')
            cv[:,1] = Nx - cv[:,1]
            pts = cv.astype(np.int32)
            cv2.polylines(mask,[pts],False,(255,255,255),1)

        if np.sum(mask>0):
            startInd = savePatch(im=im,mask=mask,trueLoc=output_directory+'/False',falseLoc=dummyLoc,
                                                  patchSize=patchSize,startInd=startInd)
    if not os.path.isfile(output_directory+'/label.txt'):
     with open(output_directory+'/label.txt', 'a') as fp:
      fp.write('True\n')
      fp.write('False')
    return output_directory



def make_label_data_TF_edge_false(curveLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
                      ,imLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/batchNorm/1/'
                      ,patchSize = 41
                      ):
    skip = patchSize+1
    output_directory = update_dir('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/patchesTFrecord/')
    trueLoc  = os.path.join(output_directory,'True')
    falseLoc = os.path.join(output_directory,'False')
    dummyLoc = '/tmp/dummies'
    os.makedirs(trueLoc)
    if not os.path.isdir(dummyLoc):
        os.makedirs(dummyLoc)
    os.makedirs(falseLoc)
    imList = sorted(os.listdir(imLoc))
    Y      = []
    startInd = 0
    for imName in imList:
        print(imName)
        im = cv2.imread(os.path.join(imLoc,imName),0)
        Nx = im.shape[0]
        Ny = im.shape[1]
        mask = np.zeros((Nx, Ny))
        #"""
        for bone in ['RH2MC','RH2PP','RH2MP','RH2DP']:
            if os.path.isfile(os.path.join(curveLoc , imName[:-4] + '_' + bone + '.txt')):
              cv = np.loadtxt(os.path.join(curveLoc , imName[:-4] + '_' + bone + '.txt'))
              cv[:,1] = cv[:,1]
              pts = cv.astype(np.int32)
              cv2.polylines(mask,[pts],False,(255,255,255),1)

        if np.sum(mask>0):
            print('bone')
            startInd = savePatch(im=im,mask=mask,trueLoc=trueLoc,falseLoc=falseLoc,
                                                  patchSize=patchSize,startInd=startInd,prate=0.2)
        #"""
        mask = np.zeros((Nx,Ny))
        if os.path.isfile(os.path.join(curveLoc,imName[:-4] + '_RH.txt')):
            print('adding edge too')
            cv = np.loadtxt(os.path.join(curveLoc,imName[:-4] + '_RH.txt'))
            cv[:,1] = cv[:,1]
            pts = cv.astype(np.int32)
            cv2.polylines(mask,[pts],False,(255,255,255),1)

        if np.sum(mask>0):
            startInd = savePatch(im=im,mask=mask,trueLoc=trueLoc,
                                 falseLoc=falseLoc,
                                                  patchSize=patchSize,startInd=startInd,prate=0.2,spacing=5)
    if not os.path.isfile(os.path.join(output_directory,'/label.txt')):
        with open(os.path.join(output_directory,'label.txt'), 'a') as fp:
            fp.write('True\n')
            fp.write('False')
    return output_directory






def turn_to_lab_val_set(patchLoc='/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/patchesTFrecord/2'):
    trainLoc = patchLoc + '/Train'
    valLoc = patchLoc + '/Val'
    #making the 2 folders
    if not os.path.isdir(trainLoc): os.makedirs(trainLoc)
    if not os.path.isdir(valLoc): os.makedirs(valLoc)
    #moking the label.txt as in the tf tutorial

    trueFileNames = np.array(sorted(os.listdir(patchLoc + '/True'),reverse=False))
    falseFileNames = np.array(sorted(os.listdir(patchLoc + '/False'),reverse=False))

    trueRange = range(len(trueFileNames))
    falseRange = range(len(falseFileNames))

    trueTrain,trueVal = train_test_split(trueRange,test_size=0.2)
    falseTrain,falseVal = train_test_split(falseRange,test_size=0.2)

    #moving patched with edges to training folder
    if not os.path.isdir(trainLoc + '/True'): os.makedirs(trainLoc + '/True')
    for f in trueFileNames[trueTrain]:
        shutil.move(patchLoc + '/True/' + f,trainLoc + '/True/' + f)
    #moving patches with no edges to training folder
    if not os.path.isdir(trainLoc + '/False'): os.makedirs(trainLoc + '/False')
    for f in falseFileNames[falseTrain]:
        shutil.move(patchLoc + '/False/' + f,trainLoc + '/False/' + f)

    #moving patched with edges to validation folder
    if not os.path.isdir(valLoc + '/True'): os.makedirs(valLoc + '/True')
    for f in trueFileNames[trueVal]:
        shutil.move(patchLoc + '/True/' + f,valLoc + '/True/' + f)
    #moving patches with no edges to training folder
    if not os.path.isdir(valLoc + '/False'): os.makedirs(valLoc + '/False')
    for f in falseFileNames[falseVal]:
        shutil.move(patchLoc + '/False/' + f,valLoc + '/False/' + f)
    os.rmdir(patchLoc + '/False/')
    os.rmdir(patchLoc + '/True/')



def make_label_data_TF_Tissue_Bone(curveLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
                      ,imLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/batchNorm/1/'
                      ,patchSize = 41
                      ):
    skip = patchSize+1
    output_directory = update_dir('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/patchesTFrecord/')
    tissueLoc  = output_directory+'/Tissue'
    boneLoc  = output_directory+'/Bone'
    falseLoc = output_directory+'/False'
    os.makedirs(boneLoc)
    os.makedirs(tissueLoc)
    os.makedirs(falseLoc)
    imList = sorted(os.listdir(imLoc))
    Y      = []
    startInd = 0
    #k = 0
    for imName in imList:
        print(imName)
        im = cv2.imread(imLoc+imName,0)
        Nx = im.shape[0]
        Ny = im.shape[1]
        flag = bool(0)
        #plot = True
        maskBone = np.zeros((Nx,Ny))
        maskEdge = np.zeros((Nx,Ny))
        for bone in ['RH2MC','RH2PP','RH2MP','RH2DP']:
            if os.path.isfile(curveLoc + imName[:-4] + '_' + bone + '.txt'):
                cv = np.loadtxt(curveLoc + imName[:-4] + '_' + bone + '.txt')
                cv[:,1] = Nx - cv[:,1]
                pts = cv.astype(np.int32)
                cv2.fillConvexPoly(maskBone,pts,255,1)

                #cv2.polylines(maskEdge,[pts],False,(255,255,255),1)
        maskHand = np.zeros((Nx,Ny))
        if os.path.isfile(os.path.join(curveLoc,imName[:-4]+'_RH.txt')):
            handCv = np.loadtxt(os.path.join(curveLoc,imName[:-4]+'_RH.txt'))
            handCv[:,1] = Nx - handCv[:,1]
            pts = handCv.astype(np.int32)
            cv2.fillPoly(maskHand,[pts],255,1)
            cv2.polylines(maskEdge,[pts],False,(255,255,255),1)

        startInd = savePatchBoneTissue(im,maskBone,maskHand,maskEdge,boneLoc=boneLoc,tissueLoc=tissueLoc,falseLoc=falseLoc,
                                       patchSize=patchSize,startInd=0,drawmode=False,prate1=1,prate2=1.0,prate3=1.0,
                                       spacing1 = 1.5,spacing2=5,spacing3=2.5)



    trainLoc = output_directory+'/Train'
    valLoc   = output_directory+'/Val'
    if not os.path.isdir(trainLoc): os.makedirs(trainLoc)
    if not os.path.isdir(valLoc): os.makedirs(valLoc)
    L = len(output_directory)+1
    for loc in [boneLoc,tissueLoc,falseLoc]:
        fileNames = np.array(sorted(os.listdir(loc),reverse=False))
        index     = range(len(fileNames))
        train,val = train_test_split(index,test_size=0.2)
        destLoc = os.path.join(trainLoc,loc[L:])
        os.makedirs(destLoc)
        for f in fileNames[train]:
            shutil.move(os.path.join(loc,f),os.path.join(destLoc,f))
        #moving patched with edges to validation folder
        destLoc = os.path.join(valLoc,loc[L:])
        os.makedirs(destLoc)
        for f in fileNames[val]:
            shutil.move(os.path.join(loc,f),os.path.join(destLoc,f))
        os.rmdir(loc)


    if not os.path.isfile(os.path.join(output_directory,'label.txt')):
     with open(os.path.join(output_directory,'label.txt'), 'a') as fp:
      fp.write('boneEdge\n')
      fp.write('boneIn\n')
      fp.write('tissueEdge\n')
      fp.write('false')
    if not os.path.isfile(output_directory+'/label.txt'):
     with open(output_directory+'/label.txt', 'a') as fp:
      fp.write('Tissue\n')
      fp.write('Bone\n')
      fp.write('False')
    return output_directory



def save_masks(curveLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
              ,imLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/batchNorm/1/'
               ,handOutlineLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/handOutlines/'
               ,boneOutlineLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/boneOutlines/'
               ,handFillLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/handFill/'
               ,boneFillLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/boneFill/'
               ):
    if not os.path.isdir(handOutlineLoc):  os.makedirs(handOutlineLoc)
    if not os.path.isdir(boneOutlineLoc):  os.makedirs(boneOutlineLoc)
    if not os.path.isdir(handFillLoc):  os.makedirs(handFillLoc)
    if not os.path.isdir(boneFillLoc):  os.makedirs(boneFillLoc)
    imList = sorted(os.listdir(imLoc))
    Y      = []
    startInd = 0
    #k = 0
    locAllBoneOutline = os.path.join(boneOutlineLoc,'all')
    locAllBoneFill    = os.path.join(boneFillLoc,'all')
    if not os.path.isdir(locAllBoneFill): os.makedirs(locAllBoneFill)
    if not os.path.isdir(locAllBoneOutline): os.makedirs(locAllBoneOutline)
    for imName in imList:
        print(imName)
        im = cv2.imread(imLoc+imName,0)
        Nx = im.shape[0]
        Ny = im.shape[1]
        flag = bool(0)
        #plot = True

        allBoneOutline = np.zeros((Nx,Ny))
        allBoneFill    = np.zeros((Nx,Ny))
        for bone in ['RH2MC','RH2PP','RH2MP','RH2DP']:
            if os.path.isfile(curveLoc + imName[:-4] + '_' + bone + '.txt'):
                maskBoneOutline = np.zeros((Nx,Ny))
                maskBoneFill    = np.zeros((Nx,Ny))
                cv = np.loadtxt(curveLoc + imName[:-4] + '_' + bone + '.txt')
                cv[:,1] = Nx - cv[:,1]
                pts = cv.astype(np.int32)
                cv2.fillPoly(maskBoneFill,[pts],255,1)
                cv2.fillPoly(allBoneFill,[pts],255,1)
                cv2.polylines(maskBoneOutline,[pts],False,(255,255,255),1)
                cv2.polylines(allBoneOutline,[pts],False,(255,255,255),1)
                locStringOutline = os.path.join(boneOutlineLoc,bone)
                locStringFill    = os.path.join(boneFillLoc,bone)
                if not os.path.isdir(locStringOutline):
                    os.makedirs(locStringOutline)
                if not os.path.isdir(locStringFill):
                    os.makedirs(locStringFill)
                cv2.imwrite(os.path.join(locStringOutline,imName),maskBoneOutline)
                cv2.imwrite(os.path.join(locStringFill,imName),maskBoneFill)

        cv2.imwrite(os.path.join(locAllBoneOutline,imName),allBoneOutline)
        cv2.imwrite(os.path.join(locAllBoneFill,imName),allBoneFill)



                #cv2.polylines(maskEdge,[pts],False,(255,255,255),1)
        maskEdge = np.zeros((Nx,Ny))
        maskHand = np.zeros((Nx,Ny))
        if os.path.isfile(os.path.join(curveLoc,imName[:-4]+'_RH.txt')):
            handCv = np.loadtxt(os.path.join(curveLoc,imName[:-4]+'_RH.txt'))
            handCv[:,1] = Nx - handCv[:,1]
            pts = handCv.astype(np.int32)
            cv2.fillPoly(maskHand,[pts],255,1)
            cv2.polylines(maskEdge,[pts],False,(255,255,255),1)
            cv2.imwrite(os.path.join(handOutlineLoc,imName),maskEdge)
            cv2.imwrite(os.path.join(handFillLoc,imName),maskHand)





def save_fingers(curveLoc='/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
               ,imLoc='/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/batchNorm/1/'
               ,fingerLoc='/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/fingers/'
               ):
    imList = sorted(os.listdir(imLoc))
    startInd = 0
    centroid_array = []
    #k = 0
    if not os.path.isdir(fingerLoc): os.makedirs(fingerLoc)
    for imName in imList:
        print(imName)
        im = cv2.imread(imLoc+imName,0)
        Nx = im.shape[0]
        Ny = im.shape[1]
        k = 0
        maskBoneFill = np.zeros((Nx,Ny))
        for bone in ['RH2MC','RH2PP','RH2MP','RH2DP']:
            if os.path.isfile(curveLoc + imName[:-4] + '_' + bone + '.txt'):

                cv = np.loadtxt(curveLoc + imName[:-4] + '_' + bone + '.txt')
                cv[:,1] = Nx - cv[:,1]
                pts = cv.astype(np.int32)
                cv2.fillPoly(maskBoneFill,[pts],255,1)
                locStringOutline = os.path.join(fingerLoc,bone)
                if k==0:
                    centroid = np.expand_dims(np.mean(cv,axis=0,keepdims=True),2)
                else:
                    centroid = np.concatenate((centroid,np.expand_dims(np.mean(cv,axis=0,keepdims=True),2)),axis=0)
                k+=1

        if k==4:
            kernel = np.ones((5,5),np.uint8)
            dilation = cv2.dilate(maskBoneFill,kernel,iterations=35)
            blurredDilation = filters.gaussian(dilation/255,sigma=20)
            maskedIm = im.astype(np.float32)*blurredDilation
            cv2.imwrite(os.path.join(fingerLoc,imName),maskedIm)
            if startInd==0:
                centroid_array = centroid
            else:
                centroid_array = np.concatenate((centroid_array,centroid),axis=2)
            startInd+=1

    sio.savemat(os.path.join(fingerLoc,'centroids.mat'),{'centroids':centroid_array})
    print('saved '+str(startInd)+' images')
    return centroid_array









#uses output from make_label_data_TF to make a training and a validation set
def turn_to_lab_val_set_hand(patchLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/patchesTFrecord/2'):
    trainLoc = patchLoc+'/Train'
    valLoc   = patchLoc+'/Val'
    #making the 2 folders 
    if not os.path.isdir(trainLoc): os.makedirs(trainLoc)
    if not os.path.isdir(valLoc): os.makedirs(valLoc)
    #moking the label.txt as in the tf tutorial
    if not os.path.isfile(trainLoc+'/label.txt'):
     with open(trainLoc+'/label.txt', 'a') as fp:
      fp.write('Tissue\n')
      fp.write('Bone\n')
      fp.write('False')
    if not os.path.isfile(valLoc+'/label.txt'):
     with open(valLoc+'/label.txt', 'a') as fp:
      fp.write('Tissue\n')
      fp.write('Bone\n')
      fp.write('False')
    tissueFileNames  = np.array(sorted(os.listdir(patchLoc+'/Tissue') ,reverse = False))
    boneFileNames  = np.array(sorted(os.listdir(patchLoc+'/Bone') ,reverse = False))
    falseFileNames = np.array(sorted(os.listdir(patchLoc+'/False') ,reverse = False))
    
    tissueRange  = range(len(tissueFileNames))
    boneRange  = range(len(boneFileNames))
    falseRange = range(len(falseFileNames))
    
    tissueTrain, tissueVal  = train_test_split(tissueRange, test_size=0.2)
    boneTrain, boneVal  = train_test_split(boneRange, test_size=0.2)
    falseTrain,falseVal = train_test_split(falseRange, test_size=0.2)
    
    #moving patched with edges to training folder
    if not os.path.isdir(trainLoc+'/Tissue'): os.makedirs(trainLoc+'/Tissue')
    for f in tissueFileNames[tissueTrain]:  
        shutil.move(patchLoc+'/Tissue/'+f,trainLoc+'/Tissue/'+f)
    if not os.path.isdir(trainLoc+'/Bone'): os.makedirs(trainLoc+'/Bone')
    for f in boneFileNames[boneTrain]:  
        shutil.move(patchLoc+'/Bone/'+f,trainLoc+'/Bone/'+f)
    #moving patches with no edges to training folder
    if not os.path.isdir(trainLoc+'/False'): os.makedirs(trainLoc+'/False')
    for f in falseFileNames[falseTrain]:  
        shutil.move(patchLoc+'/False/'+f,trainLoc+'/False/'+f)
    
    #moving patched with edges to validation folder
    if not os.path.isdir(valLoc+'/Tissue'): os.makedirs(valLoc+'/Tissue')
    for f in tissueFileNames[tissueVal]:
        shutil.move(patchLoc+'/Tissue/'+f,valLoc+'/Tissue/'+f)
    if not os.path.isdir(valLoc+'/Bone'): os.makedirs(valLoc+'/Bone')
    for f in boneFileNames[boneVal]:
        shutil.move(patchLoc+'/Bone/'+f,valLoc+'/Bone/'+f)
    #moving patches with no edges to training folder
    if not os.path.isdir(valLoc+'/False'): os.makedirs(valLoc+'/False')
    for f in falseFileNames[falseVal]:
        shutil.move(patchLoc+'/False/'+f,valLoc+'/False/'+f)
    os.rmdir(patchLoc+'/False/')
    os.rmdir(patchLoc+'/Tissue/')
    os.rmdir(patchLoc + '/Bone/')


#turns an image into a collection of patches for

def patch_one_imLoc(imLoc,patchSize=17,strides=5,destLoc = './validationData/'):
    im = cv2.imread(imLoc,0)
    im,logdir =patch_one_im(im,patchSize=17,strides=5,destLoc = './validationData/')
    return im,logdir

def patch_one_im(im,patchSize=17,strides=5,destLoc = './validationData/'):
    xDim = im.shape[0]
    yDim = im.shape[1]
    start = 0
    paddedIm = np.zeros((xDim+2*int(patchSize/2)+2,yDim+2*int(patchSize/2)+2))
    paddedIm[int(patchSize/2)+1:xDim+int(patchSize/2)+1,int(patchSize/2)+1:yDim+int(patchSize/2)+1] = im[:,:]

    midPointsY = np.arange(start=start,stop=yDim,step=strides)
    midPointsX = np.arange(start=start,stop=xDim,step=strides)

    logdir = update_dir(destLoc)
    os.mkdir(logdir+'/Image')
    with open(logdir + '/label.txt','a') as fp:
        fp.write('Image\n')
        fp.write('False')
    print(paddedIm.shape)
    k = 0
    for x in midPointsX:
        for y in midPointsY:
            patch = patchAtMidCoords(paddedIm,x+int(patchSize/2)+2,y+int(patchSize/2)+2,patchSize)
            cv2.imwrite(logdir + '/Image' + '/patch' +'_'+str(k)+'.jpeg',patch)
            patch = patch.reshape(patchSize**2)
            if k == 0:
                    patchCol = np.expand_dims(patch,0)
                    coords = np.array([[x,y]])
            else:
                    patchCol = np.concatenate((patchCol,np.expand_dims(patch,0)),axis=0)
                    coords = np.concatenate((coords,np.array([[x,y]])),axis=0)
            k += 1
            print(k)
    np.savetxt(logdir+'/coords.txt',coords)
    return im,logdir


def save_sampling_in_out():
    maskLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/boneFill/all'
    imLoc   = locations.LABELLED_XRAY_LOC_RAW
    imName  = 'CPSA0003h2011'
    im      = cv2.imread(os.path.join(imLoc,imName+'.png'),0)
    mask    = cv2.imread(os.path.join(maskLoc,imName+'.png'),0)
    samples = drawPatch(im,mask,patchSize=17,prate=1.5,spacing=2)
    kernel = np.ones((5,5),np.uint8)
    dilation = cv2.dilate(mask,kernel,iterations=1)
    cv2.imwrite(os.path.join('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/writeUps/myThesisGit/figures'
                             '/textureChapter',imName+'_in_out_smpl.png'),samples)


def show_sampling_edge():
    maskLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/boneOutlines/all'
    imLoc   = locations.LABELLED_XRAY_LOC_RAW
    imName  = 'CPSA0003h2011'
    im      = cv2.imread(os.path.join(imLoc,imName+'.png'),0)
    mask    = cv2.imread(os.path.join(maskLoc,imName+'.png'),0)
    samples = drawPatch(im,mask,patchSize=17,prate=1.5,spacing=1.7)
    kernel = np.ones((5,5),np.uint8)
    dilation = cv2.dilate(mask,kernel,iterations=1)
    cv2.imwrite(os.path.join('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/writeUps/myThesisGit/figures'
                             '/textureChapter',imName+'_edge_smpl.png'),samples)
    cv2.imwrite(os.path.join('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/writeUps/myThesisGit/figures'
                             '/textureChapter',imName+'_outline_mask.png'),dilation)






if __name__=='__main__':
    #centroid_array =save_fingers()
    #
    # patchSize=17
    # curveLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
    # imLoc = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/batchNorm/1/'
    # bones = ['RH2MC','RH2PP','RH2MP','RH2DP']
    # imList = sorted(os.listdir(imLoc))
    # #imName = imList[15]
    # imName = 'CPSA0027h2011.png'
    # im = cv2.imread(imLoc + imName,0)
    # Nx = im.shape[0]
    # Ny = im.shape[1]
    # bigMask = np.zeros((Nx,Ny))
    # for bone in bones:
    #     if os.path.isfile(curveLoc + imName[:-4] + '_' + bone + '.txt'):
    #         cv = np.loadtxt(curveLoc + imName[:-4] + '_' + bone + '.txt')
    #         cv[:,1] = Nx - cv[:,1]
    #         pts = cv.astype(np.int32)
    #         cv2.polylines(bigMask,[pts],False,(255,255,255),1)
    # maskList= []
    # maskBone = np.zeros((Nx,Ny))
    # maskEdge = np.zeros((Nx,Ny))
    # for bone in bones:
    #     if os.path.isfile(curveLoc + imName[:-4] + '_' + bone + '.txt'):
    #         cv = np.loadtxt(curveLoc + imName[:-4] + '_' + bone + '.txt')
    #         cv[:,1] = Nx - cv[:,1]
    #         pts = cv.astype(np.int32)
    #         cv2.fillConvexPoly(maskBone,pts,255,1)
    #
    #         #cv2.polylines(maskEdge,[pts],False,(255,255,255),1)
    #
    # handCv = np.loadtxt('/home/amr62/Documents/TheEffingPhDHatersGonnaHate/userInterface/savedPoints/CPSA0027h2011_RH'
    #                   '.txt')
    # maskHand = np.zeros((Nx,Ny))
    # #cv2.fillPoly(maskBone,pts,255,1)
    # handCv[:,1] = Nx - handCv[:,1]
    # pts = handCv.astype(np.int32)
    # #cv2.polylines(maskHand,[pts],False,(255,255,255),1)
    # #cv2.fillConvexPoly(maskHand,pts,255,1)
    # cv2.fillPoly(maskHand,[pts],255,1)
    # #cv2.polylines(bigMask,[pts],False,(255,255,255),1)
    # cv2.polylines(maskEdge,[pts],False,(255,255,255),1)
    # im = cv2.imread(imLoc + imName,0)
    # #savePatchInOut(im,maskIn,maskEdge,handMask,edgeLoc='/tmp/',tissueLoc='/tmp/',inLoc='/tmp/',falseLoc='/tmp/',
    #                #patchSize=patchSize,startInd=0,
    #                #drawmode=True,prate1=1.3,prate2=1.4,spacing=2.5)
    # savePatchBoneTissue(im,maskBone,maskHand,maskEdge,boneLoc='./',tissueLoc='./',falseLoc='./',patchSize=patchSize,
    #                     startInd=0,
    #                          drawmode=True,prate1=1,prate2=1.0,prate3=1.0,spacing1 = 2.5,spacing2=2.5,spacing3=2.5)
    # alpha = 1
    # beta = 1
    # gamma = 0
    # overlay = cv2.addWeighted(maskHand.astype(np.float64),alpha,im.astype(np.float64),beta,gamma)
    # plt.figure();plt.imshow(overlay)
    #savePatchBone(im,maskBone,boneLoc='./',falseLoc='./',patchSize=17,startInd=0,
    #              drawmode=True,prate=1.3,spacing=2.5)

    Qn1 = input('make train data set? 0 for no  ')
    if Qn1 !='0':
        patchSize = int(input('Choose the patch size: '))

        output_dir = make_label_data_TF_edge_false(curveLoc =
                                        '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePointsScaled'
                       ,imLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/histNorm/xRaysLabelledScaled'
                       ,patchSize   = patchSize
                       )

        # output_dir = make_label_data_TFInOut(curveLoc =
        #                                  '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
        #                 ,imLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/histNorm/1/'
        #                 ,patchSize   = patchSize
        #                 )
        #output_dir = make_label_data_TF(curveLoc='/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
        #                   ,imLoc='/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/batchNorm/1/'
        #                   ,patchSize=patchSize
        #                   )
        #output_dir = make_label_data_TF_Tissue_Bone(curveLoc =
        ## '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints/'
        #              ,imLoc    = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/batchNorm/1/'
         #             ,patchSize = patchSize
         #             )
        turn_to_lab_val_set(patchLoc = output_dir)
    Qn2 = input('make tfrecord? 0 for no')
    if Qn2 != '0':
        if not os.path.isdir('./data'): os.mkdir('./data')
        #output_dir = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/patchesTFrecord/8'
        #shutil.copy(os.path.join(output_dir,'label.txt'),os.path.join('./data','label.txt'))
        _process_dataset('train',os.path.join(output_dir,'Train'),1,os.path.join('./data','label.txt') )
        _process_dataset('validation',os.path.join(output_dir,'Val'),1,os.path.join('./data','label.txt') )
        #shutil.copy(os.path.join(output_dir,'label.txt'),os.path.join('./data','label.txt'))


    #overlay = cv2.addWeighted(maskHand.astype(np.float64),alpha,im,beta,gamma)
