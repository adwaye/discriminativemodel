from predict5 import *
from mpl_toolkits.axes_grid1 import make_axes_locatable


def save_preds(model_loc='/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/trainedModel/39/',
               output_dir ='/home/amr62/Documents/TheEffingPhDHatersGonnaHate/writeUps/myThesisGit/figures'
                           '/textureChapter/pnetRegion/',
               ind       = 0
               ):
    files = os.listdir(model_loc)
    def myfunc(a):
        return a[0:13]
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    vfunc = np.vectorize(myfunc)
    cleanFiles = vfunc(files)
    newFiles   = []
    for f in cleanFiles:
        if f[1:4]=='PSA':
            matfile = sio.loadmat(os.path.join(model_loc,f+'_full.mat'))
            preds   = matfile['predictions']
            cv2.imwrite(os.path.join(output_dir,f+'_full.png'),preds[:,:,ind]*255)
            for loc in locations.XRAY_LOCS:
                imName = ''
                if os.path.isfile(os.path.join(loc,f+'.png')):
                    imFile = os.path.join(loc,f+'.png')
            shutil.copy2(imFile,os.path.join(model_loc,f+'.png'))



def plot_preds(model_loc='/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/trainedModel/39/',
               output_dir='/home/amr62/Documents/TheEffingPhDHatersGonnaHate/writeUps/myThesisGit/figures/textureChapter/pnetRegion/',
               ind       = 0
               ):
    files = sorted(os.listdir(model_loc))
    def myfunc(a):
        return a[0:13]
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    vfunc = np.vectorize(myfunc)
    cleanFiles = vfunc(files)
    fig,ax = plt.subplots(ncols=2,nrows=1)
    divider2 = make_axes_locatable(ax[1])
    cax2 = divider2.append_axes("right",size="5%",pad=0.05)
    divider1 = make_axes_locatable(ax[0])
    cax1 = divider1.append_axes("right",size="5%",pad=0.05)
    k = 0
    for f in cleanFiles:
        if f[1:4]=='PSA':
            print(f)
            matfile = sio.loadmat(os.path.join(model_loc,f+'_full.mat'))
            preds   = matfile['predictions']
            for loc in locations.XRAY_LOCS:
                imName = ''
                if os.path.isfile(os.path.join(loc,f+'.png')):
                    imFile = os.path.join(loc,f+'.png')
            im = cv2.imread(imFile)
            imsh1 = ax[0].imshow(im)
            imsh2 = ax[1].imshow(preds[:,:,ind])
            if k==0:
                col1 = fig.colorbar(imsh1,ax=ax[0],cax=cax1)
                col2 = fig.colorbar(imsh2,ax=ax[1],cax=cax2)
            else:
                col1.update_bruteforce(imsh1)
                col2.update_bruteforce(imsh2)

            for i in [0,1]:
                ax[i].set_xticklabels([])
                ax[i].set_yticklabels([])
                ax[i].set_xticks([])
                ax[i].set_yticks([])
            mng = plt.get_current_fig_manager()
            mng.resize(*mng.window.maxsize())
            plt.pause(0.01)
            fig.savefig(os.path.join(output_dir,f+'.png'),bbox_inches='tight')
            k+=1



if __name__=='__main__':
    model_loc  = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/trainedModel/39'
    output_dir = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/writeUps/myThesisGit/figures/textureChapter/pnetRegion/'
    plot_preds(model_loc=model_loc,output_dir=output_dir)