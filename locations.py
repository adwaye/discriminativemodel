import socket
import os

if socket.gethostname()=='turin':
    MASK_RAW_LOC   = '/home/amr62/Documents/tf_unet/trainingData/masksRaw'
    MASK_LOC       = '/home/amr62/Documents/tf_unet/trainingData/croppedMasks'
    FINGER_RAW_LOC = '/home/amr62/Documents/tf_unet/trainingData/fingersRaw'
    FINGER_LOC     = '/home/amr62/Documents/tf_unet/trainingData/croppedFingers'
    LABELLED_XRAY_LOC_RAW = '/home/amr62/Documents/xRaysLabelled'

    XRAY_LOCS             = ['/home/amr62/Documents/xRaysLabelled',
                         '/home/amr62/Documents/xRaysBatch2',
                         '/home/amr62/Documents/xRaysBatch1'
                         ]
    LABELLED_XRAY_LOC_HNORM = '/home/amr62/Documents/xRaysLabelled_hnorm'
    XRAY_HNORM = ['/home/amr62/Documents/xRaysLabelled_hnorm',
                 '/home/amr62/Documents/xRaysBatch2_hnorm',
                 '/home/amr62/Documents/xRaysBatch1_hnorm'
                 ]
else:
    MASK_RAW_LOC   = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/tf_unet/trainingData/masksRaw'
    MASK_LOC       = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/tf_unet/trainingData/croppedMasks'
    FINGER_RAW_LOC = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/tf_unet/trainingData/fingersRaw'
    FINGER_LOC     = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/tf_unet/trainingData/croppedFingers'
    LABELLED_XRAY_LOC_RAW = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysLabelled'
    LABELLED_XRAY_LOC_PIXEL_SCALED = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysLabelledScaled'
    XRAY_LOCS             = ['/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysLabelled',
                         '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysBatch2',
                         '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysBatch1'
                         ]
    LABELLED_XRAY_LOC_HNORM = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysLabelled_hnorm'
    XRAY_HNORM = ['/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysLabelled_hnorm',
                  '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysBatch2_hnorm',
                  '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysBatch1_hnorm'
                 ]
    XRAY_LOCS_PIXEL_SCALED = ['/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysLabelledScaled',
                         '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysBatch2Scaled',
                         '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/xRaysBatch1Scaled'
                         ]
    DICOM_LOCS    = ['/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/dicom','/home/amr62/Documents/TheEffingPhDHatersGonnaHate/ARxrays/dicom_260717']
    RAW_CURVE_LOC = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/mergedCurvePoints'
    MAT_LOC       = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/priorSegmentation/matFiles'
    DISCRIMINATIVE_MODEL_LOC = '/home/amr62/Documents/TheEffingPhDHatersGonnaHate/discriminativeModel/trainedModel'
    BONES = ['RH2MC','RH2PP','RH2MP','RH2DP']

FOURIER_CURVE_NAME     = 'fourier_bone_curve_'
FOURIER_NO_ROT_NAME    = 'rotation_aligned_fourier_bone_curve_'
FOURIER_ALIGNED_NAME   = 'fully_aligned_fourier_bone_curve_'
FOURIER_ALIGNED_FINGER = 'rotation_aligned_fourier_wholeFinger'
FOURIER_FINGER         = 'fourier_wholeFinger'
SPLINE_CURVE_NAME      = 'spline_bone_curve_'
SPLINE_NO_ROT_NAME     = 'rotation_aligned_spline_bone_curve_'
SPLINE_ALIGNED_NAME    = 'fully_aligned_spline_bone_curve_'
SPLINE_ALIGNED_FINGER  = 'rotation_aligned_spline_wholeFinger'
SPLINE_FINGER          = 'spline_wholeFinger'
IM_NAMES               = sorted(os.listdir(LABELLED_XRAY_LOC_RAW))







